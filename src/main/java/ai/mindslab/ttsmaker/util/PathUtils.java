package ai.mindslab.ttsmaker.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PathUtils {
	
	public static boolean createDirectories(Path path) {
		try {
		    Files.createDirectories(path);
		    return true;
		  } catch (IOException e) {
		    System.err.println("Failed to create directory!" + e.getMessage());
		  }
		return false;
	}

}

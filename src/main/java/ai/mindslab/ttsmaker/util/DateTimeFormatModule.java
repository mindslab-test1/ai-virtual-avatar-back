package ai.mindslab.ttsmaker.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;

import java.io.IOException;

public class DateTimeFormatModule extends SimpleModule {
	
	private static final long serialVersionUID = -2229512508147953575L;

	public DateTimeFormatModule() {
		super();
		addSerializer(DateTime.class, new JsonSerializer<DateTime>() {
			public final DateTimeZone TIME_ZONE = DateTimeZone.forID("Asia/Seoul");
			public final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
					.withZone(TIME_ZONE);

			@Override
			public void serialize(DateTime value, JsonGenerator gen, SerializerProvider serializers)
					throws IOException, JsonProcessingException {
				if (value == null) {
					gen.writeNull();
				} else {
					gen.writeString(FORMATTER.print(value));
				}
			}
		});

		addDeserializer(DateTime.class, new JsonDeserializer<DateTime>() {
			public final DateTimeZone TIME_ZONE = DateTimeZone.forID("Asia/Seoul");
			public final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
					.withZone(TIME_ZONE);

			@SuppressWarnings("deprecation")
			@Override
			public DateTime deserialize(JsonParser p, DeserializationContext ctxt)
					throws IOException, JsonProcessingException {
				JsonToken t = p.getCurrentToken();

				if (t == JsonToken.VALUE_NUMBER_INT) {
					return new DateTime(p.getLongValue(), TIME_ZONE);
				}

				if (t == JsonToken.VALUE_STRING) {
					String str = p.getText();
					if (StringUtils.isEmpty(str)) {
						return null;
					}
					return FORMATTER.parseDateTime(str);
				}

				throw ctxt.mappingException(DateTime.class);
			}
		});
	}
}

package ai.mindslab.ttsmaker.util;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class StringUtils  extends org.springframework.util.StringUtils {
	public static String nvl(String str, String defaultStr) {
		return str == null ? defaultStr : str;
	}

	public static Integer toInt(String value) {
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}

	public static Long toLong(String value) {
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			return 0l;
		}
	}
	private static final Pattern STRIP_ACCENTS_PATTERN = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
	public static String stripAccents(final String input) {
        if (input == null) {
            return null;
        }
        final StringBuilder decomposed = new StringBuilder(Normalizer.normalize(input, Normalizer.Form.NFD));
        convertRemainingAccentCharacters(decomposed);
        // Note that this doesn't correctly remove ligatures...
        return STRIP_ACCENTS_PATTERN.matcher(decomposed).replaceAll("");
    }
	private static void convertRemainingAccentCharacters(final StringBuilder decomposed) {
        for (int i = 0; i < decomposed.length(); i++) {
            if (decomposed.charAt(i) == '\u0141') {
                decomposed.setCharAt(i, 'L');
            } else if (decomposed.charAt(i) == '\u0142') {
                decomposed.setCharAt(i, 'l');
            }
        }
    }
}

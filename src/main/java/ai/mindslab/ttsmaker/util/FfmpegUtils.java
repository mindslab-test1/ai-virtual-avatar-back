package ai.mindslab.ttsmaker.util;

import java.util.ArrayList;

public class FfmpegUtils {
	
	public static String defaultFFMpegCommand(String outputPath, float speed) {
		String ffmpegCommand = String.format("ffmpeg -i %s -ac 2 -ar 48000 ", outputPath);
		if(speed != 1.0) {
			ffmpegCommand += String.format("-filter:a atempo=%f -vn ", speed);
		}
		ffmpegCommand += outputPath;
		return ffmpegCommand;
	}
	
    public static String ffmpegSingleCommand(String outputPath, String url) {
        String ffmpegCommand = "ffmpeg -i " + url + " " + outputPath + " -y";
        return ffmpegCommand;
    }

    public static String ffmpegMergingCommand(String outputPath, String url) {
        String ffmpegCommand = "ffmpeg -i " + outputPath + " -i " + url + " -filter_complex [0:0][1:0]concat=n=2:v=0:a=1[out] -map [out] " + outputPath + " -y";
        return ffmpegCommand;
    }

    public static String ffmpegScaling(String outputPath) {
        String ffmpegCommand = "ffmpeg -i " + outputPath + " -ac 2 -acodec pcm_s24le -ar 48000 -ab 24000 "+ outputPath + " -y";
        return ffmpegCommand;
    }

    public static String ffmpegCommandGenerator(ArrayList<String> urlArray, String outputPath) {
        String ffmpegCommand = "ffmpeg ";
        for(int i = 0; i < urlArray.size(); i++) {
            ffmpegCommand += "-i " + urlArray.get(i) + " ";
        }
        ffmpegCommand += "-filter_complex ";
        for(int i = 0; i < urlArray.size(); i++) {
            ffmpegCommand += "[" + i + ":0]";
        }
        ffmpegCommand += "concat=n="+ urlArray.size() +":v=0:a=1[out] -map [out] -ac 2 -ar 48000 " + outputPath + " -y";
        return ffmpegCommand;
    }
    //-acodec pcm_s24le

    public static String ffmpeg24bit(String outputPath2, String outputPath) {
        String ffmpegCommand = "ffmpeg -i " + outputPath2 + " -acodec pcm_s24le -ar 48000 "+ outputPath;
        return ffmpegCommand;
    }
}

package ai.mindslab.ttsmaker.configurers.resolvers;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@AllArgsConstructor
public class AvaUser {
	private Long userNo;
	private String userId;
	private String userNm;
	private String companyNm;
	private List<Long> userPlanNo;
	private AvaUserAgree userAgrees;
	
	public static String userPrint(AvaUser avaUser) {
		if(avaUser != null ) {
			return String.format("user: %s(%s/%s)", avaUser.getUserNo(), avaUser.getUserId(), avaUser.getUserNm());
		}
		return "AvaUser=UNKNOWN";
	}
	
	public static AvaUser of(UserVo entity) {
        return AvaUser.builder()
        		.userNo(entity.getUserNo())
        		.userId(entity.getUserId())
        		.userNm(entity.getUserNm())
        		.companyNm(entity.getCompanyNm())
        		.userPlanNo(entity.getUserPlanNo())
        		.userAgrees(new AvaUserAgree())
        		.build();
    }
	public static AvaUser empty() {
        return AvaUser.builder()
        		.userNo(0l)
        		.userId("GUEST")
        		.userNm("GUEST")
        		.build();
    }
}

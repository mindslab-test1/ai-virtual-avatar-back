package ai.mindslab.ttsmaker.configurers.resolvers;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AvaUserAgree {
	private Boolean profanityUseAgree; // 욕설 동의 여부

	public static AvaUserAgree of(UserVo entity) {
        return AvaUserAgree.builder()
        		.profanityUseAgree(false)
        		.build();
    }
}

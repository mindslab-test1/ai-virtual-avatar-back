package ai.mindslab.ttsmaker.configurers.resolvers;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import ai.mindslab.ttsmaker.common.AVAConst;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthConst;

public class AvaUserParamResolver implements HandlerMethodArgumentResolver {
	
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(AvaUserParam.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory) throws Exception {
        String token = webRequest.getHeader(AVAAuthConst.AUTH_HEADER_STRING);
        if(token == null) {
            return null;
        }
        //String tokenBody = token.replace(JwtProperties.TOKEN_PREFIX,"");
        AvaUser user = (AvaUser) webRequest.getAttribute(AVAConst.AVA_INFO, RequestAttributes.SCOPE_REQUEST);
        
        return user != null ? user : AvaUser.empty();
    }

}
package ai.mindslab.ttsmaker.configurers.filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthConst;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthData;
import ai.mindslab.ttsmaker.configurers.auth.UserPrincipal;
import ai.mindslab.ttsmaker.configurers.auth.UserPrincipalDetailsService;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	private final AuthenticationManager authenticationManager;
	private final UserPrincipalDetailsService userPrincipalDetailsService;
	
	
	public JwtAuthenticationFilter(AuthenticationManager authenticationManager, UserPrincipalDetailsService userPrincipalDetailsService) {
		super.setFilterProcessesUrl(AVAAuthConst.SIGN_IN_PATH);
		this.authenticationManager = authenticationManager;
		this.userPrincipalDetailsService = userPrincipalDetailsService;
	}
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    	
    	String userId = request.getParameter("userId");
    	String userPw = request.getParameter("userPw");
    	
    	// Create login Token
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                		userId,    // email 이용
                		userPw,
                        new ArrayList<>()
                );
    	
        return authenticationManager.authenticate(authenticationToken);

    }
    
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
    	// Grab principal
        UserPrincipal principal = (UserPrincipal)authResult.getPrincipal();
        UserVo userVo = principal.getUser();
        Long userNo = userVo.getUserNo();
        String userId = userVo.getUserId();
        
        AVAAuthData authData = userPrincipalDetailsService.successfulAuthenticationResponse(request, response, authResult, userNo, userId);
        String accessToken = authData.getAccess_token();
        userPrincipalDetailsService.setUserVoFromRedis(accessToken, principal.getUser());
        userPrincipalDetailsService.saveRefreshToken(userNo, userId, authData);
        response.addHeader(AVAAuthConst.AUTH_HEADER_STRING, AVAAuthConst.AUTH_TOKEN_PREFIX + accessToken);
        
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(userPrincipalDetailsService.makeSuccessfulAuthenticationResponseBody(userVo, authData));
        out.flush();   
        
    }
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
    	response.setStatus(HttpStatus.UNAUTHORIZED.value());
    	PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(userPrincipalDetailsService.unsuccessfulAuthenticationResponse(request, response, failed));
        out.flush();   
    }
}

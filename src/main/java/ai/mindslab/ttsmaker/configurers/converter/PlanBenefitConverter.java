package ai.mindslab.ttsmaker.configurers.converter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.AttributeConverter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;


public class PlanBenefitConverter implements AttributeConverter<List<String>, String> {

	@SuppressWarnings("unchecked")
	@Override
	public String convertToDatabaseColumn(List<String> planBenefits) {
		JSONObject object = new JSONObject();
		object.put("items", planBenefits);
		return object.toJSONString();
	}

	@Override
	public List<String> convertToEntityAttribute(String jsonStr) {
		List<String> planBenefits = Lists.newArrayList();
		if(StringUtils.hasLength(jsonStr)) {
			JSONParser parser = new JSONParser();
				try {
					JSONObject jsonObj = (JSONObject) parser.parse( jsonStr );
					if(jsonObj.containsKey("items")) {
						JSONArray jsonArray = (JSONArray)jsonObj.get("items");
						planBenefits = IntStream.range(0, jsonArray.size())
						        .mapToObj(jsonArray::get)
						        .map(Object::toString)
						        .collect(Collectors.toList());
					}
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				
		}
		return planBenefits;
	}
}
package ai.mindslab.ttsmaker.configurers;

import com.google.common.collect.Lists;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaAuditing
@EnableJpaRepositories(
        basePackages = {"ai.mindslab.ttsmaker.api"}, // TODO Repository 패키지 지정
        transactionManagerRef = "ttsMakerTransactionManager",
        entityManagerFactoryRef = "ttsMakerEntityManagerFactory"
)
public class DataSourceConfiguration {
	
	@Bean(name = "ttsMakerDataSource")
	@Primary
    @ConfigurationProperties("spring.datasource.hikari")
    public DataSource mariaDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Primary
    @Bean(name = "ttsMakerEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("ttsMakerDataSource") DataSource dataSource) {
    	
    	List<String> packagesList = Lists.newArrayList();
    	packagesList.add("ai.mindslab.ttsmaker.api.character.domain");
    	packagesList.add("ai.mindslab.ttsmaker.api.job.domain");
    	packagesList.add("ai.mindslab.ttsmaker.api.plan.domain");
    	packagesList.add("ai.mindslab.ttsmaker.api.user.domain");
    	packagesList.add("ai.mindslab.ttsmaker.api.download.domain");
        packagesList.add("ai.mindslab.ttsmaker.api.pay.domain");
        packagesList.add("ai.mindslab.ttsmaker.api.code.domain");
        packagesList.add("ai.mindslab.ttsmaker.api.admin.domain");

        return builder.dataSource(dataSource).packages(packagesList.toArray(new String[] {})).build();
    }

    @Primary
    @Bean(name = "ttsMakerTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("ttsMakerEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}

package ai.mindslab.ttsmaker.configurers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ai.mindslab.ttsmaker.util.DateTimeFormatModule;

@Configuration
public class JsonConfiguration {
	@Bean("objectMapper")
	public ObjectMapper objectMapper() {
		
		return Jackson2ObjectMapperBuilder.json()
				//.featuresToEnable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
				.featuresToDisable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
				.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
				.modules(new JavaTimeModule(), new DateTimeFormatModule())
				.build();
	}
}

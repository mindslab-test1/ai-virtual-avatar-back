package ai.mindslab.ttsmaker.configurers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import ai.mindslab.ttsmaker.configurers.auth.AVAAuthConst;
import ai.mindslab.ttsmaker.configurers.auth.JwtAuthenticationEntryPoint;
import ai.mindslab.ttsmaker.configurers.auth.UserPrincipalDetailsService;
import ai.mindslab.ttsmaker.configurers.filters.JwtAuthenticationFilter;
import ai.mindslab.ttsmaker.configurers.filters.JwtAuthorizationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserPrincipalDetailsService userPrincipalDetailsService;
	
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(this.userPrincipalDetailsService);

        return  daoAuthenticationProvider;
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
    
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        List<String> permitAllList = new ArrayList<String>();
        permitAllList.add("/css/**");
        permitAllList.add("/js/**");
        permitAllList.add("/favicon.ico");
        permitAllList.add("/static/**");
        permitAllList.add("/monitor/**");
        permitAllList.add("/provider/**");

        permitAllList.add("/code/**");
        permitAllList.add("/plan/**");
        permitAllList.add("/pay/**");
        permitAllList.add("/inquiry/**");
        permitAllList.add("/characters/**");
        
        permitAllList.add("/user/sign_up");
        permitAllList.add("/user/sign_out");
        permitAllList.add("/user/auth/refresh_token");
        permitAllList.add("/user/tts");

        permitAllList.add("/job/requestTTS/**");

        // 지울 예정
        permitAllList.add("/manage/**");
//        permitAllList.add("/admin/**");


        http.cors().and()
                .csrf().disable()
                .cors().disable()
                .exceptionHandling().authenticationEntryPoint(new JwtAuthenticationEntryPoint()).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .addFilter(new JwtAuthenticationFilter(this.authenticationManager(), userPrincipalDetailsService))
                .addFilter(new JwtAuthorizationFilter(this.authenticationManager(), userPrincipalDetailsService))
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, AVAAuthConst.SIGN_IN_PATH).permitAll()
                .antMatchers(permitAllList.toArray(new String[]{})).permitAll()
                .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
                //.antMatchers("/job/download/**").hasAuthority(MembershipRoleVo.ROLE_PAID.toString())
                .anyRequest().authenticated();

        http
                .headers()
                .frameOptions().sameOrigin();

    }

    @Bean(name = "corsConfigurationSource")
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("https://kbs-tool.maum.ai", "http://kbs-tool.maum.ai", "http://15.222.174.72:8002", "http://15.222.174.72",  "http://3.35.96.173", "http://15.165.16.245:8002", "https://15.165.16.245:8002", "http://114.108.173.114:8002", "http://localhost:8002", "http://localhost:8000", "http://127.0.0.1:8002", "http://10.52.180.37:8000", "http://localhost:8888","https://tts.tobegin.net", "https://ai.mindslab.net", "https://www.ai.mindslab:8888"));

        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT"));
        configuration.setAllowedHeaders(Arrays.asList("Content-Type", "content-type", "x-requested-with", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "x-auth-token", "x-app-id", "Origin", "Accept", "Authorization", "X-Requested-With", "Access-Control-Request-Method", "Access-Control-Request-Headers", "X-Frame-Options"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}

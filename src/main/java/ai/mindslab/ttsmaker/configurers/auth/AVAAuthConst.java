package ai.mindslab.ttsmaker.configurers.auth;

public class AVAAuthConst {
	public static final String SIGN_IN_PATH = "/user/sign_in";
	public static final String JWT_SECRET = "ttsToolMaumAiSecretKey"; // secret key
	public static final String AUTH_TOKEN_PREFIX = "Bearer ";
	public static final String AUTH_HEADER_STRING = "Authorization";
}

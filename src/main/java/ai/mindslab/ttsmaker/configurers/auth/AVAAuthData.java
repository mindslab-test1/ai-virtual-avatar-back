package ai.mindslab.ttsmaker.configurers.auth;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AVAAuthData {
	private String access_token;
	private LocalDateTime access_token_expire;
	
	private String refresh_token;
	private LocalDateTime refresh_token_expire;
}

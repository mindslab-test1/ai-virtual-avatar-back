package ai.mindslab.ttsmaker.configurers.auth;

import java.time.Duration;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtProvider {
	
	public String generateJwtToken(Long userNo, String userId, Duration expireDuration ) {
		return Jwts.builder().setSubject(userId).claim("uNo", userNo)
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + expireDuration.toMillis()))
				.signWith(SignatureAlgorithm.HS512, AVAAuthConst.JWT_SECRET).compact();
	}
	public String generateJwtToken(UserPrincipal principal, Duration expireDuration ) {
		UserVo userVo = principal.getUser();
		return Jwts.builder().setSubject(principal.getUsername()).claim("uNo", userVo.getUserNo())
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + expireDuration.toMillis()))
				.signWith(SignatureAlgorithm.HS512, AVAAuthConst.JWT_SECRET).compact();
	}

	public boolean validateJwtToken(String authToken, HttpServletRequest httpServletRequest) {
		try {
			Jwts.parser().setSigningKey(AVAAuthConst.JWT_SECRET).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			log.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			log.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			log.error("JWT token is expired: {}", e.getMessage());
			httpServletRequest.setAttribute("expired", e.getMessage());
		} catch (UnsupportedJwtException e) {
			log.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			log.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}

	public Claims getClaims(String token) {
		return Jwts.parser().setSigningKey(AVAAuthConst.JWT_SECRET).parseClaimsJws(token).getBody();
	}
}

package ai.mindslab.ttsmaker.configurers.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;

public class UserPrincipal implements UserDetails {

	private static final long serialVersionUID = 3958009951894736287L;
	private UserVo user;

	public UserPrincipal(UserVo user) {
        this.user = user;
    }

    public static UserPrincipal create(UserVo user) {
        return new UserPrincipal(user);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        
        if(CollectionUtils.isEmpty(user.getRoles())) {
        	authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        }else {
        	authorities.addAll(user.getRoles().stream().map(p->new SimpleGrantedAuthority(p)).collect(Collectors.toList()));
        }
        
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.user.getUserPw();
    }

    @Override
    public String getUsername() {
        return this.user.getUserId();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getActive();
    }
    
    public UserVo getUser() {
  		return user;
  	}
}
package ai.mindslab.ttsmaker.configurers.auth;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.user.service.AuthService;
import ai.mindslab.ttsmaker.api.user.service.UserService;
import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.util.DateUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserPrincipalDetailsService  implements UserDetailsService {

	@Autowired
	private JwtProvider jwtProvider;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthService authService; 
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
    private RedisTemplate<String, Object> redisTemplate;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserVo userVo = userService.getUserVoFindByUserId(username);
		if(userVo == null) {
			throw new EntityNotFoundException("유저 정보 조회에 실패하였습니다.");
		}
		return UserPrincipal.create(userVo);
	}
	
	public UserVo getUserVoFromRedis(String tokenBody) {
		try {
            String key = "ava:userVo:" + tokenBody;
            UserVo userVo = (UserVo) redisTemplate.opsForValue().get(key);
            return userVo;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ00010", "JobScriptSaveVo 조회중 오류가 발생했습니다.");
        }
	}
	
	public boolean setUserVoFromRedis(String tokenBody, UserVo userVo) {
        try {
        	String key = "ava:userVo:" + tokenBody;
        	if(redisTemplate.hasKey(key)) {
        		redisTemplate.delete(key);
        	}
            redisTemplate.opsForValue().set(key, userVo);
            redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusMinutes(30)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
        }
    }
	public boolean validateJwtToken(String tokenBody, HttpServletRequest httpServletRequest) {
		return jwtProvider.validateJwtToken(tokenBody, httpServletRequest);
	}
	public Claims getClaims(String tokenBody) {
		return jwtProvider.getClaims(tokenBody);
	}
	
	public boolean saveRefreshToken(Long userNo, String userId, AVAAuthData authData) {
		return authService.saveRefreshToken(userNo, userId, authData.getRefresh_token(), authData.getRefresh_token_expire());
	}
	
	public AVAAuthData successfulAuthenticationResponse(HttpServletRequest request, HttpServletResponse response, Authentication authResult, Long userNo, String userId) {
		return authService.makejwtToken(userNo, userId);
	}
	
	public String makeSuccessfulAuthenticationResponseBody(UserVo userVo, AVAAuthData authData) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		String responseString = "{}";
		try {
			responseMap.put("userInfo", AvaUser.of(userVo));
			responseMap.put("tokenInfo", authData);
			responseString = objectMapper.writeValueAsString(responseMap);
		} catch (JsonProcessingException e) { }
		return responseString;
	}
	
	public String unsuccessfulAuthenticationResponse(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
		String errorEngMessage = null;
		String errorKorMessage = null;
		
		Map<String, String> responseMap = new HashMap<String, String>();
		
		//org.springframework.security.authentication.BadCredentialsException: Bad credentials
		if(exception instanceof BadCredentialsException || exception instanceof InternalAuthenticationServiceException) {
			errorEngMessage = "BadCredentialsException: Bad credentials";
			errorKorMessage = "아이디나 비밀번호가 맞지 않습니다. 다시 확인해 주십시오.";
		}
		else if(exception instanceof DisabledException) {
			//org.springframework.security.authentication.DisabledException: User is disabled
			errorEngMessage = "DisabledException: User is disabled";
			errorKorMessage = "계정이 비활성화 되었습니다. 관리자에게 문의하세요.";
		}
		else if(exception instanceof CredentialsExpiredException) {
			errorEngMessage = "CredentialsExpiredException: User is disabled";
			errorKorMessage = "비밀번호 유효기간이 만료 되었습니다. 관리자에게 문의하세요.";
		}
		else {
			errorEngMessage = "AuthenticationException: Service Error";
			errorKorMessage = "알수 없는 이유로 로그인에 실패하였습니다. 관리자에게 문의하세요.";
		}
		log.info("Authentication request failed: " + exception.toString(), exception);
		responseMap.put("errorEngMessage", errorEngMessage);
		responseMap.put("errorKorMessage", errorKorMessage);
		
		String responseString = "{}";
		try {
			responseString = objectMapper.writeValueAsString(responseMap);
		} catch (JsonProcessingException e) { }
		
		return responseString;
	}
}

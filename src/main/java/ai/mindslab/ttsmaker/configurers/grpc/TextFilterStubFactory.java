package ai.mindslab.ttsmaker.configurers.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.textfilter.TextFilterGrpc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * ai.mindslab.ttsmaker.configurers.grpc
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-09  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-09
 */
@Component
public class TextFilterStubFactory {

    private final ManagedChannel channel;
    private final TextFilterGrpc.TextFilterBlockingStub blockingStub;
    private final TextFilterGrpc.TextFilterStub asyncStub;
    private final TextFilterGrpc.TextFilterFutureStub futureStub;

    public TextFilterStubFactory(@Value("${spring.grpc.host}") String host, @Value("${spring.grpc.port}") int port) {
        this.channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        this.blockingStub = TextFilterGrpc.newBlockingStub(channel);
        this.asyncStub = TextFilterGrpc.newStub(channel);
        this.futureStub = TextFilterGrpc.newFutureStub(channel);
    }

    public TextFilterGrpc.TextFilterBlockingStub getBlockingStub() {
        return blockingStub;
    }

    public TextFilterGrpc.TextFilterStub getAsyncStub() {
        return asyncStub;
    }

    public TextFilterGrpc.TextFilterFutureStub getFutureStub() {
        return futureStub;
    }

}

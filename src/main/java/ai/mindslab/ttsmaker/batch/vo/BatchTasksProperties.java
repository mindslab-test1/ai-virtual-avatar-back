package ai.mindslab.ttsmaker.batch.vo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ava.batch.tasks")
public class BatchTasksProperties {
	private String routineBill;
	private String serviceEndTask;
	
	

}

package ai.mindslab.ttsmaker.batch.controller;

import ai.mindslab.ttsmaker.api.admin.service.AdminService;
import ai.mindslab.ttsmaker.api.pay.domain.Billing;
import ai.mindslab.ttsmaker.api.pay.dto.BillingDto;
import ai.mindslab.ttsmaker.api.pay.service.PayService;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.service.UserService;
import ai.mindslab.ttsmaker.batch.service.BatchService;
import ai.mindslab.ttsmaker.batch.vo.BatchProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Slf4j
@Controller
public class BatchController {

    @Autowired
    private BatchProperties batchProperties;

    @Autowired
    private PayService payService;

    @Autowired
    private UserService userService;

    @Autowired
    private BatchService batchService;

    @Autowired
    private AdminService adminService;

    /*
     * Batch 주기 결제 - 12시간마다
     */
    @Scheduled(cron = "${ava.batch.tasks.routineBill}")
//    @Scheduled(cron = "*/10 * * * * ?")
    public void routineBill() {
        if ("start".equals(batchProperties.getStatus())) {
            log.info("BATCH SCHEDULE(routineBill) START");

            Collection<BillingDto> routineBillings = payService.getRoutineBilling(LocalDateTime.now());
            for (BillingDto routineBilling : routineBillings) {
                try {
                    String paymentMethodStr = payService.getPaymentMethod(routineBilling.getPayNo());
                    if ("Card".equalsIgnoreCase(paymentMethodStr)) {
                        log.info("Starting routine bill for user --> {}", routineBilling.getUserEmail());
                        payService.routineBill(routineBilling);
                    }
                } catch (Exception e) {
                    log.error("BATCH SCHEDULE(routineBill) --> routineBill exception " + e);
                }
            }

            List<Billing> inactiveBillings = payService.getInactiveBillings();
            for (Billing inactiveBilling : inactiveBillings) {
                try {
                    batchService.handleInactiveBilling(inactiveBilling);
                } catch (Exception e) {
                    log.error("BATCH SCHEDULE(routineBill) --> deactivate exception " + e);
                }
            }
        } else {
            //log.info("BATCH SCHEDULE(routineBill) STOP");
        }
    }

    /*
     * Batch 주기 결제 - 12시간마다
     */
    @Scheduled(cron = "${ava.batch.tasks.routineBill}")
//    @Scheduled(cron = "*/10 * * * * ?")
    public void routineAvaWelcome() {
        if ("start".equals(batchProperties.getStatus())) {
            log.info("BATCH SCHEDULE(routineBill) START");

            Collection<BillingDto> routineBillings = payService.getRoutineBilling(LocalDateTime.now());
            for (BillingDto routineBilling : routineBillings) {
                try {
                    String paymentMethodStr = payService.getPaymentMethod(routineBilling.getPayNo());
                    if ("Card".equalsIgnoreCase(paymentMethodStr)) {
                        log.info("Starting routine bill for user --> {}", routineBilling.getUserEmail());
                        payService.routineBill(routineBilling);
                    }
                } catch (Exception e) {
                    log.error("BATCH SCHEDULE(routineBill) --> routineBill exception " + e);
                }
            }

            List<Billing> inactiveBillings = payService.getInactiveBillings();
            for (Billing inactiveBilling : inactiveBillings) {
                try {
                    batchService.handleInactiveBilling(inactiveBilling);
                } catch (Exception e) {
                    log.error("BATCH SCHEDULE(routineBill) --> deactivate exception " + e);
                }
            }
        } else {
            //log.info("BATCH SCHEDULE(routineBill) STOP");
        }
    }

    /*
     * Batch 구독 해지 - 주기 : 1시간마다
     */
    @Scheduled(cron = "${ava.batch.tasks.serviceEndTask}")
//    @Scheduled(cron = "*/10 * * * * ?")
    public void batchUnsubscribe() {
        if ("start".equals(batchProperties.getStatus())) {
            log.info("BATCH SCHEDULE(batchUnsubscribe) START");

            try {
                List<UserM> unsubscribingUsers = userService.getUnsubscribingUsers();

                for (UserM unsubscribingUser : unsubscribingUsers) {
                    log.info("Unsubscribing user --> {}", unsubscribingUser.getUserId());

                    // Inactive billings
                    List<Billing> inactiveBillings = payService.getInactiveBillingsByUserNo(unsubscribingUser.getUserNo());

                    LocalDate currentDate = LocalDate.now();

                    inactiveBillings.stream().forEach(billing -> {
                        log.info("Expiration date [{}] || Current date [{}] --> user : {}", billing.getPaymentDate(), currentDate, unsubscribingUser.getUserId());

                        //만료 날짜, 현재 날짜 비교 (만료 날짜가 지났을 경우 해지)
                        if (currentDate.compareTo(billing.getPaymentDate().toLocalDate()) > 0) {
                            batchService.unsubscribeUserFromPlan(unsubscribingUser, billing);
                        }
                    });

                }
            } catch (Exception e) {
                log.info("BATCH SCHEDULE(batchUnsubscribe) --> unsubscribe exception " + e);
            }
        } else {
            //log.info("BATCH SCHEDULE (batchUnsubscribe) STOP");
        }
    }

    /*
     * Batch 5일동안 미결제 사용자에 대한 해지 - 주기 : 12시간마다
     */
    @Transactional
    @Scheduled(cron = "${ava.batch.tasks.routineBill}")
//    @Scheduled(cron = "0/20 * * * * ?")
    public void batchUnpaymentUser() {
        if ("start".equals(batchProperties.getStatus())) {
            log.info("BATCH SCHEDULE(batchUnpaymentUser) START");
            try {
                List<Billing> unpaidBills = payService.getUnpaidBills();

                for (Billing unpaidBill : unpaidBills) {
                   batchService.handleUnpaidBilling(unpaidBill);
                }
            } catch (Exception e) {
                log.error("Batch schedule (batchUnpaymentUser) Exception!!" + e);
            }
        } else {
            //log.info("BATCH SCHEDULE(batchUnpaymentUser) STOP");
        }
    }

    /*
     * Batch 통계데이터 적재 - 주기 : 매일 23시 57분
     */
    @Transactional
    @Scheduled(cron = "${ava.batch.tasks.saveStatisticsData}")
    public void batchSaveStatisticsData() {
        try{
            adminService.setStatisticsMain();
        }catch (Exception e){
            log.error("Batch schedule (batchSaveStatisticsData) Exception!!" + e);
            e.printStackTrace();
        }
    }

}

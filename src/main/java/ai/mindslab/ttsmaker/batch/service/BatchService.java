package ai.mindslab.ttsmaker.batch.service;

import ai.mindslab.ttsmaker.api.pay.domain.Billing;
import ai.mindslab.ttsmaker.api.user.domain.UserM;

import javax.transaction.Transactional;

public interface BatchService {
    void unsubscribeUserFromPlan(UserM user, Billing billing);

    @Transactional
    void handleUnpaidBilling(Billing unpaidBill);

    @Transactional
    void handleInactiveBilling(Billing inactiveBill);
}

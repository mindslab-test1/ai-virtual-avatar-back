package ai.mindslab.ttsmaker.batch.service;

import ai.mindslab.ttsmaker.api.pay.domain.Billing;
import ai.mindslab.ttsmaker.api.pay.service.PayService;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserTimeline;
import ai.mindslab.ttsmaker.api.user.enums.SubscriptionStatus;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import ai.mindslab.ttsmaker.api.user.repository.UserTimelineRepository;
import ai.mindslab.ttsmaker.api.user.service.UserPlanService;
import ai.mindslab.ttsmaker.api.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Slf4j
@Service
public class BatchServiceImpl implements BatchService{
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private PayService payService;

    @Autowired
    private UserPlanService userPlanService;

    @Autowired
    private UserTimelineRepository userTimelineRepository;
    
    private static Logger logger = LoggerFactory.getLogger(BatchService.class.getName());
    
    @Override
    @Transactional
    public void unsubscribeUserFromPlan(UserM user, Billing billing) {
        try {
            userService.resetUserPlanCancelReq(user.getUserNo());
            UserPlan userPlan = payService.getUserPlanByPayNo(billing.getPayNo());
            userPlanService.deactivateUserPlan(userPlan);
            log.info("User plan deactivated --> userPlanNo: {} for user : {} ", userPlan.getNo(), user.getUserId());
        
            payService.deleteBillInfo(billing.getNo());
            log.info("Bill deleted --> billNo: {} for user : {} ", billing.getNo(), user.getUserId());
            userService.deleteUserRole(user, UserRoleEnum.ROLE_DOWNLOAD);

            UserTimeline userTimeline = userService.addUserTimeline(user, userPlan, "구독 취소", SubscriptionStatus.CANCEL, false);

            log.info("userTimeline[{}] >> 유저 타임라인 구독 취소 정보 저장 완료. User = {}", userTimeline.getNo(), user.getUserNo());
        
            log.info("User role deleted --> userRole: {} for user : {} ", UserRoleEnum.ROLE_DOWNLOAD, user.getUserId());
        } catch (Exception e) {
            log.info("BATCH SCHEDULE(batchUnsubscribe) --> unsubscribe exception " + e);
        }
    }

    @Override
    @Transactional
    public void handleUnpaidBilling(Billing unpaidBill){
        UserM unpaidUser = userService.getUserByUserNo(unpaidBill.getUserNo());
        log.info("Unpaid user --> {}", unpaidUser.getUserId());

        userService.deleteUserRole(unpaidUser, UserRoleEnum.ROLE_DOWNLOAD);
        log.info("User role deleted --> userRole: {} for user : {} ", UserRoleEnum.ROLE_DOWNLOAD, unpaidUser.getUserId());

        unpaidUser.getUserPlans()
                .stream()
                .filter(userPlan -> userPlan.getActive() && userPlan.getPlanEndAt().compareTo(LocalDateTime.now()) < 0)
                .forEach(userPlan -> {
                    userPlanService.deactivateUserPlan(userPlan);
                    log.info("User plan deactivated --> userPlanNo : {} for user :{}", userPlan.getNo(), unpaidUser.getUserId());

                    UserTimeline userTimeline = userService.addUserTimeline(unpaidUser, userPlan, "구독 강제 취소", SubscriptionStatus.FAIL, false);

                    log.info("userTimeline[{}] >> 유저 타임라인 구독 강제 취소 정보 저장 완료. User = {}", userTimeline.getNo(), unpaidUser.getUserNo());

                });

        payService.deleteBillInfo(unpaidBill.getNo());
        log.info("Bill deleted --> billNo: {} for user : {} ", unpaidBill.getNo(), unpaidUser.getUserId());
    }

    @Override
    @Transactional
    public void handleInactiveBilling(Billing inactiveBill){
        UserPlan userPlan = payService.getUserPlanByPayNo(inactiveBill.getPayNo());
        if (userPlan.getPlanEndAt().compareTo(LocalDateTime.now()) < 0) {
            userPlanService.deactivateUserPlan(userPlan);
            log.info("User plan deactivated --> userPlanNo: {} for userNo : {}", userPlan.getNo(), inactiveBill.getUserNo());

            payService.deleteBillInfo(inactiveBill.getNo());
            log.info("Bill deleted --> billNo: {} for userNo : {} ", inactiveBill.getNo(), inactiveBill.getUserNo());

            UserTimeline userTimeline = userService.addUserTimeline(userPlan.getUser(), userPlan, "구독 만료", SubscriptionStatus.END, false);

            log.info("userTimeline[{}] >> 유저 타임라인 구독 강제 취소 정보 저장 완료. User = {}", userTimeline.getNo(), userPlan.getUser().getUserNo());
        }
    }
}

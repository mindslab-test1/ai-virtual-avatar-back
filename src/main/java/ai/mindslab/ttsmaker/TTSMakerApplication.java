package ai.mindslab.ttsmaker;

import org.apache.commons.compress.utils.IOUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.awt.*;
import java.io.*;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@EnableScheduling
@SpringBootApplication
public class TTSMakerApplication {
	public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException, IOException {
		Map<String, String> google = new HashMap<>();
		InputStream inputStream = new ClassPathResource("google_credentials.json").getInputStream();
		File file = File.createTempFile("google_credentials", ".json");
		try {
			copyInputStreamToFile(inputStream, file);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
		System.out.println(file.getPath());
		google.put("GOOGLE_APPLICATION_CREDENTIALS", file.getPath());
		setEnv(google);
		SpringApplication.run(TTSMakerApplication.class, args);
	}

	private static void copyInputStreamToFile(InputStream inputStream, File file) throws IOException {
		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			int read;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		}
	}

	public static void setEnv(Map<String, String> newenv)
			throws ClassNotFoundException, IllegalAccessException, NoSuchFieldException {
		try {
			Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
			Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
			theEnvironmentField.setAccessible(true);
			Map<String, String> env = (Map<String, String>) theEnvironmentField.get(null);
			env.putAll(newenv);
			Field theCaseInsensitiveEnvironmentField = processEnvironmentClass
					.getDeclaredField("theCaseInsensitiveEnvironment");
			theCaseInsensitiveEnvironmentField.setAccessible(true);
			Map<String, String> cienv = (Map<String, String>) theCaseInsensitiveEnvironmentField.get(null);
			cienv.putAll(newenv);
		} catch (NoSuchFieldException e) {
			Class[] classes = Collections.class.getDeclaredClasses();
			Map<String, String> env = System.getenv();
			for (Class cl : classes) {
				if ("java.util.Collections$UnmodifiableMap".equals(cl.getName())) {
					Field field = cl.getDeclaredField("m");
					field.setAccessible(true);
					Object obj = field.get(env);
					Map<String, String> map = (Map<String, String>) obj;
					map.clear();
					map.putAll(newenv);
				}
			}
		}
	}

}

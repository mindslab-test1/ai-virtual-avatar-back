package ai.mindslab.ttsmaker.api.plan.controller;

import java.util.List;

import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.service.UserPlanService;
import ai.mindslab.ttsmaker.api.user.vo.response.UserPlanListResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ai.mindslab.ttsmaker.api.plan.service.PlanService;
import ai.mindslab.ttsmaker.api.plan.vo.response.PlanListResponseVo;

@RestController
@RequestMapping("/plan")
public class PlanController {

    @Autowired
    private PlanService planService;

    @Autowired
    private UserPlanService userPlanService;

    /***
     * <pre>
     * 구독 상품 리스트 조회
     * </pre>
     * @return
     */
    @ResponseBody
    @GetMapping("/list")
    public List<PlanListResponseVo> getList() {
        return planService.getList();
    }

    @ResponseBody
    @GetMapping("/{userNo}/active")
    public List<UserPlanListResponseVo> getUserPlanList(@PathVariable Long userNo) {
        return userPlanService.getUserPlanListResponseVoFindByUserNo(userNo);
    }

}

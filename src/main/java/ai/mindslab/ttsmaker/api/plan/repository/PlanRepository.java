package ai.mindslab.ttsmaker.api.plan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Long> {
    @Query("select p from Plan p where p.exposeYn = 'Y'")
    List<Plan> findPlanListByExposed();

    @Query("select p from Plan p where p.planNm = ?1")
    Plan findPlanByPlanName(String planName);

    @Query("select p from Plan p where p.planNo = ?1")
    Plan findPlanByPlanNo(Long planNo);

    @Query("select p from Plan p where p.planPrice = ?1")
    Plan findPlanByPlanPrice(Integer price);

    @Query("select p.maxLimit from Plan p where p.planPrice = ?1")
    Integer findMaxLimitByPlanPrice(Integer price);
}

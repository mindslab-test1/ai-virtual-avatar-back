package ai.mindslab.ttsmaker.api.plan.vo.response;

import java.util.List;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlanListResponseVo {
	private Long planNo;
	private String planNm;
	private String planNmKr;
	private Integer planPrice;
	private Integer maxLimit;
	private List<String> planBenefit;
	
	public static PlanListResponseVo of(Plan entity) {
        return PlanListResponseVo.builder()
        		.planNo(entity.getPlanNo())
        		.planNm(entity.getPlanNm())
        		.planNmKr(entity.getPlanNmKr())
        		.planPrice(entity.getPlanPrice())
        		.maxLimit(entity.getMaxLimit())
        		.planBenefit(entity.getPlanBenefit())
        		.build();
    }
}
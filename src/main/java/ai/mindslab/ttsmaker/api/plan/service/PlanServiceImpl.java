package ai.mindslab.ttsmaker.api.plan.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.plan.repository.PlanRepository;
import ai.mindslab.ttsmaker.api.plan.vo.response.PlanListResponseVo;

@Service
public class PlanServiceImpl implements PlanService {
	
	@Autowired
	private PlanRepository planRepository;

	@Override
	public List<PlanListResponseVo> getList() {
		
		List<Plan> list = planRepository.findPlanListByExposed();
		return list.stream().map(PlanListResponseVo::of).collect(Collectors.toList());
	}
	
	@Override
	public Plan getPlan(Long planNo) {
		return planRepository.getOne(planNo);
	}
}

package ai.mindslab.ttsmaker.api.plan.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.configurers.converter.PlanBenefitConverter;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "PLAN")
public class Plan implements Serializable {

	private static final long serialVersionUID = 2082580099679053178L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long planNo;

    @Column(name = "PLAN_NM", columnDefinition = "varchar(100)")
    private String planNm;

    @Column(name = "PLAN_NM_KR", columnDefinition = "varchar(100)")
    private String planNmKr;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Integer planPrice;

    @Column(name = "MAX_LIMIT", columnDefinition = "int(11)")
    private Integer maxLimit;
    
    @Column(name = "PLAN_BENEFIT", columnDefinition="json default '{}'")
    @Convert(converter = PlanBenefitConverter.class)
    private List<String> planBenefit;
    
    @Column(name = "EXPOSE_YN", columnDefinition = "char(1) default 'N'")
    private String exposeYn;

    @Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;
    
    @OneToMany(mappedBy = "plan", fetch = FetchType.LAZY)
    private List<UserPlan> userPlans = new ArrayList<UserPlan>();

    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //등록자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CREATED_BY")
    private Long createdBy;

    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
    
    //수정자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UPDATED_BY")
    private Long updatedBy;
}

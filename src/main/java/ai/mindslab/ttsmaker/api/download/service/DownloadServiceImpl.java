package ai.mindslab.ttsmaker.api.download.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.service.VFCharacterService;
import ai.mindslab.ttsmaker.api.download.domain.DownloadD;
import ai.mindslab.ttsmaker.api.download.domain.DownloadM;
import ai.mindslab.ttsmaker.api.download.enums.DownloadFileFormatEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadMethodEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadOutputTypeEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadProcessStatusEnum;
import ai.mindslab.ttsmaker.api.download.exception.DownloadException;
import ai.mindslab.ttsmaker.api.download.repository.DownloadDRepository;
import ai.mindslab.ttsmaker.api.download.repository.DownloadMRepository;
import ai.mindslab.ttsmaker.api.download.vo.request.DownloadProcessRequestVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadListContentsBodyVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadListContentsResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadListContentsUserPlanVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadRequestCalcResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadRequestResultResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareContentsResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadSharePreferVFCharacterVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareViewContentsBodyVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareViewContentsResponseVo;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.job.service.JobService;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;
import ai.mindslab.ttsmaker.api.provider.aws.lambda.properties.AWSLambdaConfigProperties;
import ai.mindslab.ttsmaker.api.provider.aws.lambda.service.AWSLambdaService;
import ai.mindslab.ttsmaker.api.provider.aws.lambda.vo.AWSLambdaResponseMessage;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiProperties;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.service.UserPlanService;
import ai.mindslab.ttsmaker.common.AVAConst;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.common.page.AvaPageImpl;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DownloadServiceImpl implements DownloadService {
	
	@Autowired
	private DownloadMRepository downloadMRepository;
	
	@Autowired
	private DownloadDRepository downloadDRepository;
	
	@Autowired
    private VFCharacterService vfCharacterService;
	
	@Autowired
	private AWSLambdaConfigProperties awsLambdaConfigProperties;
	
	@Autowired
    private TTSApiProperties ttsApiProperties;
	
	@Autowired
	private AWSLambdaService awsLambdaService; 
	
	@Autowired
    private JobService jobService;
	
	@Autowired
	private UserPlanService userPlanService;
		
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public DownloadListContentsResponseVo getDownloadList(AvaUser avaUser, PageRequest pageable) {
		Page<DownloadM> page = downloadMRepository.getActiveDownloadFindByUserNo(avaUser.getUserNo(), pageable);
		List<DownloadListContentsBodyVo> list = page.getContent().stream().map(DownloadListContentsBodyVo::of).collect(Collectors.toList());
		AvaPage<DownloadListContentsBodyVo> pageContents = new AvaPageImpl<>(list, pageable, page.getTotalElements());

		List<UserPlan> userPlanList = userPlanService.getUserPlanListFindByUserNo(avaUser.getUserNo());
		List<DownloadListContentsUserPlanVo> plans = Lists.newArrayList();
		Integer totalCharUseCnt = 0;
		Integer totalCharMaxLimit = 0;
		if(!CollectionUtils.isEmpty(userPlanList)) {
			plans.addAll(userPlanList.stream().map(DownloadListContentsUserPlanVo::of).collect(Collectors.toList()));
			for(DownloadListContentsUserPlanVo row : plans) {
				totalCharUseCnt += row.getCharUseCnt();
				totalCharMaxLimit += row.getCharMaxLimit();
			}
		}
		return DownloadListContentsResponseVo.builder()
				.totalCharUseCnt(totalCharUseCnt)
				.totalCharMaxLimit(totalCharMaxLimit)
				.plans(plans)
				.pageContents(pageContents)
				.build();
	}
	
	@Override
	@Transactional
	public DownloadRequestResultResponseVo requestDownload(AvaUser avaUser, DownloadProcessRequestVo requestDto) {
		DownloadRequestResultResponseVo response = null;
		try {
			log.info("requestDownloadId: {}, download  ==> {}", requestDto.getDownloadId(), requestDto);
			
			Long userNo = avaUser.getUserNo();

			// 유효성 검사(파라미터, 구독 상품의 잔여 글자 체크)
			validationDownloadProcess(requestDto, userNo);

			// 변환 대상 저장 
			saveAsRequestDownload(requestDto, userNo);
			
			// lambda function 데이터 가공 및 요청
			Map<String, String> payloadMap = new HashMap<>();
			payloadMap.put("DOWNLOAD_ID", requestDto.getDownloadId());
			payloadMap.put("TTS_API_URL", ttsApiProperties.getApiHost() + ttsApiProperties.getApiStreamUrl());
			payloadMap.put("TTS_API_ID", ttsApiProperties.getApiId());
			payloadMap.put("TTS_API_KEY", ttsApiProperties.getApiKey());
			
			String region = awsLambdaConfigProperties.getRegion();
			String functionNm = awsLambdaConfigProperties.getFunctionNm();
			
			AWSLambdaResponseMessage message = awsLambdaService.invokeRequestAsync(region, functionNm, payloadMap);
			if(message.getStatus() == 500) {
				throw new DownloadException("ERRD0005", "AWS Lambda 람다 에러");
			}
			response = DownloadRequestResultResponseVo.of(DownloadProcessStatusEnum.A, "다운로드 요청 완료하였습니다.");
			
		}catch(DownloadException ex) {
			response = DownloadRequestResultResponseVo.of(DownloadProcessStatusEnum.D, String.format("다운로드 요청중 오류가 발생했습니다.([%s]%s)", ex.getErrorCode(), ex.getErrorMessage()));
		}catch(Exception ex) {
			response = DownloadRequestResultResponseVo.of(DownloadProcessStatusEnum.D, String.format("다운로드 요청중 오류가 발생했습니다.(%s)", ex.getMessage()));
		}
		return response;
	}
	
	private void validationDownloadProcess(DownloadProcessRequestVo requestDto, Long userNo) throws Exception {
		String paramJobId = requestDto.getJobId();
		String paramDownloadId = requestDto.getDownloadId();
		List<String> paramJobScriptIds = requestDto.getJobScriptIds();
		
		if(StringUtils.isEmpty(paramJobId)) {
			throw new DownloadException("ERRD0001", "JobId param 오류");
		}
		
		// 이미 요청된경우 
		boolean exists = downloadMRepository.existsByDownloadId(paramDownloadId);
		if(exists) {
			throw new DownloadException("ERRD0002", "중복 요청");
		}
		
		// 변환 대상이 없는 경우
		if (CollectionUtils.isEmpty(paramJobScriptIds)) {
			throw new DownloadException("ERRD0003", "변환 대상 없음");
		}
		
		DownloadRequestCalcResponseVo calResponse = calcRequestDownloadProcess(paramJobId, userNo, paramJobScriptIds);
		
		if(calResponse.isDownloadProcessFlg() == false) {
			//String status_message = String.format("변환에 필요한 글자수(%d)가 부족합니다. 글자 현황(요청:%d, 잔여:%d, 진행중:%d)", needCharCnt, downloadExpectCharCnt, remainingCharCnt, convertInProgressExpectDownloadCharCnt - convertInProgressDownloadCharCnt);
			throw new DownloadException("ERRD0004", String.format("변환에 필요한 글자수(%d)가 부족합니다.", calResponse.getDownloadProcessNeedCharCnt()));		
		}
	}
	
	private DownloadRequestCalcResponseVo calcRequestDownloadProcess(String jobId, Long userNo, List<String> jobScriptIds) {
		Map<String, JobScriptVo> jobScriptMap = jobService.getJobScriptMapFromRedis(jobId);
		List<UserPlan> userPlanList = userPlanService.getUserPlanListFindByUserNo(userNo);
		Long totalPlanCharUseCnt = 0l;
		Long totalPlanCharMaxLimit = 0l;
		if(!CollectionUtils.isEmpty(userPlanList)) {
			for(UserPlan row : userPlanList) {
				totalPlanCharUseCnt += row.getCharUseCnt();
				totalPlanCharMaxLimit += row.getCharMaxLimit();
			}
		}
		
		// 사용 가능한 글자수 체크 
		Long downloadExpectCharCnt = 0l;
		for(String jobScriptId : jobScriptIds) {
			if(jobScriptMap.containsKey(jobScriptId)) {
				JobScriptVo jobScriptVo = jobScriptMap.get(jobScriptId);
				String scriptText = StringUtils.nvl(jobScriptVo.getScriptText(), "").trim();
				scriptText = scriptText.replaceAll(AVAConst.SCRIPT_COUNT_PATTERN, "");
				downloadExpectCharCnt += scriptText.length();
			}
		}
		// 변환진행인 다운로드 대상(예상 글자, 진행된 글자 수)  
		Long totalConvertInProgressExpectDownloadCharCnt = 0l;
		Long totalConvertInProgressDownloadCharCnt = 0l;
		List<DownloadM> convertList = downloadMRepository.getConvertInProgressListFindByUserNo(userNo);
		if(!CollectionUtils.isEmpty(convertList)) {
			for(DownloadM convertInProgressRow : convertList) {
				totalConvertInProgressExpectDownloadCharCnt += convertInProgressRow.getDownloadExpectCharCnt();
				totalConvertInProgressDownloadCharCnt += convertInProgressRow.getDownloadCharCnt();
			}
		}
		
		Long remainingCharCnt = totalPlanCharMaxLimit - totalPlanCharUseCnt;
		// (전체 변환 중인 예상 글수 - 전체 변환 진행된 글자 수) - 요청된 글자수 
		Long candidatesCanBeUsedCnt = (totalConvertInProgressExpectDownloadCharCnt - totalConvertInProgressDownloadCharCnt) + downloadExpectCharCnt;
		// 변환 가능여부 : 전체 가용 글자 수 - 사용된 글자수 - (전체 변환 중인 예상 글수 - 진행된 글자 수)
		boolean downloadProcessFlg = remainingCharCnt > candidatesCanBeUsedCnt;
		Long downloadProcessNeedCharCnt = 0l;
		if(downloadProcessFlg == false) {
			downloadProcessNeedCharCnt = candidatesCanBeUsedCnt - remainingCharCnt;
		}
		
		return DownloadRequestCalcResponseVo.builder()
				.totalPlanCharUseCnt(totalPlanCharUseCnt)
				.totalPlanCharMaxLimit(totalPlanCharMaxLimit)
				.totalConvertInProgressExpectDownloadCharCnt(totalConvertInProgressExpectDownloadCharCnt)
				.totalConvertInProgressDownloadCharCnt(totalConvertInProgressDownloadCharCnt)
				.downloadExpectCharCnt(downloadExpectCharCnt)
				.remainingCharCnt(remainingCharCnt)
				.downloadProcessFlg(downloadProcessFlg)
				.downloadProcessNeedCharCnt(downloadProcessNeedCharCnt)
				.build();
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void saveAsRequestDownload(DownloadProcessRequestVo requestDto, Long userNo) {
		String paramJobId = requestDto.getJobId();
		String paramDownloadId = requestDto.getDownloadId();
		String paramDownloadTitle = requestDto.getDownloadTitle();
		String paramDownloadMethod = requestDto.getDownloadMethod();
		String paramFileFormat = requestDto.getFileFormat();
		List<String> paramJobScriptIds = requestDto.getJobScriptIds();
		DownloadMethodEnum downloadMethod = DownloadMethodEnum.findBy(paramDownloadMethod);
		DownloadFileFormatEnum fileFormat = DownloadFileFormatEnum.findBy(paramFileFormat);
		
		// DownloadM 데이터 가공 및 저장
		DownloadM downloadM = new DownloadM();
		downloadM.setDownloadId(paramDownloadId);
		downloadM.setDownloadTitle(paramDownloadTitle);
		downloadM.setDownloadMethod(downloadMethod);
		downloadM.setFileFormat(fileFormat);
		downloadM.setUserNo(userNo);
		downloadM.setOutputType(DownloadOutputTypeEnum.AWS_S3);
		downloadM.setProcessStep(0);
		downloadM.setProcessTotalStep(0);
		downloadM.setProcessStatus(DownloadProcessStatusEnum.A); // 처리 진행 상태(A:대기,B:진행중,C:완료,D:오류
		downloadM.setDownloadCharCnt(0);
		downloadM.setActive(true);
		
		// DownloadM 데이터 저장 
		downloadM = downloadMRepository.save(downloadM);
		
		// 보이스폰트 조회(Map)
		Map<String, VFCharacter> charactersMap = vfCharacterService.getVFCharactersMap();
		
		// 임시저장된 Job Script 조회(Map)
		Map<String, JobScriptVo> jobScriptMap = jobService.getJobScriptMapFromRedis(paramJobId);
		
		
		Long seq = 0l;
		Integer downloadExpectCharCnt = 0;
		List<DownloadD> downloadDList = Lists.newArrayList();
		for(String jobScriptId : paramJobScriptIds) {
			if(jobScriptMap.containsKey(jobScriptId)) {
				JobScriptVo jobScriptVo = jobScriptMap.get(jobScriptId);
				String scriptText = StringUtils.nvl(jobScriptVo.getScriptText(), "").trim();
				downloadExpectCharCnt += scriptText.replaceAll(AVAConst.SCRIPT_COUNT_PATTERN, "").length();
				
				DownloadD downloadD = new DownloadD();
				downloadD.setDownloadM(downloadM);
				downloadD.setSeq(++seq);
				downloadD.setUserNo(userNo);
				
				downloadD.setVfCharacter(charactersMap.get(jobScriptVo.getCharacterId()));
				downloadD.setScriptText(scriptText);
				if(jobScriptVo.getTtsResource() != null) {
					downloadD.setTtsResource(jobScriptVo.getTtsResource());
					downloadD.setTtsPlayTime(jobScriptVo.getTtsPlayTime());
					downloadD.setProcessStatus(DownloadProcessStatusEnum.D);
				}else {
					downloadD.setProcessStatus(DownloadProcessStatusEnum.A);
				}
				
				
				downloadD.setActive(true);
				
				downloadDList.add(downloadD);
			}
		}
		downloadDRepository.saveAll(downloadDList);
		
		// 예상 글자 수 적용
		downloadM.setDownloadExpectCharCnt(downloadExpectCharCnt);
		downloadMRepository.save(downloadM);
		em.flush();
	}

	@Override
	public DownloadRequestCalcResponseVo checkForPreRequestDownload(Long userNo, DownloadProcessRequestVo requestDto) {
		String paramJobId = requestDto.getJobId();
		List<String> paramJobScriptIds = requestDto.getJobScriptIds();
		
		return calcRequestDownloadProcess(paramJobId, userNo, paramJobScriptIds);
	}

	@Override
	public DownloadShareContentsResponseVo getSNSSharePreProcess(AvaUser avaUser, String downloadId) {
		// 보이스폰트 조회(Map)
        Map<String, VFCharacter> charactersMap = vfCharacterService.getVFCharactersMap();
        
        DownloadM downloadM = downloadMRepository.findByDownloadId(downloadId);
        
        if(downloadM == null) {
        	throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRD0006", String.format("DOWNLOAD ID(%s) 데이터가 존재 하지 않습니다.", downloadId));
        }
        		
        // 해당 DOWNLOAD ID의 DOWNLOAD_D 조회
        List<DownloadD> details = downloadM.getDetails();
        
        List<DownloadSharePreferVFCharacterVo> preferVFList = Lists.newArrayList();
     	
     	// 미리보기 데이터 가공
        if (!CollectionUtils.isEmpty(details)) {
        	preferVFList.addAll(getPreferVFList(charactersMap, details));
        }
		return DownloadShareContentsResponseVo.of(downloadM, preferVFList);
	}
	
	/**
	 * <pre>
	 * JobScript 캐릭터 선호도 랭킹 가공 (최대 8건 까지)
	 * <pre>
	 * @param charactersMap
	 * @param details
	 * @return
	 */
	private List<DownloadSharePreferVFCharacterVo> getPreferVFList(Map<String, VFCharacter> charactersMap, List<DownloadD> details) {
		List<DownloadSharePreferVFCharacterVo> preferVFList = Lists.newArrayList();
		if (!CollectionUtils.isEmpty(details)) {
			Map<String, Long> preferVFGroupByCountMap = details.stream()
					.map(DownloadD::getVfCharacter)
					.collect(Collectors.groupingBy(VFCharacter::getCharacterId, Collectors.counting()));
			Set<String> characterIds = charactersMap.keySet();
			preferVFList.addAll(preferVFGroupByCountMap.entrySet().stream()
					.sorted(Map.Entry.<String, Long>comparingByValue().reversed())
					.filter(m->characterIds.contains(m.getKey()))
					.map(p->DownloadSharePreferVFCharacterVo.of(charactersMap.get(p.getKey()), p.getValue()))
					.collect(Collectors.toList()));

			// 8건+ 이상 경우 8건 까지 노출
			final long EXPOSE_LIST_SIZE = 8l;
			if(preferVFList.size() > EXPOSE_LIST_SIZE) {
				preferVFList = preferVFList.stream().limit(EXPOSE_LIST_SIZE).collect(Collectors.toList());
			}
		}
		return preferVFList;
	}

	@Override
	public DownloadShareViewContentsResponseVo getSNSShareDownloadView(AvaUser avaUser, String downloadId) {
		 DownloadM downloadM = downloadMRepository.findByDownloadId(downloadId);
	        
        if(downloadM == null) {
        	throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRD0007", String.format("DOWNLOAD ID(%s) 데이터가 존재 하지 않습니다.", downloadId));
        }
        
        // 해당 DOWNLOAD ID의 DOWNLOAD_D 조회
        List<DownloadShareViewContentsBodyVo> details = Lists.newArrayList();
        
        if(!CollectionUtils.isEmpty(downloadM.getDetails())) {
        	details.addAll(downloadM.getDetails().stream().map(DownloadShareViewContentsBodyVo::of).collect(Collectors.toList()));
        }
        
		return DownloadShareViewContentsResponseVo.of(downloadM, details);
	}
}

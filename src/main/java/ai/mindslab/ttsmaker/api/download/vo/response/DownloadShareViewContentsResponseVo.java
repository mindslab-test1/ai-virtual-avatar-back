package ai.mindslab.ttsmaker.api.download.vo.response;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.download.domain.DownloadM;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadShareViewContentsResponseVo {
	private Long downloadNo;
	private String downloadId;
	private String downloadTitle;				// 다운로드 제목
	private String downloadMethod;				// 다운로드 유형(0:개별 파일다운로드, 1:전체 파일다운로드)
	private String fileFormat;					// 파일 포맷
	private Float totalDurationSecs;			// 총 재생시간
	private Integer downloadCharCnt;			// 다운로드 글자 수
	private String objectUrl;					// 노출 경로
	private Boolean active;						// 비활성화 여부
	
	private List<DownloadShareViewContentsBodyVo> details;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
    private LocalDateTime createdAt;			// 등록 일자
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
	private LocalDateTime updatedAt;			// 수정 일자
	
	public static DownloadShareViewContentsResponseVo of(DownloadM entity, List<DownloadShareViewContentsBodyVo> details) {
		return DownloadShareViewContentsResponseVo.builder()
				.downloadNo(entity.getNo())
				.downloadId(entity.getDownloadId())
				.downloadTitle(entity.getDownloadTitle())
				.downloadMethod(entity.getDownloadMethod().name())
				.fileFormat(entity.getFileFormat().name())
				.totalDurationSecs(entity.getTotalDurationSecs())
				.downloadCharCnt(entity.getDownloadCharCnt())
				.objectUrl(entity.getObjectUrl())
				.active(entity.getActive())
				.details(details)
				.createdAt(entity.getCreatedAt())
				.updatedAt(entity.getUpdatedAt())
				.build();
	}
}

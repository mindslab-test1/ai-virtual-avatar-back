package ai.mindslab.ttsmaker.api.download.service;

import org.springframework.data.domain.PageRequest;

import ai.mindslab.ttsmaker.api.download.vo.request.DownloadProcessRequestVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadListContentsResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadRequestCalcResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadRequestResultResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareContentsResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareViewContentsResponseVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;

public interface DownloadService {
	/**
	 * <pre>
	 * 다운로드 리스트 조회
	 * </pre>
	 * @param avaUser
	 * @param pageable
	 * @return
	 */
	DownloadListContentsResponseVo getDownloadList(AvaUser avaUser, PageRequest pageable);

	/**
	 * <pre>
	 * 다운로드 요청 처리
	 * </pre>
	 * @param avaUser
	 * @param requestDto
	 * @return
	 */
	DownloadRequestResultResponseVo requestDownload(AvaUser avaUser, DownloadProcessRequestVo requestDto);

	/**
	 * <pre>
	 * 다운로드 처리 가용 가능한 글자수 체크(다운로드 전)
	 * </pre>
	 * @param userNo
	 * @param requestDto
	 * @return
	 */
	DownloadRequestCalcResponseVo checkForPreRequestDownload(Long userNo, DownloadProcessRequestVo requestDto);

	/**
	 * <pre>
	 * SNS 공유 사전 처리(다운로드 완료 이후)
	 * </pre>
	 * @param avaUser
	 * @param downloadId
	 * @return
	 */
	DownloadShareContentsResponseVo getSNSSharePreProcess(AvaUser avaUser, String downloadId);

	/**
	 * <pre>
	 * Download 결과 보기 + 웹 재생 + 다운로드(SNS 공유 후)
	 * </pre>
	 * @param avaUser
	 * @param downloadId
	 * @return
	 */
	DownloadShareViewContentsResponseVo getSNSShareDownloadView(AvaUser avaUser, String downloadId);
}

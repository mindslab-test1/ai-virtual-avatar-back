package ai.mindslab.ttsmaker.api.download.enums;

public enum DownloadFileFormatEnum {
	WAV,  // 개별 파일 다운로드 
    MP3; 	// 전체 파일 다운로드
    
    public static DownloadFileFormatEnum findBy(String fileFormat){
        for(DownloadFileFormatEnum v : values()){
            if(v.toString().equals(fileFormat.toUpperCase())){
                return v;
            }
        }
        return DownloadFileFormatEnum.WAV;
    }
}

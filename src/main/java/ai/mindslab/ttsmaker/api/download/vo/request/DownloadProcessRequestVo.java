package ai.mindslab.ttsmaker.api.download.vo.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadProcessRequestVo {
	private String downloadId;
	private String jobId;
	private String downloadTitle;
	private String downloadMethod;
	private String fileFormat;
	private List<String> jobScriptIds;
}

package ai.mindslab.ttsmaker.api.download.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.download.domain.DownloadM;

@Repository
public interface DownloadMRepository extends JpaRepository<DownloadM, Long> {
	boolean existsByDownloadId(String downloadId);
	DownloadM findByDownloadId(String downloadId);
	void deleteByNo(Long no);
	
	@Query("SELECT m FROM DownloadM m WHERE m.active = 1 AND m.userNo = :userNo")
	Page<DownloadM> getActiveDownloadFindByUserNo(@Param("userNo") Long userNo, Pageable pageable);
	
	@Query("SELECT m FROM DownloadM m WHERE m.active = 1 AND processStatus='B' AND m.userNo = :userNo")
	List<DownloadM> getConvertInProgressListFindByUserNo(@Param("userNo") Long userNo);
}

package ai.mindslab.ttsmaker.api.download.exception;

public class DownloadException extends Exception {

	private static final long serialVersionUID = 6495668895610764674L;
	private final String errorCode;
	private final String errorMessage;
    
    public DownloadException(String errorCode, String errorMessage) {
		super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
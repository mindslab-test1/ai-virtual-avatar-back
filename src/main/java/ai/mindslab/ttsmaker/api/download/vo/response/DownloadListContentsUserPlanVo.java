package ai.mindslab.ttsmaker.api.download.vo.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadListContentsUserPlanVo {
	//관리번호
	private Long no;
	
	// 플랜 이름
	private String planNm;
	
	// 글자 사용수
    private Integer charUseCnt;
    // 글자 최대 허용수
    private Integer charMaxLimit;
    
    // 계약 시작일시
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime planStartAt;
    
    // 계약 종료일시
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime planEndAt;
    
    public static DownloadListContentsUserPlanVo of(UserPlan entity) {
    	Plan plan = entity.getPlan();
    	return DownloadListContentsUserPlanVo.builder()
    			.no(entity.getNo())
    			.planNm(plan.getPlanNm())
    			.charUseCnt(entity.getCharUseCnt())
    			.charMaxLimit(entity.getCharMaxLimit())
    			.planStartAt(entity.getPlanStartAt())
    			.planEndAt(entity.getPlanEndAt())
    			.build();
    }
}

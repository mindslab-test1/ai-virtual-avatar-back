package ai.mindslab.ttsmaker.api.download.vo.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadRequestCalcResponseVo {
	private long totalPlanCharUseCnt;
	private long totalPlanCharMaxLimit;
	private long totalConvertInProgressExpectDownloadCharCnt;
	private long totalConvertInProgressDownloadCharCnt;
	private long downloadExpectCharCnt;
	private long remainingCharCnt;
	private boolean downloadProcessFlg;
	private long downloadProcessNeedCharCnt; 
}

package ai.mindslab.ttsmaker.api.download.vo.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.download.domain.DownloadD;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadShareViewContentsBodyVo {
	private String scriptText;			// 스크립트
	private String characterId;			// 보이스폰트 ID
	private String characterNm; 		// 보이스폰트 이름
	private String characterClassNm;	// 보이스 폰트 class이름
	private byte[] ttsResource;			// TTS WAV Array
	private Float ttsPlayTime;			// 재생시간
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
    private LocalDateTime createdAt;	// 등록 일자
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
	private LocalDateTime updatedAt;	// 수정일자
	
	
	public static DownloadShareViewContentsBodyVo of(DownloadD entity) {
		VFCharacter vfCharacter = entity.getVfCharacter();
		return DownloadShareViewContentsBodyVo.builder()
				.scriptText(entity.getScriptText())
				.characterId(vfCharacter.getCharacterId())
				.characterNm(vfCharacter.getCharacterNm())
				.characterClassNm(vfCharacter.getAvatarClassNm())
				.ttsResource(entity.getTtsResource())
				.ttsPlayTime(entity.getTtsPlayTime())
				.createdAt(entity.getCreatedAt())
				.updatedAt(entity.getUpdatedAt())
				.build();
			
	}
}

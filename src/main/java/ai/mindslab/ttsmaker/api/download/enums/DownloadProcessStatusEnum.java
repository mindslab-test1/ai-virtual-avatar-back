package ai.mindslab.ttsmaker.api.download.enums;

public enum DownloadProcessStatusEnum {
	A,  // 대기
	B, // 진행중
	C, // 완료
	D, // 완료(재새용)
	E, // 오류
	
	F, // DOWNLOAD_D 오류(TTS 변환)
	G, // DOWNLOAD_D 오류(Audio 변환)
	H, // DOWNLOAD_D 오류(AWS 변환)
	I, // 오류(기타)
	J, // 시간 경과
}

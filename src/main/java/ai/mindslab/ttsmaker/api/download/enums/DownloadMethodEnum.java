package ai.mindslab.ttsmaker.api.download.enums;

public enum DownloadMethodEnum {
	EACH,  // 개별 파일 다운로드 
    ALL; 	// 전체 파일 다운로드
    
    public static DownloadMethodEnum findBy(String method){
        for(DownloadMethodEnum v : values()){
            if(v.toString().equals(method.toUpperCase())){
                return v;
            }
        }
        return DownloadMethodEnum.EACH;
    }
}

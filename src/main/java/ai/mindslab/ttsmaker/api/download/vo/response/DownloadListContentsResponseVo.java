package ai.mindslab.ttsmaker.api.download.vo.response;

import java.util.List;

import ai.mindslab.ttsmaker.common.page.AvaPage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadListContentsResponseVo {
	private Integer totalCharUseCnt;
	private Integer totalCharMaxLimit;
	private List<DownloadListContentsUserPlanVo> plans;
	private AvaPage<DownloadListContentsBodyVo> pageContents;
}

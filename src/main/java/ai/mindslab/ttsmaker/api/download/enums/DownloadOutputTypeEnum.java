package ai.mindslab.ttsmaker.api.download.enums;

public enum DownloadOutputTypeEnum {
	VM,  // VM
	AWS_S3, // AWS_S3
}

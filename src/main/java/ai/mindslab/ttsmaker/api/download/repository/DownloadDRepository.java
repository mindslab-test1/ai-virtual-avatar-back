package ai.mindslab.ttsmaker.api.download.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.download.domain.DownloadD;

@Repository
public interface DownloadDRepository extends JpaRepository<DownloadD, Long> {

}

package ai.mindslab.ttsmaker.api.download.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.download.enums.DownloadOutputTypeEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadProcessStatusEnum;
import lombok.Getter;
import lombok.Setter;

@DynamicInsert 
@DynamicUpdate 
@Entity
@Getter
@Setter
@Table(name = "DOWNLOAD_D")
public class DownloadD implements Serializable {

	private static final long serialVersionUID = -3940089479156311141L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="DOWNLOAD_NO", referencedColumnName = "NO", nullable = false,  foreignKey = @ForeignKey(name = "FK_DOWNLOAD_D__DOWNLOAD_M"))
    private DownloadM downloadM;
	
	@Column(name = "SEQ")
	private Long seq;
	
	// 유저 번호
	@Column(name = "USER_NO")
    private Long userNo;
	
	//화자 ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHARACTER_ID", columnDefinition = "varchar(36)", referencedColumnName = "CHARACTER_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_DOWNLOAD_D__CHARACTER"))
    private VFCharacter vfCharacter;
    
    //TTS 스크립트 Text
    @Column(name = "SCRIPT_TEXT", columnDefinition = "varchar(1000)", nullable = false)
    private String scriptText;
	
	//저장 방식(VM, S3)
    @Enumerated(EnumType.STRING)
	@Column(name = "OUTPUT_TYPE")
    private DownloadOutputTypeEnum outputType;
	
	//노출 경로
	@Column(name = "OBJECT_URL", columnDefinition = "varchar(1000)")
    private String objectUrl;
	
	//AWS S3 Bucket Region 이름
	@Column(name = "AWS_S3_REGION_NM", columnDefinition = "varchar(250)")
	private String awsS3RegionNm;
	
	//AWS S3 Bucket 이름
	@Column(name = "AWS_S3_BUCKET_NM", columnDefinition = "varchar(500)")
	private String awsS3BucketNm;
	
	//AWS S3 Bucket Object 이름
	@Column(name = "AWS_S3_OBJECT_NM", columnDefinition = "varchar(500)")
	private String awsS3ObjectNm;
	
	//처리 진행 상태(A:대기,B:진행중,C:완료,D:오류,E:오류(TTS 변환),F:오류(Audio변환),G:오류(AWS), H:오류(기타)
	@Enumerated(EnumType.STRING)
	@Column(name = "PROCESS_STATUS", columnDefinition = "varchar(1)")
    private DownloadProcessStatusEnum processStatus;
	
	@Lob
    @Column(name = "TTS_RESOURCE", columnDefinition = "MEDIUMBLOB")
    private byte[] ttsResource;
	
	// 재생시간
	@Column(name = "TTS_PLAY_TIME")
	private Float ttsPlayTime;
	
	// 비활성화 여부
	@Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;

	//등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}

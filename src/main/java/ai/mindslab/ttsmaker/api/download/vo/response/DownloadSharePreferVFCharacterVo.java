package ai.mindslab.ttsmaker.api.download.vo.response;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadSharePreferVFCharacterVo {
	private String characterId; 	// 보이스폰트 ID
	private String characterNm; 	// 보이스폰트 이름
	private String avatarClassNm;   // 보이스폰트 아바타 클래스 명
	private Long count;				// 사용된 카운팅
	
	public static DownloadSharePreferVFCharacterVo of(VFCharacter entity, Long count) {
        return DownloadSharePreferVFCharacterVo.builder()
                .characterId(entity.getCharacterId())
                .characterNm(entity.getCharacterNm())
                .avatarClassNm(entity.getAvatarClassNm())
                .count(count)
                .build();
	}
}

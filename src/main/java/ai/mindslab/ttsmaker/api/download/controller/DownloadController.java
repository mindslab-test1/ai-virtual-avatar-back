package ai.mindslab.ttsmaker.api.download.controller;


import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.download.service.DownloadService;
import ai.mindslab.ttsmaker.api.download.vo.request.DownloadProcessRequestVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadListContentsResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadRequestCalcResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadRequestResultResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareContentsResponseVo;
import ai.mindslab.ttsmaker.api.download.vo.response.DownloadShareViewContentsResponseVo;
import ai.mindslab.ttsmaker.common.page.PageRequest;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;

@RestController
@RequestMapping(path = "/download")
public class DownloadController {

	@Autowired
	private DownloadService downloadService;
	
    @GetMapping
    @ResponseBody
    public DownloadListContentsResponseVo getDownloadList(@AvaUserParam AvaUser avaUser, final PageRequest pageable) {
		pageable.setSortColumn("no");
		return downloadService.getDownloadList(avaUser, pageable.of());
	}
    
    @ResponseBody
    @PostMapping(path = "/check")
	public DownloadRequestCalcResponseVo checkForPreRequestDownload(@AvaUserParam AvaUser avaUser, @RequestBody DownloadProcessRequestVo requestDto) {
		return downloadService.checkForPreRequestDownload(avaUser.getUserNo(), requestDto);
	}
        
    @ResponseBody
	@PostMapping
	public DownloadRequestResultResponseVo requestDownload(@AvaUserParam AvaUser avaUser, @RequestBody DownloadProcessRequestVo requestDto) throws IOException, InterruptedException {
		return downloadService.requestDownload(avaUser, requestDto);
	}
    
    @GetMapping(path = "/{downloadId}/share")
    @ResponseBody
    public DownloadShareContentsResponseVo getSNSSharePreProcess(@AvaUserParam AvaUser avaUser, @PathVariable String downloadId) {
		return downloadService.getSNSSharePreProcess(avaUser, downloadId);
	}
    
    @GetMapping(path = "/{downloadId}")
    @ResponseBody
    public DownloadShareViewContentsResponseVo viewDownload(@AvaUserParam AvaUser avaUser, @PathVariable String downloadId) {
		return downloadService.getSNSShareDownloadView(avaUser, downloadId);
	}
}

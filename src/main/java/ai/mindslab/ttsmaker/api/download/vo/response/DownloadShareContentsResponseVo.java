package ai.mindslab.ttsmaker.api.download.vo.response;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.download.domain.DownloadM;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadShareContentsResponseVo {
	private Long downloadNo;			// 다운로드 관리번호
	private String downloadId;			// 다운로드 ID
	private String downloadTitle;		// 다운로드 제목
	
	private List<DownloadSharePreferVFCharacterVo> preferVFList;  // 선호 보이스폰트
	
	private Boolean active;				// 활성화 여부
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
	private LocalDateTime createdAt;	// 등록일자
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
	private LocalDateTime updatedAt;	// 수정일자
	
	
	public static DownloadShareContentsResponseVo of(DownloadM entity, List<DownloadSharePreferVFCharacterVo> preferVFList) {
		return DownloadShareContentsResponseVo.builder()
				.downloadNo(entity.getNo())
				.downloadId(entity.getDownloadId())
				.downloadTitle(entity.getDownloadTitle())
				.preferVFList(preferVFList)
				.active(entity.getActive())
				.createdAt(entity.getCreatedAt())
				.updatedAt(entity.getUpdatedAt())
				.build();
	}
    
	
}

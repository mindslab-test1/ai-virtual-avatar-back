package ai.mindslab.ttsmaker.api.download.vo.response;

import ai.mindslab.ttsmaker.api.download.domain.DownloadM;
import ai.mindslab.ttsmaker.api.download.enums.DownloadProcessStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadRequestResultResponseVo {
	private String status;
	private String status_message;
	
	public static DownloadRequestResultResponseVo of(DownloadM entity) {
		//처리 진행 상태(A:대기,B:진행중,C:완료,D:오류
		String status_message = null;
		DownloadProcessStatusEnum status = entity.getProcessStatus();
		String downloadTitle = entity.getDownloadTitle();
		Integer processStep = entity.getProcessStep();
		Integer processTotleStep = entity.getProcessTotalStep();
		String processStatusCause = entity.getProcessStatusCause();
		switch(status) {
			case B:
				status_message = String.format("'%s' 다운로드 변환 작업을 진행중(%d/%d) 있습니다.", downloadTitle, processStep, processTotleStep);
			break;
			case C:
				status_message = String.format("'%s' 다운로드 변환이 완료 되었습니다.", downloadTitle);
			break;
			case D:
				status_message = String.format("'%s' 다운로드 변환 작업중 오류가 발생했습니다.(사유 : %s)", downloadTitle, processStatusCause);
			break;
			default: // A
				status_message = String.format("'%s' 다운로드 변환 작업을 기다리고 있습니다.", downloadTitle);
		}
		return of(status, status_message);
	}
	
	public static DownloadRequestResultResponseVo of(DownloadProcessStatusEnum statusEnum, String status_message) {
		return DownloadRequestResultResponseVo.builder()
				.status(statusEnum.toString())
				.status_message(status_message)
				.build();
	}
}

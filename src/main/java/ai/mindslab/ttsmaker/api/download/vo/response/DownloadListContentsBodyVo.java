package ai.mindslab.ttsmaker.api.download.vo.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.download.domain.DownloadM;
import ai.mindslab.ttsmaker.api.download.enums.DownloadProcessStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownloadListContentsBodyVo {
	private Long downloadNo;
	private String downloadId;
	private String downloadTitle;
	private Float totalDurationSecs;
	private String fileFormat;
	private Integer downloadCharCnt;
	private Integer downloadExpectCharCnt;
	private String objectUrl;
	private DownloadProcessStatusEnum processStatus;
	private String processStatusCause;
	private Integer processStep;
	private Integer processTotalStep;
	private boolean elapsed7Days;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:ss")
	private LocalDateTime downloadAt;	
	
	public static DownloadListContentsBodyVo of(DownloadM entity) {
		DownloadListContentsBodyVo result = DownloadListContentsBodyVo.builder()
				.downloadNo(entity.getNo())
				.downloadId(entity.getDownloadId())
				.downloadTitle(entity.getDownloadTitle())
				.totalDurationSecs(entity.getTotalDurationSecs())
				.downloadCharCnt(entity.getDownloadCharCnt())
				.downloadExpectCharCnt(entity.getDownloadExpectCharCnt())
				.fileFormat(entity.getFileFormat().name().toString())
				.objectUrl(entity.getObjectUrl())
				.processStatus(entity.getProcessStatus())
				.processStatusCause(entity.getProcessStatusCause())
				.processStep(entity.getProcessStep())
				.processTotalStep(entity.getProcessTotalStep())
				.downloadAt(entity.getCreatedAt())
				.build();
		LocalDateTime elapsed7DayAt =  LocalDateTime.from(entity.getCreatedAt()).plusDays(7);
		if(DownloadProcessStatusEnum.C.equals(entity.getProcessStatus()) && LocalDateTime.now().isAfter(elapsed7DayAt)) {
			result.setElapsed7Days(true);
			result.setObjectUrl("");
			result.setProcessStatus(DownloadProcessStatusEnum.I);
			result.setProcessStatusCause("생성 시간 경과로 다운로드 받을 수 없습니다.");
		}
		
		return result;
	}
}

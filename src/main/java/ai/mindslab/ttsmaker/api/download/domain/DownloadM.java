package ai.mindslab.ttsmaker.api.download.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;

import ai.mindslab.ttsmaker.api.download.enums.DownloadFileFormatEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadMethodEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadOutputTypeEnum;
import ai.mindslab.ttsmaker.api.download.enums.DownloadProcessStatusEnum;
import lombok.Getter;
import lombok.Setter;

@DynamicInsert 
@DynamicUpdate 
@Entity
@Getter
@Setter
@Table(name = "DOWNLOAD_M")
public class DownloadM implements Serializable {

	private static final long serialVersionUID = -3940089479156311141L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;
	
	@Column(name = "DOWNLOAD_ID", columnDefinition = "varchar(36)", nullable = false)
    private String downloadId;
	
	// 다운로드 제목
	@Column(name = "DOWNLOAD_TITLE", columnDefinition = "varchar(100)")
    private String downloadTitle;
	
	// 다운로드 유형(0:개별 파일다운로드, 1:전체 파일다운로드)
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DOWNLOAD_METHOD", columnDefinition = "varchar(1)")
    private DownloadMethodEnum downloadMethod;
	
	// 파일 포맷
	@Enumerated(EnumType.STRING)
	@Column(name = "FILE_FORMAT", columnDefinition = "varchar(3)")
    private DownloadFileFormatEnum fileFormat;
	
	// 유저 번호
	@Column(name = "USER_NO")
    private Long userNo;
	
	//총 재생시간
	@Column(name = "TOTAL_DURATION_SECS")
	private Float totalDurationSecs;
	
	//다운로드 글자 수
	@Column(name = "DOWNLOAD_CHAR_CNT", columnDefinition = "int default 0")
    private Integer downloadCharCnt;
	
	//다운로드 글자 수
	@Column(name = "DOWNLOAD_EXPECT_CHAR_CNT", columnDefinition = "int default 0")
    private Integer downloadExpectCharCnt;
	
	//저장 방식(VM, S3)
    @Enumerated(EnumType.STRING)
	@Column(name = "OUTPUT_TYPE")
    private DownloadOutputTypeEnum outputType;
	
	//노출 경로
	@Column(name = "OBJECT_URL", columnDefinition = "varchar(1000)")
    private String objectUrl;
	
	//AWS S3 Bucket Region 이름
	@Column(name = "AWS_S3_REGION_NM", columnDefinition = "varchar(250)")
	private String awsS3RegionNm;
	
	//AWS S3 Bucket 이름
	@Column(name = "AWS_S3_BUCKET_NM", columnDefinition = "varchar(500)")
	private String awsS3BucketNm;
	
	//AWS S3 Bucket Object 이름
	@Column(name = "AWS_S3_OBJECT_NM", columnDefinition = "varchar(500)")
	private String awsS3ObjectNm;
	
	//진행 단계
	@Column(name = "PROCESS_STEP", columnDefinition = "int default 0")
    private Integer processStep;
	
	//진행 단계(전체)
	@Column(name = "PROCESS_TOTAL_STEP", columnDefinition = "int default 0")
	private Integer processTotalStep;
	
	//처리 진행 상태(A:대기,B:진행중,C:완료,D:완료(재사용)
	@Enumerated(EnumType.STRING)
	@Column(name = "PROCESS_STATUS", columnDefinition = "varchar(1)")
    private DownloadProcessStatusEnum processStatus;
	
	//처리 진행 에러 원인
	@Column(name = "PROCESS_STATUS_CAUSE", columnDefinition = "TEXT")
    private String processStatusCause;
	
	//비활성화 여부
	@Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy ="downloadM", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    private List<DownloadD> details = Lists.newArrayList();

    //등록일시
    @CreationTimestamp
    @Column(name = "CREATED_AT")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}

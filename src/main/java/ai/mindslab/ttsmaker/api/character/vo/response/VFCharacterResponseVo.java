package ai.mindslab.ttsmaker.api.character.vo.response;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VFCharacterResponseVo {
	private Long characterNo; // 보이스폰트 관리 번호
	private String characterId; // 보이스폰트 ID
	private String characterNm; // 보이스폰트 이름
	private String characterFeature; // 보이스폰트 특징
	private String characterLang; // 보이스폰트 언어스킬
	private String avatarClassNm;    // 보이스폰트 아바타 클래스 명
	private String voiceFontNm; // TTS VoiceName
	private String voiceSampleUrl; // TTS Voice 샘플 URL
	
	public static VFCharacterResponseVo of(VFCharacter entity) {
        return VFCharacterResponseVo.builder()
        		.characterNo(entity.getId())
                .characterId(entity.getCharacterId())
                .characterNm(entity.getCharacterNm())
                .characterFeature(entity.getCharacterFeature())
                .characterLang(entity.getCharacterLang())
                .avatarClassNm(entity.getAvatarClassNm())
                .voiceFontNm(entity.getVoiceFontNm())
                .voiceSampleUrl(entity.getVoiceSampleUrl())
                .build();
	}
}
package ai.mindslab.ttsmaker.api.character.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;

public interface VFCharacterRepository extends JpaRepository<VFCharacter, Long> { 
	
	@Query("select p from VFCharacter p where p.isDeleted = 'N' order by seq")
	List<VFCharacter> findActiveList();

	/**
	 * characterId로 조회
	 * @param characterId
	 * @return
	 */
	Optional<VFCharacter> findByCharacterIdEquals(String characterId);

}

package ai.mindslab.ttsmaker.api.character.service;

import java.util.List;
import java.util.Map;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.vo.response.VFCharacterResponseVo;

public interface VFCharacterService {
	
	/**
	 * <pre>
	 * 보이스 폰트 전체 리스트 조회
	 * </pre>
	 * @return
	 */
	public List<VFCharacterResponseVo> getAllList();
	
	
	/**
	 * <pre>
	 * 보이스폰트 전체 모델 엔티 조회
	 * </pre>
	 * @return
	 */
	public List<VFCharacter> allEntities();
	
	
	/**
	 * <pre>
	 * 보이스폰트 전체 모델 엔티 조회(Map)
	 * </pre>
	 * @return
	 */
	public Map<String, VFCharacter> getVFCharactersMap();


	/**
	 * <pre>
	 * 보이스폰트 랜덤 조회
	 * </pre>
	 * @return
	 */
	public VFCharacterResponseVo getDailyCharacter();
}

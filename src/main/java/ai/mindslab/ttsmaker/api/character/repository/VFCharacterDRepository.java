package ai.mindslab.ttsmaker.api.character.repository;

import ai.mindslab.ttsmaker.api.admin.vo.data.VFCharacterDataVo;
import ai.mindslab.ttsmaker.api.character.domain.QVFCharacter;
import ai.mindslab.ttsmaker.api.character.domain.QVFCharacterD;
import ai.mindslab.ttsmaker.api.character.domain.VFCharacterD;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

public interface VFCharacterDRepository extends JpaRepository<VFCharacterD, Long> {

    QVFCharacter vFCharacter = QVFCharacter.vFCharacter;
    QVFCharacterD vFCharacterD = QVFCharacterD.vFCharacterD;

    /**
     * id로 조회
     * @param id
     * @return
     */
    Optional<VFCharacterD> findByIdEquals(long id);

    /**
     * 보이스폰트 정보 조회
     * @param entityManager
     * @param characterId
     * @return
     */
    default VFCharacterDataVo findVFCharacterByCharacterId(EntityManager entityManager, String characterId){
        return new JPAQuery<VFCharacterDataVo>(entityManager)
                .select(
                        Projections.constructor(
                                VFCharacterDataVo.class,
                                vFCharacter.characterId,
                                vFCharacter.characterNm,
                                vFCharacter.voiceFontNm,
                                vFCharacter.avatarClassNm,
                                vFCharacter.isDeleted,
                                vFCharacter.createdAt,
                                vFCharacter.updatedAt,
                                vFCharacterD.agency,
                                vFCharacterD.contractor,
                                vFCharacterD.depositAccount,
                                vFCharacterD.depositBank,
                                vFCharacterD.distYn,
                                vFCharacterD.recorder
                        )
                )
                .from(vFCharacter)
                .leftJoin(vFCharacterD).on(vFCharacter.id.eq(vFCharacterD.id))
                .where(vFCharacter.characterId.eq(characterId))
                .fetchOne();
    }

}

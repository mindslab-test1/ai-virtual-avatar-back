package ai.mindslab.ttsmaker.api.character.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.repository.VFCharacterRepository;
import ai.mindslab.ttsmaker.api.character.vo.response.VFCharacterResponseVo;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class VFCharacterServiceImpl implements VFCharacterService {

	@Autowired
    private VFCharacterRepository vfCharacterRepository;

	@Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public List<VFCharacterResponseVo> getAllList() {
        try {
            log.info("Getting list of all speakers");
            List<VFCharacter> list = vfCharacterRepository.findActiveList();
            return list.stream().map(VFCharacterResponseVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
        	log.error("{} ERRS0001 - 화자 리스트 조회중 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0001", "화자 리스트 조회중 오류가 발생했습니다.");
        }
    }

    @Override
    public List<VFCharacter> allEntities() {
        try {
        	log.info("Getting list of all speakers DB entities");
            return vfCharacterRepository.findActiveList();
        } catch (Exception ex) {
        	log.error("{} ERRS0002 - 화자 리스트 조회중 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0002", "화자 리스트 조회중 오류가 발생했습니다.");
        }
    }
    
    @Override
    public Map<String, VFCharacter> getVFCharactersMap() {
		return this.allEntities().stream().collect(Collectors.toMap(VFCharacter::getCharacterId, Function.identity()));
	}

    @Override
    public VFCharacterResponseVo getDailyCharacter() {
        try {
        	VFCharacterResponseVo dailyCharacter = getDailyCharacterTagetByRedis();
            if (dailyCharacter == null) {
                List<VFCharacter> allEntities = vfCharacterRepository.findActiveList();

                int count = allEntities.size();
                int rnd = new Random().nextInt(count);

                dailyCharacter = VFCharacterResponseVo.of(allEntities.get(rnd));

                setDailyCharacterTagetByRedis(dailyCharacter);
            }
            return dailyCharacter;
        } catch (TTSMakerApiException ex) {
            throw ex;
        } catch (Exception ex) {
        	log.error("{} ERRS0003 - 랜덤 화자 조회중 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0003", "랜덤 화자 조회중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * 기본 화자 VFCharacterResponseVo 저장(Redis)
     * </pre>
     *
     * @param characterVo
     */
    private void setDailyCharacterTagetByRedis(VFCharacterResponseVo characterVo) {
    	log.info("Setting DailyCharacter to cache");
        try {
            String key = "ava:default:dailyCharacter";
            redisTemplate.opsForValue().set(key, characterVo);
            redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(1)));
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0004", "DailyCharacter 업데이트 처리중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * 기본 화자 SpeakerVo 불러오기(Redis)
     * </pre>
     *
     * @return
     */
    private VFCharacterResponseVo getDailyCharacterTagetByRedis() {
    	log.info("Getting DailyCharacter from cache");

        try {
            String key = "ava:default:dailyCharacter";
            return (VFCharacterResponseVo) redisTemplate.opsForValue().get(key);
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0005", "DailyCharacter 업데이트 처리중 오류가 발생했습니다.");
        }
    }
}

package ai.mindslab.ttsmaker.api.character.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "VF_CHARACTER_D")
public class VFCharacterD implements Serializable {

    private static final long serialVersionUID = 3219342698001401431L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    //소속
    @Column(name = "AGENCY", columnDefinition = "varchar(100)")
    private String agency;

    //녹음자
    @Column(name = "RECORDER", columnDefinition = "varchar(100)")
    private String recorder;

    //계약자
    @Column(name = "CONTRACTOR", columnDefinition = "varchar(100)")
    private String contractor;

    //입금은행
    @Column(name = "DEPOSIT_BANK", columnDefinition = "varchar(100)")
    private String depositBank;

    //입금계좌
    @Column(name = "DEPOSIT_ACCOUNT", columnDefinition = "varchar(100)")
    private String depositAccount;

    //분배대상 여부
    @Column(name = "DIST_YN", columnDefinition = "char(1) default 'N'")
    private String distYn;

    @JsonIgnore
    @MapsId
    @OneToOne
    @JoinColumn(name = "ID", foreignKey = @ForeignKey(name = "FK_VF_CHARACTER_D__VF_CHARACTER"))
    VFCharacter vfCharacter;

}

package ai.mindslab.ttsmaker.api.admin.repository;

import ai.mindslab.ttsmaker.api.admin.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository <Post, Long> {
}

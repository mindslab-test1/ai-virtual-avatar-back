package ai.mindslab.ttsmaker.api.admin.vo.request;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemoPostRequestVo {
    private Long createdBy;
    private String content;
}

package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InquiryDetailResponseDto {
    private String inquiryType;
    private String userNm;
    private String userCellPhone;
    private String userId;
    private String userPlanNm;
    private String inquiryTitle;
    private String inquiryQuestion;
    private Boolean inquiryStatus;
    private String inquiryAnswer;
    private String answeredEmail;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:SS")
    private LocalDateTime questionDate;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd HH:mm:SS")
    private LocalDateTime answerDate;
    
    public static InquiryDetailResponseDto of(UserInquiry entity) {
        UserM user = entity.getUser();
        List<UserPlan> list = user.getUserPlans();
        String userPlanNm = null;
        if(list.size() > 0) {
            userPlanNm = list.get(0).getPlan().getPlanNm();
        }
        
        return InquiryDetailResponseDto.builder()
            .inquiryType(entity.getInquiryType())
            .userNm(user.getUserNm())
            .userCellPhone(user.getCellPhone())
            .userId(user.getUserId())
            .userPlanNm(userPlanNm)
            .inquiryTitle(entity.getInquiryTitle())
            .inquiryQuestion(entity.getInquiryQuestion())
            .inquiryStatus(entity.getInquiryStatus())
            .inquiryAnswer(entity.getInquiryAnswer())
            .answeredEmail(entity.getAnsweredEmail())
            .questionDate(entity.getCreatedAt())
            .answerDate(entity.getAnsweredAt())
            .build();
            
    }
}

package ai.mindslab.ttsmaker.api.admin.vo.request;

import ai.mindslab.ttsmaker.api.admin.domain.Statistics;
import lombok.*;
import org.springframework.beans.BeanUtils;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.request
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-24  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-24
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTimelineStatisticsRequestVo {

    private Long userNo;
    private String userId;
    private String userNm;
    private String cellPhone;
    private Long timelineNo;

}

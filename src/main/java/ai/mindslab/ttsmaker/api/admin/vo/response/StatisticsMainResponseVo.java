package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.admin.domain.Statistics;
import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.vo.data.StatisticsDataVo;
import lombok.*;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-20
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsMainResponseVo {

    private String statisticsStatus;
    private String message;
    private StatisticsData data;

    public static StatisticsMainResponseVo of(StatisticsStatus statisticsStatus, String msg) {
        return StatisticsMainResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .build();
    }

    public static StatisticsMainResponseVo of(StatisticsStatus statisticsStatus, String msg, StatisticsDataVo vo, Statistics statistics) {
        return StatisticsMainResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .data(
                        StatisticsData.builder()
                            .totUser(vo.getTotUser())
                            .diffTotUser(vo.getTotUser() - statistics.getTotUser())
                            .paidUser(vo.getPaidUser())
                            .diffPaidUser(vo.getPaidUser() - statistics.getPaidUser())
                            .activeUser(vo.getActiveUser())
                            .diffActiveUser(vo.getActiveUser() - statistics.getActiveUser())
                            .totLoginCount(vo.getTotLoginCount())
                            .diffTotLoginCount(vo.getTotLoginCount() - statistics.getTotLoginCount())
                            .totVfCharacter(vo.getTotVfCharacter())
                            .diffTotVfCharacter(vo.getTotVfCharacter() - statistics.getTotVfCharacter())
                            .totDownloadLength(vo.getTotDownloadLength())
                            .diffTotDownloadLength(vo.getTotDownloadLength() - statistics.getTotDownloadLength())
                            .totConversionLength(vo.getTotConversionLength())
                            .diffTotConversionLength(vo.getTotConversionLength() - statistics.getTotConversionLength())
                            .build()
                )
                .message(msg)
                .build();
    }

    @Builder(toBuilder = true)
    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    static class StatisticsData{
        private long totUser;
        private long diffTotUser;
        private long paidUser;
        private long diffPaidUser;
        private long activeUser;
        private long diffActiveUser;
        private long totLoginCount;
        private long diffTotLoginCount;
        private long totVfCharacter;
        private long diffTotVfCharacter;
        private long totDownloadLength;
        private long diffTotDownloadLength;
        private long totConversionLength;
        private long diffTotConversionLength;
    }

}

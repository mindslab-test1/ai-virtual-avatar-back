package ai.mindslab.ttsmaker.api.admin.service.conditions;

import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.vo.request.InquirySearchRequestDto;
import com.google.api.client.util.Lists;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

public class InquirySearchSpecs {
    public static Specification<UserInquiry> chainingSpecs(InquirySearchRequestDto searchDto) {
        List<Specification<UserInquiry>> specs = Lists.newArrayList();
        
        // 문의 번호 조회
        if(StringUtils.hasText(searchDto.getInquiryNo())) {
            specs.add(inquiryNoEqual(ai.mindslab.ttsmaker.util.StringUtils.toLong(searchDto.getInquiryNo())));
        }
        
        if(StringUtils.hasText(searchDto.getUserNm())) {
            specs.add(userNmLike(searchDto.getUserNm()));
        }
        
        if(StringUtils.hasText(searchDto.getUserId())) {
            specs.add(userIdLike(searchDto.getUserId()));
        }
        
        if(StringUtils.hasText(searchDto.getInquiryType())) {
            specs.add(inquiryTypeEqual(searchDto.getInquiryType()));
        }
        
        if(StringUtils.hasText(searchDto.getInquiryStatus())) {
            specs.add(inquiryStatusEqual(Boolean.parseBoolean(searchDto.getInquiryStatus())));
        }
        
        if(StringUtils.hasText(searchDto.getAnsweredEmail())) {
            specs.add(answeredEmailLike(searchDto.getAnsweredEmail()));
        }
        
        Specification<UserInquiry> specification = Specification.where(null);
        if(!CollectionUtils.isEmpty(specs)) {
            specification = specs.stream().reduce(specification, (specs1, specs2) -> specs1.and(specs2));
        }
        return specification;
    }
    
    /**
     * <pre>
     * 문의 번호 조회(Specification)
     * <pre>
     * @param inquiryNo
     * @return
     */
    public static Specification<UserInquiry> inquiryNoEqual(final Long inquiryNo) {
        return (Specification<UserInquiry>) ((root, query, builder) -> {
            return builder.equal(root.get("inquiryNo"), inquiryNo);
        });
    }
    
    /**
     * <pre>
     * 문의 유저 이름 조회(Specification)
     * <pre>
     * @param userNm
     * @return
     */
    public static Specification<UserInquiry> userNmLike(final String userNm) {
        return (Specification<UserInquiry>) ((root, query, builder) -> {
            return builder.like(root.get("user").get("userNm"), "%" + userNm + "%");
        });
    }
    
    /**
     * <pre>
     * 문의 유저 이메일 조회(Specification)
     * <pre>
     * @param userId
     * @return
     */
    public static Specification<UserInquiry> userIdLike(final String userId) {
        return (Specification<UserInquiry>) ((root, query, builder) -> {
            return builder.like(root.get("user").get("userId"), "%" + userId + "%");
        });
    }
    
    /**
     * <pre>
     * 문의 유형 조회(Specification)
     * <pre>
     * @param inquiryType
     * @return
     */
    public static Specification<UserInquiry> inquiryTypeEqual(final String inquiryType) {
        return (Specification<UserInquiry>) ((root, query, builder) -> {
           return builder.equal(root.get("inquiryType"), inquiryType);
        });
    }
    
    /**
     * <pre>
     * 답변 여부 조회(Specification)
     * <pre>
     * @param inquiryStatus
     * @return
     */
    public static Specification<UserInquiry> inquiryStatusEqual(final Boolean inquiryStatus) {
        return (Specification<UserInquiry>) ((root, query, builder) -> {
           return builder.equal(root.get("inquiryStatus"), inquiryStatus);
        });
    }
    
    /**
     * <pre>
     * 답변자 이메일 조회(Specification)
     * <pre>
     * @param answeredEmail
     * @return
     */
    public static Specification<UserInquiry> answeredEmailLike(final String answeredEmail) {
        return (Specification<UserInquiry>) ((root, query, builder) -> {
           return builder.like(root.get("answeredEmail"), answeredEmail);
        });
    }
}

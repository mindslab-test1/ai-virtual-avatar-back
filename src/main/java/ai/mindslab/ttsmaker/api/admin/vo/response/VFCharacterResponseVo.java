package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.repository.StatisticsRepository;
import ai.mindslab.ttsmaker.api.admin.vo.data.VFCharacterDataVo;
import lombok.*;

import java.util.List;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-07  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-07
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VFCharacterResponseVo {

    private String statisticsStatus;
    private String message;
    private VFCharacterDataVo data;
    private List<StatisticsRepository.VoiceFontStatisticsData> monthStatisticsData;
    private StatisticsRepository.VoiceFontStatisticsData totalStatisticsData;
    private List<Integer> yearList;

    public static VFCharacterResponseVo of(StatisticsStatus statisticsStatus, String msg) {
        return VFCharacterResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .build();
    }

    public static VFCharacterResponseVo of(StatisticsStatus statisticsStatus, String msg, List<StatisticsRepository.VoiceFontStatisticsData> monthStatisticsData) {
        return VFCharacterResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .monthStatisticsData(monthStatisticsData)
                .message(msg)
                .build();
    }

    public static VFCharacterResponseVo of(StatisticsStatus statisticsStatus, String msg, VFCharacterDataVo data, List<StatisticsRepository.VoiceFontStatisticsData> monthStatisticsData, StatisticsRepository.VoiceFontStatisticsData totalStatisticsData, List<Integer> yearList) {
        return VFCharacterResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .data(data)
                .monthStatisticsData(monthStatisticsData)
                .totalStatisticsData(totalStatisticsData)
                .yearList(yearList)
                .message(msg)
                .build();
    }

}

package ai.mindslab.ttsmaker.api.admin.controller;

import ai.mindslab.ttsmaker.api.admin.service.AdminService;
import ai.mindslab.ttsmaker.api.admin.vo.request.MemoPostRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.UserInfoVo;
import ai.mindslab.ttsmaker.api.admin.vo.request.VFCharacterRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.response.*;
import ai.mindslab.ttsmaker.api.admin.vo.request.UserTimelineStatisticsRequestVo;
import ai.mindslab.ttsmaker.api.pay.service.PayService;
import ai.mindslab.ttsmaker.api.pay.vo.request.PayCancelPartialRequestVo;
import ai.mindslab.ttsmaker.api.pay.vo.request.PayCancelRequestVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.PayCancelPartialResponseVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.PayCancelResponseVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.UserPayHistoryResponseVo;
import ai.mindslab.ttsmaker.api.user.service.UserInquiryService;
import ai.mindslab.ttsmaker.api.user.vo.request.InquiryRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.InquirySearchRequestDto;
import ai.mindslab.ttsmaker.api.user.vo.response.UserTimelineResponseVo;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.common.page.PageRequest;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminController {
    final PayService payService;
    final AdminService adminService;
    final UserInquiryService userInquiryService;


    public AdminController(PayService payService, AdminService adminService, UserInquiryService userInquiryService) {
        this.payService = payService;
        this.adminService = adminService;
        this.userInquiryService = userInquiryService;
    }
    
    
//    @ResponseBody
//    @PostMapping(value = "/filteringInfo")
//    public String filterInfo(@RequestBody PaymentFilterRequestVo filterRequest) throws IOException {
//        String paymentPages = paymentService.getFilteredPayments(filterRequest);
//        logger.info("[Filtering Payment Lists] : {}", filterRequest);
//        return paymentPages;
//    }
    
    /**
     * 결제 - 취소
     */
    @ResponseBody
    @PostMapping(value = "/payment/payCancel")
    public PayCancelResponseVo payCancel(@RequestBody PayCancelRequestVo request) throws Exception {
        log.info("[Cancelling Pay] : payNo[{}], msg[{}]", request.getPayNo(), request.getMsg());
        
        return payService.cancelPay(request.getPayNo(), request.getMsg());
    }
    
    /**
     * 결제 - 부분 취소
     */
    @ResponseBody
    @PostMapping(value = "/payment/payCancelPartial")
    public PayCancelPartialResponseVo payCancelPartial(@RequestBody PayCancelPartialRequestVo request) throws Exception {
        log.info("[Cancelling Pay] : tid[{}], msg[{}], price[{}], confirmPrice[{}]", request.getTid(), request.getMsg(), request.getCancelPrice(), request.getPriceAfterCancel());
        
        return payService.cancelPayPartial(request.getTid(), request.getMsg(), request.getCancelPrice(), request.getPriceAfterCancel());
    }

    /**
     * 결제 - 정보
     */
    @ResponseBody
    @GetMapping(value = "/payment/{userNo}")
    public List<UserPayHistoryResponseVo> getUserPayHistory(@PathVariable Long userNo) throws Exception {
        log.info("[User Pay History] : userNo[{}]", userNo);

        return payService.getUserPayHistory(userNo);
    }

    /**
     * 사용자 - 정보
     */
    @ResponseBody
    @GetMapping(value = "/user/{userNo}")
    public UserInfoVo getUserInfo(@PathVariable Long userNo) throws Exception {
        log.info("[User Information] : userNo[{}]", userNo);

        return adminService.getUserByUserNo(userNo);
    }

    /**
     * 결제 - 비고 조회
     */
    @ResponseBody
    @GetMapping(value = "/payment/memo/{payNo}")
    public List<MemoPostsResponseVo> getPaymentMemo(@PathVariable Long payNo) throws Exception {
        log.info("[Payment memo] : payNo[{}]", payNo);

        return adminService.getPaymentMemo(payNo);
    }

    /**
     * 결제 - 비고 추가
     */
    @ResponseBody
    @PostMapping(value = "/payment/memo/{payNo}")
    public void addPaymentMemo(@PathVariable Long payNo, @RequestBody MemoPostRequestVo memoPostRequestVo) throws Exception {
        log.info("[Payment memo] : payNo[{}]", payNo);

        adminService.addPaymentMemo(payNo, memoPostRequestVo);
    }

    /**
     * 유저 타임라인  - 비고 조회
     */
    @ResponseBody
    @GetMapping(value = "/userTimeline/memo/{userTimelineNo}")
    public List<MemoPostsResponseVo> getUserTimelineMemo(@PathVariable Long userTimelineNo) throws Exception {
        log.info("[UserTimeline memo] : payNo[{}]", userTimelineNo);

        return adminService.getUserTimelineMemo(userTimelineNo);
    }

    /**
     * 유저 타임라인  - 비고 추가
     */
    @ResponseBody
    @PostMapping(value = "/userTimeline/memo/{userTimelineNo}")
    public void addUserTimelineMemo(@PathVariable Long userTimelineNo, @RequestBody MemoPostRequestVo memoPostRequestVo) throws Exception {
        log.info("[UserTimeline memo] : userTimelineNo[{}]", userTimelineNo);

        adminService.addUserTimelineMemo(userTimelineNo, memoPostRequestVo);
    }

    @ResponseBody
    @GetMapping(value = "/userTimeline/{userNo}")
    public List<UserTimelineResponseVo> getUserTimeline(@PathVariable Long userNo) throws Exception {
        log.info("[Get UserTimeline] ... userNo : {}", userNo);
        return adminService.getUserTimelineFindByUserNo(userNo);
    }

    @ResponseBody
    @Secured("ROLE_ADMIN")
    @GetMapping("/authorize")
    public String checkAdminRole(@AvaUserParam AvaUser avaUser) {
        return "Authorized";
    }

    /**
     * 통계 메인페이지 데이터 추출
     * @param date
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/statistics/main/{date}")
    public StatisticsMainResponseVo getStatisticsMain(@PathVariable int date) {
        return adminService.getStatisticsMain(date);
    }
    
    @ResponseBody
    @GetMapping(value = "/inquiry/list")
    public AvaPage<InquiryListResponseDto> getInquiryList(final PageRequest pageable, final InquirySearchRequestDto searchRequestDto) {
        pageable.setSortColumn("inquiryNo");
        return adminService.getInquiryList(pageable.of(), searchRequestDto);
    }
    
    @ResponseBody
    @PostMapping(value = "/inquiry/reply")
    public String replyInquiry(@AvaUserParam AvaUser avaUser, @RequestBody InquiryRequestVo requestVo) {
        log.info("[Reply Inquiry] : InquiryNo = {}, Admin Email = {}", requestVo.getInquiryNo(), avaUser.getUserId());
        userInquiryService.replyInquiry(requestVo, avaUser.getUserId());
        return "Success";
    }
    
    @ResponseBody
    @GetMapping(value = "/inquiry/detail/{inquiryNo}")
    public InquiryDetailResponseDto getInquiryDetail(@PathVariable Long inquiryNo) {
        return adminService.getInquiryDetail(inquiryNo);
    }

    /**
     * 유저 타임라인 조회
     * @param vo
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/statistics/user/timeline")
    public UserTimelineStatisticsResponseVo getUserTimeline(UserTimelineStatisticsRequestVo vo) {
        return adminService.getUserTimelineList(vo);
    }

    /**
     * 유저 타임라인 차트 조회
     * @param vo
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/statistics/user/timeline/chart")
    public UserTimelineStatisticsResponseVo getUserTimelineChartData(UserTimelineStatisticsRequestVo vo) {
        return adminService.getUserTimelineStatistics(vo);
    }

    /**
     * 보이스폰트 통계 목록 조회
     * @param pageable
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/statistics/voicefont")
    public VoiceFontStatisticsResponseVo getVoiceFontStatisticsDataPage(PageRequest pageable) {
        return adminService.getVoiceFontStatisticsDataPage(pageable);
    }

    /**
     * 보이스폰트 정보 조회
     * @param characterId
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/statistics/voicefont/{characterId}/{year}")
    public VFCharacterResponseVo getVFCharacterData(@PathVariable String characterId, @PathVariable String year) {
        return adminService.getVFCharacterDataList(characterId, Integer.parseInt(year));
    }

    /**
     * 보이스폰트 정보 조회
     * @param characterId
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/statistics/voicefont/{characterId}")
    public VFCharacterResponseVo getVFCharacterData(@PathVariable String characterId) {
        return adminService.getVFCharacterData(characterId);
    }

    /**
     * 보이스폰트 정보 저장
     * @param characterId
     * @param vo
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/statistics/voicefont/{characterId}")
    public VFCharacterResponseVo setVFCharacterData(@AvaUserParam AvaUser avaUser,
                                                    @PathVariable String characterId,
                                                    @RequestBody VFCharacterRequestVo vo) {
        return adminService.setVFCharacterData(avaUser, characterId, vo);
    }

}

package ai.mindslab.ttsmaker.api.admin.vo.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-07  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-07
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VFCharacterDataVo {

    private String characterId;
    private String characterNm;
    private String voiceFontNm;
    private String avatarClassNm;
    private String delYn;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;
    private String agency;
    private String contractor;
    private String depositAccount;
    private String depositBank;
    private String distYn;
    private String recorder;

}

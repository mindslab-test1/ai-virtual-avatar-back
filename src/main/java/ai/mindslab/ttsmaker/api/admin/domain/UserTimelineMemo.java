package ai.mindslab.ttsmaker.api.admin.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "USER_TIMELINE_MEMO")
public class UserTimelineMemo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    @Column(name = "USER_TIMELINE_NO")
    private Long userTimeLineNo;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "POST_ID")
    private List<Post> posts;

    public void addPost(Post post){
        this.posts.add(post);
    }
}

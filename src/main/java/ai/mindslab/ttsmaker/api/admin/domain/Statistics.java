package ai.mindslab.ttsmaker.api.admin.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * ai.mindslab.ttsmaker.api.admin.domain
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-20
 */
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "STATISTICS")
public class Statistics implements Serializable {

    private static final long serialVersionUID = -267283352942391433L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    @Column(name = "TOT_USER")
    private Long totUser;

    @Column(name = "PAID_USER")
    private Long paidUser;

    @Column(name = "ACTIVE_USER")
    private Long activeUser;

    @Column(name = "TOT_LOGIN_COUNT")
    private Long totLoginCount;

    @Column(name = "TOT_VF_CHARACTER")
    private Long totVfCharacter;

    @Column(name = "TOT_DOWNLOAD_LENGTH")
    private Long totDownloadLength;

    @Column(name = "TOT_CONVERSION_LENGTH")
    private Long totConversionLength;

    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

}

package ai.mindslab.ttsmaker.api.admin.vo.request;

import ai.mindslab.ttsmaker.api.admin.vo.data.VFCharacterDataVo;
import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.domain.VFCharacterD;
import lombok.*;
import org.springframework.beans.BeanUtils;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-08  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-08
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VFCharacterRequestVo {

    VFCharacterDataVo data;
    int year;

    public static VFCharacter toVFCharacter(VFCharacter vfCharacter, VFCharacterDataVo data) {
        BeanUtils.copyProperties(data, vfCharacter);
        return vfCharacter;
    }

    public static VFCharacterD toVFCharacterD(VFCharacterD vfCharacterD, VFCharacter vfCharacter, VFCharacterDataVo data) {
        BeanUtils.copyProperties(data, vfCharacterD);
        vfCharacterD.setVfCharacter(vfCharacter);
        return vfCharacterD;
    }

}

package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.repository.StatisticsRepository;
import lombok.*;
import org.springframework.data.domain.Page;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-25  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-25
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VoiceFontStatisticsResponseVo {

    private String statisticsStatus;
    private String message;
    private Page<StatisticsRepository.VoiceFontStatisticsData> voiceFontStatisticsDataPage;

    public static VoiceFontStatisticsResponseVo of(StatisticsStatus statisticsStatus, String msg) {
        return VoiceFontStatisticsResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .build();
    }

    public static VoiceFontStatisticsResponseVo of(StatisticsStatus statisticsStatus, String msg, Page<StatisticsRepository.VoiceFontStatisticsData> data) {
        return VoiceFontStatisticsResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .voiceFontStatisticsDataPage(data)
                .build();
    }

}

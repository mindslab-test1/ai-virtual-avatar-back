package ai.mindslab.ttsmaker.api.admin.enums;

public enum StatisticsStatus {
    SUCCESS, FAIL, MULTIPLE_ROW, NOT_FOUND
}

package ai.mindslab.ttsmaker.api.admin.service;

import ai.mindslab.ttsmaker.api.admin.domain.PayMemo;
import ai.mindslab.ttsmaker.api.admin.domain.Post;
import ai.mindslab.ttsmaker.api.admin.domain.Statistics;
import ai.mindslab.ttsmaker.api.admin.domain.UserTimelineMemo;
import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.repository.PayMemoRepository;
import ai.mindslab.ttsmaker.api.admin.repository.StatisticsRepository;
import ai.mindslab.ttsmaker.api.admin.repository.UserTimelineMemoRepository;
import ai.mindslab.ttsmaker.api.admin.service.conditions.InquirySearchSpecs;
import ai.mindslab.ttsmaker.api.admin.vo.data.VFCharacterDataVo;
import ai.mindslab.ttsmaker.api.admin.vo.request.MemoPostRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.UserInfoVo;
import ai.mindslab.ttsmaker.api.admin.vo.data.StatisticsDataVo;
import ai.mindslab.ttsmaker.api.admin.vo.request.VFCharacterRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.response.*;
import ai.mindslab.ttsmaker.api.admin.vo.request.UserTimelineStatisticsRequestVo;
import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.domain.VFCharacterD;
import ai.mindslab.ttsmaker.api.character.repository.VFCharacterDRepository;
import ai.mindslab.ttsmaker.api.character.repository.VFCharacterRepository;
import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserTimeline;
import ai.mindslab.ttsmaker.api.user.repository.UserInquiryRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserMRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserPlanRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserTimelineRepository;
import ai.mindslab.ttsmaker.api.user.vo.request.InquirySearchRequestDto;
import ai.mindslab.ttsmaker.api.user.vo.request.UserFilterRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserTimelineResponseVo;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.common.page.AvaPageImpl;
import ai.mindslab.ttsmaker.common.page.PageRequest;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import com.google.cloud.bigquery.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private UserTimelineRepository userTimelineRepository;

    @Autowired
    private UserPlanRepository userPlanRepository;

    @Autowired
    private UserMRepository userMRepository;

    @Autowired
    private StatisticsRepository statisticsRepository;

    @Autowired
    private UserInquiryRepository userInquiryRepository;

    @Autowired
    private PayMemoRepository payMemoRepository;

    @Autowired
    private UserTimelineMemoRepository userTimelineMemoRepository;

    @Autowired
    private VFCharacterRepository vfCharacterRepository;

    @Autowired
    private VFCharacterDRepository vfCharacterDRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<UserTimelineResponseVo> getUserTimelineFindByUserNo(Long userNo) {
        log.info("유저 타임라인 조회. userNo = {}", userNo);
        List<UserTimeline> userTimelines = userTimelineRepository.findTimelineByUserNo(userNo);
        List<UserTimelineResponseVo> userTimelineResponseVoList = new ArrayList<>();

        userTimelines.stream()
                .forEach(
                        userTimeline -> {
                            UserPlan currUserPlan = userTimeline.getUserPlan();
                            Long prevUserNo = currUserPlan.getPrevUserPlanNo();
                            log.info("현재 플랜 정보 조회 >> userNo = {}, currUserPlan = {}", userNo, currUserPlan);

                            if (prevUserNo != null) {
                                UserPlan prevUserPlan = userPlanRepository.findUserPlanByUserPlanNo(prevUserNo);
                                Long prevPrevUserNo = prevUserPlan.getPrevUserPlanNo();
                                log.info("이전 플랜 정보 조회 >> userNo = {}, prevUserPlan = {}", userNo, prevUserPlan);

                                if (prevPrevUserNo != null) {
                                    UserPlan prevPrevUserPlan = userPlanRepository.findUserPlanByUserPlanNo(prevPrevUserNo);
                                    log.info("이전 플랜 정보 조회 >> userNo = {}, prevUserPlan = {}", userNo, prevPrevUserPlan);
                                    userTimelineResponseVoList.add(UserTimelineResponseVo.of(currUserPlan, prevUserPlan, prevPrevUserPlan));
                                    return;
                                }
                                userTimelineResponseVoList.add(UserTimelineResponseVo.of(currUserPlan, prevUserPlan));
                                return;
                            }
                            userTimelineResponseVoList.add(UserTimelineResponseVo.of(currUserPlan));
                            return;
                        }
                );


        return userTimelineResponseVoList;
    }

    // TODO:유저 리스트 필터링 기능 추가
    public String getFilteredUsers(UserFilterRequestVo userFilterRequestVo) {
        return null;
    }

    public void deactivateExpiredAvaWelcomeUserPlans() {
        List<UserPlan> activeAvaWelcomeUserPlans = userPlanRepository.findActiveUserPlanByPlanNo(5L);

        activeAvaWelcomeUserPlans.stream()
                .filter(userPlan -> userPlan.getPlanEndAt().isAfter(LocalDateTime.now()))
                .forEach(expiredUserPlan -> {
                    expiredUserPlan.setActive(false);
                    userPlanRepository.save(expiredUserPlan);
                });
    }

    @Override
    public UserInfoVo getUserByUserNo(Long userNo) {
        UserM userM = userMRepository.findByUserNo(userNo).orElse(null);
        return UserInfoVo.of(userM);
    }

    /**
     * 통계데이터 적재
     */
    @Override
    public void setStatisticsMain() {
        statisticsRepository.save(StatisticsDataVo.toEntity(getStatisticsData()));
        log.info("통계메인 데이터 적재완료");
    }

    /**
     * 통계 데이터 추출
     *
     * @param date
     * @return
     */
    @Override
    public StatisticsMainResponseVo getStatisticsMain(int date) {
        Optional<Statistics> optional = Optional.ofNullable(statisticsRepository.findByCreatedAtEquals(entityManager, LocalDate.now().minusDays(date)));
        if (optional.isPresent()) {
            return StatisticsMainResponseVo.of(StatisticsStatus.SUCCESS,
                    "조회 성공",
                    getStatisticsData(),
                    optional.get());
        } else {
            return StatisticsMainResponseVo.of(StatisticsStatus.FAIL, "기준 통계데이터가 존재하지 않습니다.");
        }
    }

    /**
     * 통계 데이터 수집
     *
     * @return
     */
    public StatisticsDataVo getStatisticsData() {
        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        QueryJobConfiguration loginQuery =
                QueryJobConfiguration.newBuilder(
                        "SELECT  count(e.value.string_value) as count\n" +
                                "    FROM `minds-ava.analytics_254440953.events_*`, UNNEST(event_params) as e\n" +
                                "    where event_name = 'signIn'\n" +
                                "    and e.key = 'userId'\n" +
                                "    and e.value.string_value != 'minds@minds.com'\n" +
                                "    group by event_name")
                        .setUseLegacySql(false)
                        .build();
        QueryJobConfiguration activeQuery =
                QueryJobConfiguration.newBuilder(
                        "SELECT \n" +
                                "    (SELECT\n" +
                                "        count(A.userId)\n" +
                                "    FROM (\n" +
                                "        SELECT\n" +
                                "            e.value.string_value as userId\n" +
                                "        FROM `minds-ava.analytics_254440953.events_intraday_*`, UNNEST(event_params) as e\n" +
                                "        where _TABLE_SUFFIX BETWEEN \n" +
                                "            FORMAT_DATE(\"%Y%m%d\", DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)) AND\n" +
                                "            FORMAT_DATE(\"%Y%m%d\", DATE_SUB(CURRENT_DATE(), INTERVAL 0 DAY))\n" +
                                "            and(event_name = 'playScriptOnce' or event_name = 'downloadScript')\n" +
                                "            and e.key = 'userId'\n" +
                                "            and e.value.string_value != 'minds@minds.com'\n" +
                                "        group by e.value.string_value\n" +
                                "    ) A) + \n" +
                                "    (SELECT\n" +
                                "        count(A.userId)\n" +
                                "    FROM (\n" +
                                "        SELECT\n" +
                                "            e.value.string_value as userId\n" +
                                "        FROM `minds-ava.analytics_254440953.events_*`, UNNEST(event_params) as e\n" +
                                "        where _TABLE_SUFFIX BETWEEN \n" +
                                "            FORMAT_DATE(\"%Y%m%d\", DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)) AND\n" +
                                "            FORMAT_DATE(\"%Y%m%d\", DATE_SUB(CURRENT_DATE(), INTERVAL 0 DAY))\n" +
                                "            and(event_name = 'playScriptOnce' or event_name = 'downloadScript')\n" +
                                "            and e.key = 'userId'\n" +
                                "            and e.value.string_value != 'minds@minds.com'\n" +
                                "        group by e.value.string_value\n" +
                                "    ) A) as count")
                        .setUseLegacySql(false)
                        .build();
        JobId loginId = JobId.of(UUID.randomUUID().toString());
        JobId activeId = JobId.of(UUID.randomUUID().toString());
        Job loginJob = bigquery.create(JobInfo.newBuilder(loginQuery).setJobId(loginId).build());
        Job activeJob = bigquery.create(JobInfo.newBuilder(activeQuery).setJobId(activeId).build());
        try {
            loginJob = loginJob.waitFor();
            activeJob = activeJob.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TableResult loginResult = null;
        TableResult activeResult = null;
        try {
            loginResult = loginJob.getQueryResults();
            activeResult = activeJob.getQueryResults();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long loginCount = 0;
        long activeCount = 0;
        for (FieldValueList row : loginResult.iterateAll()) {
            loginCount = row.get("count").getLongValue();
        }
        for (FieldValueList row : activeResult.iterateAll()) {
            activeCount = row.get("count").getLongValue();
        }
        return StatisticsDataVo.builder()
                .totUser(statisticsRepository.countUserMByActive(entityManager))
                .paidUser(statisticsRepository.countBillingByActive(entityManager))
                .activeUser(activeCount)
                .totVfCharacter(statisticsRepository.countVfCharacterByDelYn(entityManager))
                .totLoginCount(loginCount)
                .totDownloadLength(statisticsRepository.sumDownloadChar(entityManager))
                .totConversionLength(statisticsRepository.sumTtsConversionChar(entityManager))
                .build();
    }

    @Override
    public AvaPage<InquiryListResponseDto> getInquiryList(org.springframework.data.domain.PageRequest pageable, InquirySearchRequestDto searchDto) {
        Specification<UserInquiry> specification = InquirySearchSpecs.chainingSpecs(searchDto);
        Page<UserInquiry> page = userInquiryRepository.findAll(specification, pageable);
        List<InquiryListResponseDto> list = page.getContent().stream().map(InquiryListResponseDto::of).collect(Collectors.toList());
        return new AvaPageImpl<>(list, pageable, page.getTotalElements());
    }

    @Override
    public InquiryDetailResponseDto getInquiryDetail(Long inquiryNo) {
        UserInquiry inquiry = userInquiryRepository.findInquiryByInquiryNo(inquiryNo);
        return InquiryDetailResponseDto.of(inquiry);
    }

    /**
     * 유저 타임라인 목록 조회
     *
     * @param vo
     * @return
     */
    @Override
    public UserTimelineStatisticsResponseVo getUserTimelineList(UserTimelineStatisticsRequestVo vo) {
        return statisticsRepository.findUserTimelineNoByRequest(entityManager, vo);
    }

    /**
     * 유저 타임라인 통계 조회
     *
     * @param vo
     * @return
     */
    @Override
    public UserTimelineStatisticsResponseVo getUserTimelineStatistics(UserTimelineStatisticsRequestVo vo) {
        Map<String, Object> map = statisticsRepository.findUserPlanDateAndPlanNameByUserTimeline(entityManager, vo);
        try {
            return UserTimelineStatisticsResponseVo.of(
                    StatisticsStatus.SUCCESS,
                    "조회 성공",
                    map,
                    statisticsRepository.findUserTtsLengthByRequest(entityManager, vo.getUserNo(), map),
                    statisticsRepository.findDownloadMLengthByRequest(entityManager, vo.getUserNo(), map),
                    statisticsRepository.findUserTtsChartDataByRequest(vo.getUserNo(), (LocalDateTime) map.get("planStartAt"), (LocalDateTime) map.get("planEndAt")),
                    statisticsRepository.findDownloadDChartDataByRequest(vo.getUserNo(), (LocalDateTime) map.get("planStartAt"), (LocalDateTime) map.get("planEndAt"))
            );
        } catch (Exception e) {
            e.printStackTrace();
            return UserTimelineStatisticsResponseVo.of(StatisticsStatus.FAIL, "에러 발생. (" + e + ") 개발자에게 문의 바랍니다.");
        }
    }

    /**
     * 보이스폰트 통계 목록 페이징
     *
     * @param pageRequest
     * @return
     */
    @Override
    public VoiceFontStatisticsResponseVo getVoiceFontStatisticsDataPage(PageRequest pageRequest) {
        pageRequest.setSortColumns(new String[]{"conversionPtg", "downloadPtg"});
        Page<StatisticsRepository.VoiceFontStatisticsData> data = statisticsRepository.findDownloadDAndUserTTSStatisticsPage(pageRequest.of());
        if (data.getTotalElements() > 0) {
            return VoiceFontStatisticsResponseVo.of(StatisticsStatus.SUCCESS, "조회 성공", data);
        } else {
            return VoiceFontStatisticsResponseVo.of(StatisticsStatus.NOT_FOUND, "조회된 결과가 없습니다.");
        }
    }

    /**
     * 보이스폰트 정보 조회
     * @param characterId
     * @return
     */
    @Override
    public VFCharacterResponseVo getVFCharacterData(String characterId) {
        Optional<VFCharacterDataVo> opt = Optional.ofNullable(vfCharacterDRepository.findVFCharacterByCharacterId(entityManager, characterId));
        if(opt.isPresent()){
            List<Integer> yearList = statisticsRepository.findDownloadDAndUserTTSStatisticsYearByCharacterId(characterId);
            int year = yearList.stream().findFirst().isPresent() ? yearList.stream().findFirst().get() : 0000;
            return VFCharacterResponseVo.of(
                    StatisticsStatus.SUCCESS,
                    "조회 성공",
                    opt.get(),
                    statisticsRepository.findDownloadDAndUserTTSStatisticsByCharacterIdAndYear(characterId, year),
                    statisticsRepository.findDownloadDAndUserTTSStatisticsByCharacterId(characterId),
                    yearList
            );
        }else{
            return VFCharacterResponseVo.of(StatisticsStatus.NOT_FOUND, "조회된 보이스 폰트 정보가 없습니다.");
        }
    }

    /**
     * 보이스폰트 정보 조회 (연도별)
     * @param characterId
     * @param year
     * @return
     */
    @Override
    public VFCharacterResponseVo getVFCharacterDataList(String characterId, int year) {
        Optional<List<StatisticsRepository.VoiceFontStatisticsData>> opt = Optional.ofNullable(statisticsRepository.findDownloadDAndUserTTSStatisticsByCharacterIdAndYear(characterId, year));
        if(opt.isPresent()){
            return VFCharacterResponseVo.of(
                    StatisticsStatus.SUCCESS,
                    "조회 성공",
                    opt.get()
            );
        }else{
            return VFCharacterResponseVo.of(StatisticsStatus.NOT_FOUND, "조회된 목록이 없습니다.");
        }
    }

    /**
     * 보이스폰트 정보 저장
     * @param characterId
     * @param vo
     * @return
     */
    @Override
    public VFCharacterResponseVo setVFCharacterData(AvaUser avaUser, String characterId, VFCharacterRequestVo vo) {
        Optional<VFCharacter> opt = vfCharacterRepository.findByCharacterIdEquals(characterId);
        if(opt.isPresent()){
            VFCharacter vfCharacter =  opt.get();
            vfCharacter.setUpdatedBy(avaUser.getUserNo());
            vfCharacter.setUpdatedAt(LocalDateTime.now());
            vfCharacter = vfCharacterRepository.save(VFCharacterRequestVo.toVFCharacter(vfCharacter, vo.getData()));
            if(vfCharacter != null){
                Optional<VFCharacterD> dOptional = vfCharacterDRepository.findByIdEquals(vfCharacter.getId());
                VFCharacterD vfCharacterD;
                if(dOptional.isPresent()){
                    vfCharacterD = VFCharacterRequestVo.toVFCharacterD(dOptional.get(), vfCharacter, vo.getData());
                }else{
                    vfCharacterD = VFCharacterRequestVo.toVFCharacterD(new VFCharacterD(), vfCharacter, vo.getData());
                }
                if(vfCharacterDRepository.save(vfCharacterD) != null){
                    return VFCharacterResponseVo.of(StatisticsStatus.SUCCESS, "보이스 폰트 정보 저장이 완료되었습니다.");
                }else{
                    return VFCharacterResponseVo.of(StatisticsStatus.FAIL, "보이스 폰트 상세정보 등록에 실패했습니다.");
                }
            }else{
                return VFCharacterResponseVo.of(StatisticsStatus.FAIL, "보이스 폰트 수정에 실패했습니다.");
            }
        }else{
            return VFCharacterResponseVo.of(StatisticsStatus.NOT_FOUND, "조회된 보이스 폰트 정보가 없습니다.");
        }
    }

    @Override
    public List<MemoPostsResponseVo> getPaymentMemo(Long payNo) {
        Optional<PayMemo> payMemoOpt = payMemoRepository.findByPayNo(payNo);
        List<MemoPostsResponseVo> memoPostsResponseVos = new ArrayList<>();
        if (payMemoOpt.isPresent()) {
            PayMemo payMemo = payMemoOpt.get();
            memoPostsResponseVos = payMemo.getPosts().stream()
                    .map(MemoPostsResponseVo::of).collect(Collectors.toList());
        }

        return memoPostsResponseVos;
    }

    @Override
    public List<MemoPostsResponseVo> getUserTimelineMemo(Long userTimelineNo) {
        Optional<UserTimelineMemo> userTimelineMemoOpt = userTimelineMemoRepository.findByUserTimeLineNo(userTimelineNo);
        List<MemoPostsResponseVo> memoPostsResponseVos = new ArrayList<>();
        if (userTimelineMemoOpt.isPresent()) {
            UserTimelineMemo payMemo = userTimelineMemoOpt.get();
            memoPostsResponseVos = payMemo.getPosts().stream()
                    .map(MemoPostsResponseVo::of).collect(Collectors.toList());
        }

        return memoPostsResponseVos;
    }

    @Override
    public void addPaymentMemo(Long payNo, MemoPostRequestVo memoPostRequestVo) {
        try {
            Optional<PayMemo> payMemoOpt = payMemoRepository.findByPayNo(payNo);
            PayMemo payMemo;

            Post post = Post.builder()
                    .content(memoPostRequestVo.getContent())
                    .createdBy(memoPostRequestVo.getCreatedBy())
                    .createdAt(LocalDateTime.now())
                    .build();

            if (payMemoOpt.isPresent()) {
                payMemo = payMemoOpt.get();
                payMemo.addPost(post);
            } else {
                List<Post> posts = new ArrayList<>();
                posts.add(post);

                payMemo = new PayMemo();
                payMemo.setPayNo(payNo);
                payMemo.setPosts(posts);

            }
            payMemoRepository.save(payMemo);
        } catch (Exception e) {
            // TODO: exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void addUserTimelineMemo(Long userTimelineNo, MemoPostRequestVo memoPostRequestVo) {
        try {
            Optional<UserTimelineMemo> userTimelineMemoOpt = userTimelineMemoRepository.findByUserTimeLineNo(userTimelineNo);
            UserTimelineMemo userTimelineMemo;

            Post post = Post.builder()
                    .content(memoPostRequestVo.getContent())
                    .createdBy(memoPostRequestVo.getCreatedBy())
                    .createdAt(LocalDateTime.now())
                    .build();

            if (userTimelineMemoOpt.isPresent()) {
                userTimelineMemo = userTimelineMemoOpt.get();
                userTimelineMemo.addPost(post);
            } else {
                List<Post> posts = new ArrayList<>();
                posts.add(post);

                userTimelineMemo = new UserTimelineMemo();
                userTimelineMemo.setUserTimeLineNo(userTimelineNo);
                userTimelineMemo.setPosts(posts);

            }
            userTimelineMemoRepository.save(userTimelineMemo);
        } catch (Exception e) {
            // TODO: exception handling
            e.printStackTrace();
        }
    }

}

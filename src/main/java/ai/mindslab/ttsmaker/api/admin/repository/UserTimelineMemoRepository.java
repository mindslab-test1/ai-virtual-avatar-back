package ai.mindslab.ttsmaker.api.admin.repository;

import ai.mindslab.ttsmaker.api.admin.domain.PayMemo;
import ai.mindslab.ttsmaker.api.admin.domain.UserTimelineMemo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserTimelineMemoRepository extends JpaRepository<UserTimelineMemo, Long> {
    Optional<UserTimelineMemo> findByUserTimeLineNo(Long userTimelineNo);
}

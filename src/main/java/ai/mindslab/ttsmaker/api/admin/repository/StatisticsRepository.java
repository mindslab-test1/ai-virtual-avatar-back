package ai.mindslab.ttsmaker.api.admin.repository;

import ai.mindslab.ttsmaker.api.admin.domain.QStatistics;
import ai.mindslab.ttsmaker.api.admin.domain.Statistics;
import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.vo.request.UserTimelineStatisticsRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.response.UserTimelineStatisticsResponseVo;
import ai.mindslab.ttsmaker.api.character.domain.QVFCharacter;
import ai.mindslab.ttsmaker.api.download.domain.QDownloadM;
import ai.mindslab.ttsmaker.api.pay.domain.QBilling;
import ai.mindslab.ttsmaker.api.plan.domain.QPlan;
import ai.mindslab.ttsmaker.api.user.domain.*;
import ai.mindslab.ttsmaker.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ai.mindslab.ttsmaker.api.admin.repository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-20
 */
@Repository
public interface StatisticsRepository extends JpaRepository<Statistics, Long>, JpaSpecificationExecutor<Statistics> {

    QUserM userM = QUserM.userM;
    QBilling billing = QBilling.billing;
    QVFCharacter vFCharacter = QVFCharacter.vFCharacter;
    QDownloadM downloadM = QDownloadM.downloadM;
    QUserTts userTts = QUserTts.userTts;
    QStatistics statistics = QStatistics.statistics;
    QUserTimeline userTimeline = QUserTimeline.userTimeline;
    QUserPlan userPlan = QUserPlan.userPlan;
    QPlan plan = QPlan.plan;

    /**
     * 유저수 조회
     * @param entityManager
     * @return
     */
    default long countUserMByActive(EntityManager entityManager){
        return new JPAQuery<Long>(entityManager)
                .select(userM.userNo.count())
                .from(userM)
                .where(userM.active.isTrue())
                .fetchCount();
    }

    /**
     * 유료결제 회원수 조회
     * @param entityManager
     * @return
     */
    default long countBillingByActive(EntityManager entityManager){
        return new JPAQuery<Long>(entityManager)
                .select(userM.userId.count())
                .from(userM)
                .leftJoin(billing).on(userM.userNo.eq(billing.userNo))
                .where(billing.active.isTrue())
                .groupBy(userM.userId)
                .fetchCount();
    }

    /**
     * 보이스폰트 수 조회
     * @param entityManager
     * @return
     */
    default long countVfCharacterByDelYn(EntityManager entityManager){
        return new JPAQuery<Long>(entityManager)
                .select(vFCharacter.id.count())
                .from(vFCharacter)
                .where(vFCharacter.isDeleted.eq("N"))
                .fetchOne();
    }

    /**
     * 전체 다운로드 문자열 수 조회
     * @param entityManager
     * @return
     */
    default long sumDownloadChar(EntityManager entityManager){
        return new JPAQuery<Integer>(entityManager)
                .select(downloadM.downloadExpectCharCnt.sum())
                .from(downloadM)
                .fetchOne();
    }

    /**
     * 전체 문자열 변환 수 조회
     * @param entityManager
     * @return
     */
    default long sumTtsConversionChar(EntityManager entityManager){
        return new JPAQuery<Long>(entityManager)
                .select(Expressions.numberTemplate(Long.class, "CHAR_LENGTH({0})", userTts.scriptText).sum())
                .from(userTts)
                .fetchOne();
    }

    /**
     * 메인통계 조회 (생성일자)
     * @param entityManager
     * @param date
     * @return
     */
    default Statistics findByCreatedAtEquals(EntityManager entityManager, LocalDate date){
        return new JPAQuery<Statistics>(entityManager)
                .select(statistics)
                .from(statistics)
                .where(
                        Expressions.stringTemplate("DATE_FORMAT({0}, {1})", statistics.createdAt, ConstantImpl.create("%Y-%m-%d")).eq(String.valueOf(date))
                ).orderBy(statistics.no.desc()).limit(1).fetchOne();
    }

    /**
     * 유저 타임라인 조회
     * @param entityManager
     * @param vo
     * @return
     */
    default UserTimelineStatisticsResponseVo findUserTimelineNoByRequest(EntityManager entityManager, UserTimelineStatisticsRequestVo vo){
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if(vo.getUserNo() > 0) booleanBuilder.and(userM.userNo.eq(vo.getUserNo()));
        if(!StringUtils.isEmpty(vo.getUserId())) booleanBuilder.and(userM.userId.eq(vo.getUserId()));
        if(!StringUtils.isEmpty(vo.getUserNm())) booleanBuilder.and(userM.userNm.eq(vo.getUserNm()));
        if(!StringUtils.isEmpty(vo.getCellPhone())) booleanBuilder.and(userM.cellPhone.eq(vo.getCellPhone()));
        List<UserM> resultUserM = new JPAQuery<UserM>(entityManager)
                .select(
                        userM
                ).from(userM)
                .where(booleanBuilder).fetch();
        if(resultUserM.size() > 1){
            // 예외, 단건 조회시에만 타임라인 조회가 가능함
            return UserTimelineStatisticsResponseVo.of(StatisticsStatus.MULTIPLE_ROW, "유저가 여러건 검색되었습니다. 검색 조건을 확인해주세요.");
        } else if(resultUserM.size() == 0){
            return UserTimelineStatisticsResponseVo.of(StatisticsStatus.NOT_FOUND, "유저가 존재하지 않습니다. 검색 조건을 확인해주세요.");
        } else {
            List<Long> timeline = new JPAQuery<Long>(entityManager)
                    .select(
                            userTimeline.no
                    ).from(userTimeline)
                    .where(userTimeline.user().eq(resultUserM.stream().findFirst().get()))
                    .fetch();
            if(timeline.size() > 0){
                return UserTimelineStatisticsResponseVo.of(
                        StatisticsStatus.SUCCESS,
                        "조회 성공",
                        resultUserM.stream().findFirst().get().getUserNo(),
                        timeline
                );
            }else{
                return UserTimelineStatisticsResponseVo.of(StatisticsStatus.NOT_FOUND, "조회된 구독정보가 없습니다.");
            }
        }
    }

    /**
     * timeline start, end date, planName 조회
     * @param entityManager
     * @param vo
     * @return
     */
    default Map<String, Object> findUserPlanDateAndPlanNameByUserTimeline(EntityManager entityManager, UserTimelineStatisticsRequestVo vo){
        /*
            SELECT
                (SELECT
                        UP.PLAN_START_AT
                    FROM USER_TIMELINE UT
                    INNER JOIN USER_PLAN UP ON UT.USER_PLAN_NO = UP.NO
                    WHERE UP.USER_NO = 1 -- 회원번호
                    AND UT.NO = 2 -- TIMELINE 회차, 전체일 경우 > 0
                    ORDER BY UT.NO
                    LIMIT 1) startAt,      (SELECT
                    UP.PLAN_END_AT
                FROM USER_TIMELINE UT
                INNER JOIN USER_PLAN UP ON UT.USER_PLAN_NO = UP.NO
                WHERE UP.USER_NO = 1 -- 회원번호
                AND UT.NO = 2 -- TIMELINE 회차, 전체일 경우 > 0
                ORDER BY UT.NO DESC
                LIMIT 1
                ) endAt;
         */
        /*
            return (Tuple) new JPAQuery()
                .select(
                        ExpressionUtils.as(
                                JPAExpressions.select(userPlan.planStartAt)
                                        .from(userTimeline)
                                        .innerJoin(userPlan).on(userTimeline.userPlan().eq(userPlan))
                                        .where(userPlan.user().eq(
                                                JPAExpressions.select(userM)
                                                        .from(userM)
                                                        .where(userM.userNo.eq(vo.getUserNo()))
                                        ).and(vo.getTimelineNo() != 0 ? userTimeline.no.eq(vo.getTimelineNo()) : userTimeline.no.gt(0)))
                                        .orderBy(userTimeline.no.asc())
                                        .limit(1),
                                "planStartAt"
                        ),
                        ExpressionUtils.as(
                                JPAExpressions.select(userPlan.planEndAt)
                                        .from(userTimeline)
                                        .innerJoin(userPlan).on(userTimeline.userPlan().eq(userPlan))
                                        .where(userPlan.user().eq(
                                                JPAExpressions.select(userM)
                                                        .from(userM)
                                                        .where(userM.userNo.eq(vo.getUserNo()))
                                        ).and(vo.getTimelineNo() != 0 ? userTimeline.no.eq(vo.getTimelineNo()) : userTimeline.no.gt(0)))
                                        .orderBy(userTimeline.no.desc())
                                        .limit(1),
                                "planEndAt"
                        )
                ).from(userPlan)
                .fetchOne();
         */
        // FIXME 추후 위 코드 토대로 SUBQUERY 형태로 수정필요
        Map<String, Object> map = new HashMap<>();
        map.put("planStartAt", new JPAQuery<LocalDateTime>(entityManager)
                .select(userPlan.planStartAt)
                .from(userTimeline)
                .innerJoin(userPlan).on(userTimeline.userPlan().eq(userPlan))
                .limit(1)
                .where(userPlan.user().eq(
                        JPAExpressions.select(userM)
                                .from(userM)
                                .where(userM.userNo.eq(vo.getUserNo()))
                ).and(vo.getTimelineNo() != 0 ? userTimeline.no.eq(vo.getTimelineNo()) : userTimeline.no.gt(0)))
                .orderBy(userTimeline.no.asc())
                .limit(1)
                .fetchOne());
        map.put("planEndAt", new JPAQuery<LocalDateTime>(entityManager)
                .select(userPlan.planEndAt)
                .from(userTimeline)
                .innerJoin(userPlan).on(userTimeline.userPlan().eq(userPlan))
                .limit(1)
                .where(userPlan.user().eq(
                        JPAExpressions.select(userM)
                                .from(userM)
                                .where(userM.userNo.eq(vo.getUserNo()))
                ).and(vo.getTimelineNo() != 0 ? userTimeline.no.eq(vo.getTimelineNo()) : userTimeline.no.gt(0)))
                .orderBy(userTimeline.no.desc())
                .limit(1)
                .fetchOne());
        map.put("planNm", vo.getTimelineNo() != 0 ? new JPAQuery<String>(entityManager)
                .select(userPlan.plan().planNm)
                .from(userTimeline)
                .innerJoin(userPlan).on(userTimeline.userPlan().eq(userPlan))
                .innerJoin(plan).on(userPlan.plan().eq(plan))
                .limit(1)
                .where(userPlan.user().eq(
                        JPAExpressions.select(userM)
                                .from(userM)
                                .where(userM.userNo.eq(vo.getUserNo()))
                ).and(vo.getTimelineNo() != 0 ? userTimeline.no.eq(vo.getTimelineNo()) : userTimeline.no.gt(0)))
                .fetchOne() : "-");
        return map;
    }

    /**
     * 전체 변환길이 조회
     * @param entityManager
     * @param userNo
     * @param dateTimeMap
     * @return
     */
    default long findUserTtsLengthByRequest(EntityManager entityManager, long userNo, Map<String, Object> dateTimeMap){
        /*
            SELECT
                IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) AS totConversionLength -- 전체 변환길이
            FROM USER_TTS UT
            WHERE UT.CREATED_AT BETWEEN ? AND ?
              AND USER_NO = 1 -- 회원번호
            AND CHARACTER_ID IS NOT NULL AND SCRIPT_ID IS NOT NULL;
         */
        return new JPAQuery<Integer>(entityManager)
                .select(
                        Expressions.numberTemplate(Long.class, "CHAR_LENGTH({0})", userTts.scriptText).sum().coalesce(0L).as("conversionCharCnt")
                ).from(userTts)
                .where(
                        userTts.createdAt.between((LocalDateTime) dateTimeMap.get("planStartAt"), (LocalDateTime) dateTimeMap.get("planEndAt")).and(userTts.user().eq(
                                JPAExpressions.select(userM)
                                        .from(userM)
                                        .where(userM.userNo.eq(userNo))
                        )).and(userTts.characterId.isNotNull()).and(userTts.scriptId.isNotNull())
                ).fetchOne();
    }

    /**
     * 전체 다운로드 글자수조회
     * @param entityManager
     * @param userNo
     * @param dateTimeMap
     * @return
     */
    default long findDownloadMLengthByRequest(EntityManager entityManager, long userNo, Map<String, Object> dateTimeMap){
        /*
            SELECT
                SUM(DM.DOWNLOAD_CHAR_CNT)
            FROM DOWNLOAD_M DM
            WHERE DM.CREATED_AT BETWEEN ? AND ?
            AND USER_NO = 1;
         */
        return new JPAQuery<Integer>(entityManager)
                .select(
                        downloadM.downloadExpectCharCnt.sum().coalesce(0).as("downloadCharCnt")
                ).from(downloadM)
                .where(
                        downloadM.createdAt.between((LocalDateTime) dateTimeMap.get("planStartAt"), (LocalDateTime) dateTimeMap.get("planEndAt"))
                                .and(downloadM.userNo.eq(userNo))
                ).fetchOne();
    }

    /**
     * 변환 차트 데이터
     * @param userNo
     * @param startDate
     * @param endDate
     * @return
     */
    @Query(
            value = "SELECT\n" +
                    "    IFNULL(result.rlength * 100 / result.tLENGTH, 0) AS ptg, \n" +
                    "    IFNULL(result.rlength, 0) AS length, \n" +
                    "    result.characterId AS characterId, \n" +
                    "    result.characterNm AS characterNm \n" +
                    "FROM(\n" +
                    "    SELECT\n" +
                    "           (SELECT SUM(CHAR_LENGTH(UT.SCRIPT_TEXT))\n" +
                    "            FROM USER_TTS UT\n" +
                    "            WHERE UT.CHARACTER_ID = VC.CHARACTER_ID\n" +
                    "              AND UT.CREATED_AT BETWEEN ?2 AND ?3 \n" +
                    "              AND UT.USER_NO = ?1 \n" +
                    "           ) AS rlength,\n" +
                    "           TT.LENGTH AS tlength,\n" +
                    "           VC.CHARACTER_ID AS characterId,\n" +
                    "           VC.CHARACTER_NM AS characterNm\n" +
                    "    FROM VF_CHARACTER VC,\n" +
                    "         (\n" +
                    "             SELECT SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)) AS LENGTH\n" +
                    "             FROM VF_CHARACTER VC\n" +
                    "                      INNER JOIN USER_TTS UT ON VC.CHARACTER_ID = UT.CHARACTER_ID\n" +
                    "             WHERE UT.CREATED_AT BETWEEN ?2 AND ?3 \n" +
                    "             AND UT.USER_NO = ?1 \n" +
                    "         ) TT\n" +
                    "    ORDER BY LENGTH DESC\n" +
                    ") result",
            nativeQuery = true
    )
    List<ChartData> findUserTtsChartDataByRequest(long userNo, Object startDate, Object endDate);

    /**
     * 다운로드 차트 데이터
     * @param userNo
     * @param startDate
     * @param endDate
     * @return
     */
    @Query(
            value = "SELECT\n" +
                    "    IFNULL(result.rlength * 100 / result.tLENGTH, 0) AS ptg, \n" +
                    "    IFNULL(result.rlength, 0) AS length, \n" +
                    "    result.characterId AS characterId, \n" +
                    "    result.characterNm AS characterNm \n" +
                    "FROM (\n" +
                    "    SELECT\n" +
                    "           (SELECT IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0)\n" +
                    "            FROM DOWNLOAD_D DD\n" +
                    "            WHERE DD.CHARACTER_ID = VC.CHARACTER_ID\n" +
                    "              AND DD.CREATED_AT BETWEEN ?2 AND ?3 \n" +
                    "              AND DD.USER_NO = ?1 \n" +
                    "           )                   AS rlength,\n" +
                    "           TT.LENGTH           AS tlength,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC,\n" +
                    "         (\n" +
                    "             SELECT SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)) AS LENGTH\n" +
                    "             FROM VF_CHARACTER VC\n" +
                    "                      INNER JOIN DOWNLOAD_D DD ON VC.CHARACTER_ID = DD.CHARACTER_ID\n" +
                    "             WHERE DD.CREATED_AT BETWEEN ?2 AND ?3 \n" +
                    "               AND DD.USER_NO = ?1 \n" +
                    "         ) TT\n" +
                    "    ORDER BY LENGTH DESC\n" +
                    ") result",
            nativeQuery = true
    )
    List<ChartData> findDownloadDChartDataByRequest(long userNo, Object startDate, Object endDate);

    /**
     * 보이스폰트 통계 목록 페이징
     * @param pageable
     * @return
     */
    @Query(
            value = "SELECT\n" +
                    "    VC.CHARACTER_ID AS characterId,\n" +
                    "    VC.CHARACTER_NM AS characterNm,\n" +
                    "    VC.AVATAR_CLASS_NM AS classNm,\n" +
                    "    VC.CREATED_AT AS createdAt,\n" +
                    "    IFNULL(DD.dLength, 0) AS downloadLength,\n" +
                    "    IFNULL(DD.ptg, 0) AS downloadPtg,\n" +
                    "    IFNULL(MDD.dLength, 0) AS monthDownloadLength,\n" +
                    "    IFNULL(MDD.ptg, 0) AS monthDownloadPtg,\n" +
                    "    IFNULL(UT.uLength, 0) AS conversionLength,\n" +
                    "    IFNULL(UT.ptg, 0) AS conversionPtg,\n" +
                    "    IFNULL(MUT.uLength, 0) AS monthConversionLength,\n" +
                    "    IFNULL(MUT.ptg, 0) AS monthConversionPtg\n" +
                    "FROM VF_CHARACTER VC\n" +
                    "     LEFT JOIN\n" +
                    "     (\n" +
                    "    SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) AS dLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM DOWNLOAD_D) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN DOWNLOAD_D DD ON VC.CHARACTER_ID = DD.CHARACTER_ID\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC\n" +
                    ") DD ON VC.CHARACTER_ID = DD.characterId\n" +
                    "     LEFT JOIN\n" +
                    "     (\n" +
                    "    SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) AS dLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM DOWNLOAD_D WHERE MONTH(CREATED_AT) = MONTH(NOW())) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN DOWNLOAD_D DD ON VC.CHARACTER_ID = DD.CHARACTER_ID\n" +
                    "    WHERE MONTH(DD.CREATED_AT) = MONTH(NOW())\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC\n" +
                    ") MDD ON VC.CHARACTER_ID = MDD.characterId\n" +
                    "LEFT JOIN (SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) AS uLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM USER_TTS WHERE CHARACTER_ID IS NOT NULL) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN USER_TTS UT ON VC.CHARACTER_ID = UT.CHARACTER_ID\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC) UT ON VC.CHARACTER_ID = UT.characterId\n" +
                    "LEFT JOIN (SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) AS uLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM USER_TTS WHERE CHARACTER_ID IS NOT NULL AND MONTH(CREATED_AT) = MONTH(NOW())) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN USER_TTS UT ON VC.CHARACTER_ID = UT.CHARACTER_ID\n" +
                    "    WHERE MONTH(UT.CREATED_AT) = MONTH(NOW())\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC) MUT ON VC.CHARACTER_ID = MUT.characterId\n",
            countQuery = "SELECT\n" +
                         "    COUNT(ID)\n" +
                         "FROM VF_CHARACTER",
            nativeQuery = true
    )
    Page<VoiceFontStatisticsData> findDownloadDAndUserTTSStatisticsPage(Pageable pageable);

    /**
     * 월간 사용량 목록 (보이스폰트, 연도 조건)
     * @param characterId
     * @param year
     * @return
     */
    @Query(
            value = "SELECT CAST(SUBSTR(MDD.YYYYMM, 6) AS DECIMAL) AS mm,\n" +
                    "       IFNULL(UT.uLength, 0)               AS conversionLength,\n" +
                    "       IFNULL(UT.ptg, 0)                   AS conversionPtg,\n" +
                    "       IFNULL(DD.dLength, 0)               AS downloadLength,\n" +
                    "       IFNULL(DD.ptg, 0)                   AS downloadPtg\n" +
                    "FROM (\n" +
                    "    SELECT DATE_FORMAT(MDD.CREATED_AT, '%Y-%m') AS YYYYMM\n" +
                    "FROM DOWNLOAD_D MDD\n" +
                    "WHERE MDD.CHARACTER_ID = ?1\n" +
                    "GROUP BY DATE_FORMAT(MDD.CREATED_AT, '%Y-%m')\n" +
                    "UNION\n" +
                    "SELECT DATE_FORMAT(MUT.CREATED_AT, '%Y-%m') AS YYYYMM\n" +
                    "FROM USER_TTS MUT\n" +
                    "WHERE MUT.CHARACTER_ID = ?1\n" +
                    "GROUP BY DATE_FORMAT(MUT.CREATED_AT, '%Y-%m')\n" +
                    "    ) MDD\n" +
                    "         LEFT JOIN (\n" +
                    "    SELECT DATE_FORMAT(UT.CREATED_AT, '%Y-%m')                                                 AS YYYYMM,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0)                                         AS uLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length\n" +
                    "                                                                FROM USER_TTS\n" +
                    "                                                                WHERE CHARACTER_ID = ?1\n" +
                    "                                                                  AND YEAR(CREATED_AT) = ?2 AND CHARACTER_ID IS NOT NULL) AS ptg\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "             LEFT JOIN USER_TTS UT ON VC.CHARACTER_ID = UT.CHARACTER_ID\n" +
                    "    WHERE UT.CHARACTER_ID = ?1\n" +
                    "      AND YEAR(UT.CREATED_AT) = ?2\n" +
                    "     AND UT.CHARACTER_ID IS NOT NULL\n" +
                    "    GROUP BY YYYYMM\n" +
                    ") UT ON UT.YYYYMM = MDD.YYYYMM\n" +
                    "         LEFT JOIN (\n" +
                    "    SELECT DATE_FORMAT(DD.CREATED_AT, '%Y-%m')                                                 AS YYYYMM,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0)                                         AS dLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length\n" +
                    "                                                                FROM DOWNLOAD_D\n" +
                    "                                                                WHERE CHARACTER_ID = ?1\n" +
                    "                                                                  AND YEAR(CREATED_AT) = ?2) AS ptg\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "             LEFT JOIN DOWNLOAD_D DD ON VC.CHARACTER_ID = DD.CHARACTER_ID\n" +
                    "    WHERE DD.CHARACTER_ID = ?1\n" +
                    "      AND YEAR(DD.CREATED_AT) = ?2\n" +
                    "    GROUP BY YYYYMM\n" +
                    ") DD ON DD.YYYYMM = MDD.YYYYMM\n" +
                    "GROUP BY MDD.YYYYMM\n" +
                    "ORDER BY mm DESC",
            nativeQuery = true
    )
    List<VoiceFontStatisticsData> findDownloadDAndUserTTSStatisticsByCharacterIdAndYear(String characterId, int year);

    /**
     * 전체 사용량 목록 (보이스폰트 조건)
     * @param characterId
     * @return
     */
    @Query(
            value = "SELECT\n" +
                    "    IFNULL(DD.dLength, 0) AS downloadLength,\n" +
                    "    IFNULL(DD.ptg, 0) AS downloadPtg,\n" +
                    "    IFNULL(MDD.dLength, 0) AS monthDownloadLength,\n" +
                    "    IFNULL(MDD.ptg, 0) AS monthDownloadPtg,\n" +
                    "    IFNULL(UT.uLength, 0) AS conversionLength,\n" +
                    "    IFNULL(UT.ptg, 0) AS conversionPtg,\n" +
                    "    IFNULL(MUT.uLength, 0) AS monthConversionLength,\n" +
                    "    IFNULL(MUT.ptg, 0) AS monthConversionPtg\n" +
                    "FROM VF_CHARACTER VC\n" +
                    "     LEFT JOIN\n" +
                    "     (\n" +
                    "    SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) AS dLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM DOWNLOAD_D) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN DOWNLOAD_D DD ON VC.CHARACTER_ID = DD.CHARACTER_ID\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC\n" +
                    ") DD ON VC.CHARACTER_ID = DD.characterId\n" +
                    "     LEFT JOIN\n" +
                    "     (\n" +
                    "    SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) AS dLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(DD.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM DOWNLOAD_D WHERE MONTH(CREATED_AT) = MONTH(NOW())) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN DOWNLOAD_D DD ON VC.CHARACTER_ID = DD.CHARACTER_ID\n" +
                    "    WHERE MONTH(DD.CREATED_AT) = MONTH(NOW())\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC\n" +
                    ") MDD ON VC.CHARACTER_ID = MDD.characterId\n" +
                    "LEFT JOIN (SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) AS uLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM USER_TTS WHERE CHARACTER_ID IS NOT NULL) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN USER_TTS UT ON VC.CHARACTER_ID = UT.CHARACTER_ID\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC) UT ON VC.CHARACTER_ID = UT.characterId\n" +
                    "LEFT JOIN (SELECT\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) AS uLength,\n" +
                    "           IFNULL(SUM(CHAR_LENGTH(UT.SCRIPT_TEXT)), 0) * 100 / (SELECT SUM(CHAR_LENGTH(SCRIPT_TEXT)) AS length FROM USER_TTS WHERE CHARACTER_ID IS NOT NULL AND MONTH(CREATED_AT) = MONTH(NOW())) AS ptg,\n" +
                    "           VC.CHARACTER_ID     AS characterId,\n" +
                    "           VC.CHARACTER_NM     AS characterNm\n" +
                    "    FROM VF_CHARACTER VC\n" +
                    "         LEFT JOIN USER_TTS UT ON VC.CHARACTER_ID = UT.CHARACTER_ID\n" +
                    "    WHERE MONTH(UT.CREATED_AT) = MONTH(NOW())\n" +
                    "    GROUP BY characterNm, characterId\n" +
                    "    ORDER BY ptg DESC) MUT ON VC.CHARACTER_ID = MUT.characterId\n" +
                    "WHERE VC.CHARACTER_ID = ?1",
            nativeQuery = true
    )
    VoiceFontStatisticsData findDownloadDAndUserTTSStatisticsByCharacterId(String characterId);

    /**
     * 연도 산출 (봇이스폰트 조건)
     * @param characterId
     * @return
     */
    @Query(
            value = "SELECT MDD.YYYY AS yyyy\n" +
                    "FROM (\n" +
                    "         SELECT DATE_FORMAT(MDD.CREATED_AT, '%Y') AS YYYY\n" +
                    "         FROM DOWNLOAD_D MDD\n" +
                    "         WHERE MDD.CHARACTER_ID = ?1\n" +
                    "         GROUP BY DATE_FORMAT(MDD.CREATED_AT, '%Y')\n" +
                    "         UNION\n" +
                    "         SELECT DATE_FORMAT(MUT.CREATED_AT, '%Y') AS YYYY\n" +
                    "         FROM USER_TTS MUT\n" +
                    "         WHERE MUT.CHARACTER_ID = ?1\n" +
                    "         GROUP BY DATE_FORMAT(MUT.CREATED_AT, '%Y')\n" +
                    "     ) MDD",
            nativeQuery = true
    )
    List<Integer> findDownloadDAndUserTTSStatisticsYearByCharacterId(String characterId);

    interface ChartData{
        Double getPtg();
        Double getLength();
        String getCharacterId();
        String getCharacterNm();
    }

    interface VoiceFontStatisticsData{
        String getYyyy();
        String getMm();
        String getCharacterId();
        String getCharacterNm();
        String getClassNm();
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
        LocalDateTime getCreatedAt();
        Long getDownloadLength();
        Double getDownloadPtg();
        Long getMonthDownloadLength();
        Double getMonthDownloadPtg();
        Long getConversionLength();
        Double getConversionPtg();
        Long getMonthConversionLength();
        Double getMonthConversionPtg();
    }

}

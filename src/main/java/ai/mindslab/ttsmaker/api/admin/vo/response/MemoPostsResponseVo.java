package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.admin.domain.Post;
import ai.mindslab.ttsmaker.api.admin.vo.request.MemoPostRequestVo;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.*;

import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemoPostsResponseVo {
    private Long createdBy;
    private String createdAt;
    private String content;

    public static MemoPostsResponseVo of(Post entity) {
        return MemoPostsResponseVo.builder()
                .createdBy(entity.getCreatedBy())
                .createdAt(entity.getCreatedAt().toString())
                .content(entity.getContent())
                .build();
    }
}

package ai.mindslab.ttsmaker.api.admin.repository;

import ai.mindslab.ttsmaker.api.admin.domain.PayMemo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PayMemoRepository extends JpaRepository<PayMemo, Long> {
    Optional<PayMemo> findByPayNo(Long payNo);
}

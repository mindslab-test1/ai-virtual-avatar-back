package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InquiryListResponseDto {
    private Long inquiryNo;
    private String inquiryType;
    private String userNm;
    private String userId;
    private String userCellPhone;
    private String inquiryTitle;
    private String answeredEmail;
    private Boolean inquiryStatus;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yy.MM.dd")
    private LocalDateTime questionDate;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yy.MM.dd")
    private LocalDateTime answerDate;
    
    public static InquiryListResponseDto of(UserInquiry entity) {
        UserM user = entity.getUser();
        
        return InquiryListResponseDto.builder()
            .inquiryNo(entity.getInquiryNo())
            .inquiryType(entity.getInquiryType())
            .userNm(user.getUserNm())
            .userId(user.getUserId())
            .userCellPhone(user.getCellPhone())
            .inquiryTitle(entity.getInquiryTitle())
            .answeredEmail(entity.getAnsweredEmail())
            .inquiryStatus(entity.getInquiryStatus())
            .questionDate(entity.getCreatedAt())
            .answerDate(entity.getAnsweredAt())
            .build();
    }
}

package ai.mindslab.ttsmaker.api.admin.vo.response;

import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.repository.StatisticsRepository;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-24  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-24
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTimelineStatisticsResponseVo {

    private String statisticsStatus;
    private String message;
    private long userNo;
    private List<Long> timeline;
    private String planNm;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    private LocalDateTime planStartAt;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
    private LocalDateTime planEndAt;
    private long totConversionLength;
    private long totDownloadLength;
    private List<StatisticsRepository.ChartData> conversionChartData;
    private List<StatisticsRepository.ChartData> downloadChartData;

    public static UserTimelineStatisticsResponseVo of(StatisticsStatus statisticsStatus, String msg) {
        return UserTimelineStatisticsResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .build();
    }

    public static UserTimelineStatisticsResponseVo of(StatisticsStatus statisticsStatus, String msg, long userNo, List<Long> timeline) {
        return UserTimelineStatisticsResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .userNo(userNo)
                .timeline(timeline)
                .build();
    }

    public static UserTimelineStatisticsResponseVo of(StatisticsStatus statisticsStatus, String msg, Map<String, Object> dateTimeMap, long totConversionLength, long totDownloadLength, List<StatisticsRepository.ChartData> conversionChartData, List<StatisticsRepository.ChartData> downloadChartData) {
        return UserTimelineStatisticsResponseVo.builder()
                .statisticsStatus(statisticsStatus.name())
                .message(msg)
                .planNm((String) dateTimeMap.get("planNm"))
                .planStartAt((LocalDateTime) dateTimeMap.get("planStartAt"))
                .planEndAt((LocalDateTime) dateTimeMap.get("planEndAt"))
                .totConversionLength(totConversionLength)
                .totDownloadLength(totDownloadLength)
                .conversionChartData(conversionChartData)
                .downloadChartData(downloadChartData)
                .build();
    }

}

package ai.mindslab.ttsmaker.api.admin.vo;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserRole;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoVo implements Serializable {
    private static final long serialVersionUID = 5374884932662896408L;
    private Long userNo;
    private String userId;
    private String userPw;
    private String userNm;
    private String companyNm;
    private String phoneNum;
    private String registeredDate;
    private String cancelledDate;
    private Set<String> roles;
    private String currPlanNm;
    private Boolean active;

    public static UserInfoVo of(UserM entity) {

        List<UserRole> roleList = entity.getUserRoles();
        Set<String> roles = new HashSet<>();
        if (!CollectionUtils.isEmpty(roleList)) {
            roles = roleList.stream().map(UserRole::getRoleNm).map(UserRoleEnum::name).collect(HashSet::new, HashSet::add, HashSet::addAll);
        } else {
            roles.add("ROLE_USER");
        }


        List<UserPlan> userPlans = entity.getUserPlans();
        String currPlanNm = "";
        if (!userPlans.isEmpty()) {
            currPlanNm = userPlans.stream()
                    .sorted(Comparator.comparingLong(UserPlan::getNo)
                            .reversed())
                    .map(UserPlan::getPlan)
                    .map(Plan::getPlanNm)
                    .collect(Collectors.toList()).get(0);
        }

        return UserInfoVo.builder()
                .userNo(entity.getUserNo())
                .userId(entity.getUserId())
                .userPw(entity.getUserPw())
                .userNm(entity.getUserNm())
                .companyNm(entity.getCompanyNm())
                .phoneNum(entity.getCellPhone())
                .registeredDate(DateUtils.asString(entity.getCreatedAt(), "yyyy-MM-dd"))
                .roles(roles)
                .currPlanNm(currPlanNm)
                .active(entity.getActive())
                .build();
    }
}

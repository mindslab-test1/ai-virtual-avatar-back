package ai.mindslab.ttsmaker.api.admin.vo.request;

import ai.mindslab.ttsmaker.api.admin.domain.Statistics;
import lombok.*;
import org.springframework.beans.BeanUtils;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.request
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-20
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsDataVo {

    private long totUser;
    private long paidUser;
    private long activeUser;
    private long totLoginCount;
    private long totVfCharacter;
    private long totDownloadLength;
    private long totConversionLength;

    public static Statistics toEntity(StatisticsDataVo vo){
        Statistics statistics = new Statistics();
        BeanUtils.copyProperties(vo, statistics);
        return statistics;
    }

}

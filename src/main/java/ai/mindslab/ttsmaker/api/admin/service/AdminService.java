package ai.mindslab.ttsmaker.api.admin.service;

import ai.mindslab.ttsmaker.api.admin.vo.UserInfoVo;
import ai.mindslab.ttsmaker.api.admin.vo.request.MemoPostRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.request.UserTimelineStatisticsRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.request.VFCharacterRequestVo;
import ai.mindslab.ttsmaker.api.admin.vo.response.*;
import ai.mindslab.ttsmaker.api.user.vo.request.InquirySearchRequestDto;
import ai.mindslab.ttsmaker.api.user.vo.request.UserFilterRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserTimelineResponseVo;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;

import java.util.List;

public interface AdminService {

    List<UserTimelineResponseVo> getUserTimelineFindByUserNo(Long userNo);

    String getFilteredUsers(UserFilterRequestVo userFilterRequestVo);

    UserInfoVo getUserByUserNo(Long userNo);

    /**
     * 통계데이터 적재
     */
    void setStatisticsMain();

    /**
     * 통계 데이터 추출
     * @param date
     * @return
     */
    StatisticsMainResponseVo getStatisticsMain(int date);
    
    AvaPage<InquiryListResponseDto> getInquiryList(org.springframework.data.domain.PageRequest pageable, InquirySearchRequestDto searchRequestDto);

    InquiryDetailResponseDto getInquiryDetail(Long inquiryNo);

    /**
     * 유저 타임라인 목록 조회
     * @param vo
     * @return
     */
    UserTimelineStatisticsResponseVo getUserTimelineList(UserTimelineStatisticsRequestVo vo);

    /**
     * 유저 타임라인 통계 조회
     * @param vo
     * @return
     */
    UserTimelineStatisticsResponseVo getUserTimelineStatistics(UserTimelineStatisticsRequestVo vo);

    /**
     * 보이스폰트 통계 목록 페이징
     * @param pageRequest
     * @return
     */
    VoiceFontStatisticsResponseVo getVoiceFontStatisticsDataPage(ai.mindslab.ttsmaker.common.page.PageRequest pageRequest);

    /**
     * 보이스폰트 정보 조회
     * @param characterId
     * @return
     */
    VFCharacterResponseVo getVFCharacterData(String characterId);

    /**
     * 보이스폰트 정보 조회 (연도별)
     * @param characterId
     * @return
     */
    VFCharacterResponseVo getVFCharacterDataList(String characterId, int year);

    /**
     * 보이스폰트 정보 저장
     * @param characterId
     * @param vo
     * @return
     */
    VFCharacterResponseVo setVFCharacterData(AvaUser avaUser, String characterId, VFCharacterRequestVo vo);

    List<MemoPostsResponseVo> getUserTimelineMemo(Long userTimelineNo);

    void addPaymentMemo(Long payNo, MemoPostRequestVo post);

    List<MemoPostsResponseVo> getPaymentMemo(Long payNo);

    void addUserTimelineMemo(Long payNo, MemoPostRequestVo memoPostRequestVo);
}

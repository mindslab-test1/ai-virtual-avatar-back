package ai.mindslab.ttsmaker.api.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseError;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@ControllerAdvice
public class TTSMakerApiRestExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(value = { TTSMakerApiException.class })
	protected ResponseEntity<Object> handleConflict(TTSMakerApiException ex, WebRequest request) {
		HttpStatus statusCode = ex.getStatusCode();
		MaumAIResponseError error = MaumAIResponseError.builder()
			.requestId(UUID.randomUUID().toString())
			.httpStatus(ex.getStatusCode().value())
			.message(String.format("API 처리 중 오류(%s:%s)가 발생했습니다.", statusCode.value(), statusCode.name()))
			.errorCode(ex.getErrorCode())
			.errorMessage(ex.getErrorMessage()).build();
		
		return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.OK, request);
	}

	@ExceptionHandler(value = { EntityNotFoundException.class })
	protected ResponseEntity<Object> handleEntityNotFoundConflict(EntityNotFoundException ex, WebRequest request) {
		HttpStatus statusCode = HttpStatus.NOT_FOUND;
		MaumAIResponseError error = MaumAIResponseError.builder()
				.requestId(UUID.randomUUID().toString())
				.httpStatus(statusCode.value())
				.message(String.format("Entity 조회 중 오류 (%s,%s)가 발생했습니다.", statusCode.value(), statusCode.name()))
				.errorMessage(ex.getMessage()).build();

		return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

}

package ai.mindslab.ttsmaker.api.code.vo.response;

import ai.mindslab.ttsmaker.api.admin.enums.StatisticsStatus;
import ai.mindslab.ttsmaker.api.admin.vo.data.VFCharacterDataVo;
import ai.mindslab.ttsmaker.api.code.enums.CodeStatus;
import ai.mindslab.ttsmaker.api.code.vo.data.CodeDataVo;
import lombok.*;

import java.util.List;

/**
 * ai.mindslab.ttsmaker.api.admin.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-08  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-08
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CodeResponseVo {

    private String codeStatus;
    private String message;
    private List<CodeDataVo> data;

    public static CodeResponseVo of(CodeStatus codeStatus, String msg) {
        return CodeResponseVo.builder()
                .codeStatus(codeStatus.name())
                .message(msg)
                .build();
    }

    public static CodeResponseVo of(CodeStatus codeStatus, String msg, List<CodeDataVo> data) {
        return CodeResponseVo.builder()
                .codeStatus(codeStatus.name())
                .data(data)
                .message(msg)
                .build();
    }

}

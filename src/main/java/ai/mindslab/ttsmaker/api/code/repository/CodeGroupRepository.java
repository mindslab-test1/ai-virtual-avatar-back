package ai.mindslab.ttsmaker.api.code.repository;

import ai.mindslab.ttsmaker.api.code.domain.CodeGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeGroupRepository extends JpaRepository<CodeGroup, Long> {
}

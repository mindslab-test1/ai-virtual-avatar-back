package ai.mindslab.ttsmaker.api.code.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "CODE_GROUP")
public class CodeGroup implements Serializable {
    
    private static final long serialVersionUID = 8593548644494627872L;
    
    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    //그룹 코드 관리번호
    @Column(name = "GROUP_CODE", columnDefinition = "VARCHAR(20)", unique = true)
    private String groupCode;

    //그룹 코드 명칭
    @Column(name = "GROUP_NM", columnDefinition = "varchar(100)")
    private String groupName;

    //활성화 여부
    @Column(name = "ACTIVE_STATUS")
    private Boolean activeStatus;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}

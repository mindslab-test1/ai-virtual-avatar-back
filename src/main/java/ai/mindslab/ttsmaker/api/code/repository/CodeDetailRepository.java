package ai.mindslab.ttsmaker.api.code.repository;

import ai.mindslab.ttsmaker.api.code.domain.CodeDetail;
import ai.mindslab.ttsmaker.api.code.domain.QCodeDetail;
import ai.mindslab.ttsmaker.api.code.domain.QCodeGroup;
import ai.mindslab.ttsmaker.api.code.vo.data.CodeDataVo;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public interface CodeDetailRepository extends JpaRepository<CodeDetail, Long> {

    QCodeGroup codeGroup = QCodeGroup.codeGroup;
    QCodeDetail codeDetail = QCodeDetail.codeDetail;

    @Query("SELECT cd FROM CodeDetail cd WHERE cd.detailCode = ?1 AND cd.groupCode = ?2")
    CodeDetail findCodeDetailByDetailCodeAndGroupCode(String detailCode, String groupCode);

    /**
     * 그룹코드로 코드목록 조회
     * @param entityManager
     * @param groupCode
     * @return
     */
    default List<CodeDataVo> findCodeDetailByGroupCode(EntityManager entityManager, String groupCode){
        return new JPAQuery<CodeDataVo>(entityManager)
                .select(
                        Projections.constructor(
                                CodeDataVo.class,
                                codeDetail.no,
                                codeDetail.detailCode,
                                codeDetail.detailName,
                                codeDetail.groupCode
                        )
                ).from(codeGroup)
                .innerJoin(codeDetail).on(codeGroup.no.eq(codeDetail.groupCode.castToNum(Long.class)))
                .where(codeGroup.groupCode.eq(groupCode))
                .fetch();
    }

}

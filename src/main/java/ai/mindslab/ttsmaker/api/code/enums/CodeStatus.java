package ai.mindslab.ttsmaker.api.code.enums;

public enum CodeStatus {
    SUCCESS, FAIL, MULTIPLE_ROW, NOT_FOUND
}

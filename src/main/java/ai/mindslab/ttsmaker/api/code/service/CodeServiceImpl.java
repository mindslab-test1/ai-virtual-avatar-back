package ai.mindslab.ttsmaker.api.code.service;

import ai.mindslab.ttsmaker.api.code.enums.CodeStatus;
import ai.mindslab.ttsmaker.api.code.repository.CodeDetailRepository;
import ai.mindslab.ttsmaker.api.code.vo.data.CodeDataVo;
import ai.mindslab.ttsmaker.api.code.vo.response.CodeResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * ai.mindslab.ttsmaker.api.code.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-08  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-08
 */
@Slf4j
@Service
public class CodeServiceImpl implements CodeService {

    @PersistenceContext
    EntityManager entityManager;

    final CodeDetailRepository codeDetailRepository;

    public CodeServiceImpl(CodeDetailRepository codeDetailRepository) {
        this.codeDetailRepository = codeDetailRepository;
    }

    /**
     * 그룹코드로 코드목록 조회
     * @param groupCode
     * @return
     */
    @Override
    public CodeResponseVo getCodeDetailByGroupCode(String groupCode) {
        Optional<List<CodeDataVo>> opt = Optional.ofNullable(codeDetailRepository.findCodeDetailByGroupCode(entityManager, groupCode));
        if(opt.isPresent()){
            return CodeResponseVo.of(CodeStatus.SUCCESS, "조회 성공", opt.get());
        }else{
            return CodeResponseVo.of(CodeStatus.NOT_FOUND, "조회된 코드가 없습니다.");
        }
    }

}

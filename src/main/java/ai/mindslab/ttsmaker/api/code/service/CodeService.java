package ai.mindslab.ttsmaker.api.code.service;

import ai.mindslab.ttsmaker.api.code.vo.response.CodeResponseVo;

/**
 * ai.mindslab.ttsmaker.api.code.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-08  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-08
 */
public interface CodeService {

    /**
     * 그룹코드로 코드목록 조회
     * @param groupCode
     * @return
     */
    CodeResponseVo getCodeDetailByGroupCode(String groupCode);

}

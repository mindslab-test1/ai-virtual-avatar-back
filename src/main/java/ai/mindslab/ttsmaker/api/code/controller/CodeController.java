package ai.mindslab.ttsmaker.api.code.controller;

import ai.mindslab.ttsmaker.api.code.service.CodeService;
import ai.mindslab.ttsmaker.api.code.vo.response.CodeResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/code")
public class CodeController {

    final CodeService codeService;

    public CodeController(CodeService codeService) {
        this.codeService = codeService;
    }

    /**
     * 그룹코드로 코드목록 조회
     * @param groupCode
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/list/{groupCode}")
    public CodeResponseVo getCodeDetailByGroupCode(@PathVariable String groupCode) {
        return codeService.getCodeDetailByGroupCode(groupCode);
    }

}

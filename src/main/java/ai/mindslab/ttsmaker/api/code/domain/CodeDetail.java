package ai.mindslab.ttsmaker.api.code.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "CODE_DETAIL", uniqueConstraints = {@UniqueConstraint(name = "UK_GROUP_CODE_DETAIL_CODE", columnNames = {"GROUP_CODE", "DETAIL_CODE"})})
public class CodeDetail implements Serializable {
    
    private static final long serialVersionUID = 6414573002383329980L;
    
    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    //그룹 코드 관리번호
    @Column(name = "GROUP_CODE", columnDefinition = "VARCHAR(20)")
    private String groupCode;
    
    //상세 코드
    @Column(name = "DETAIL_CODE", columnDefinition = "varchar(10)")
    private String detailCode;

    //상세 코드 명칭
    @Column(name = "DETAIL_NM", columnDefinition = "varchar(100)")
    private String detailName;

    //활성화 여부
    @Column(name = "ACTIVE_STATUS")
    private Boolean activeStatus;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}

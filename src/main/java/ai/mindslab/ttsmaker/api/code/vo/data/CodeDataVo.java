package ai.mindslab.ttsmaker.api.code.vo.data;

import lombok.*;

/**
 * ai.mindslab.ttsmaker.api.code.vo.data
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-03-08  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-03-08
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CodeDataVo {

    long no;
    String detailCode;
    String detailNm;
    String groupCode;

}

package ai.mindslab.ttsmaker.api.provider.maumai.tts.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.amazonaws.services.s3.model.CannedAccessControlList;

import ai.mindslab.ttsmaker.api.provider.aws.s3.properties.AWSS3ConfigProperties;
import ai.mindslab.ttsmaker.api.provider.aws.s3.service.AWSS3Service;
import ai.mindslab.ttsmaker.api.provider.aws.s3.vo.AWSS3ResponseMessage;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.exception.MaumAIApiException;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.TTSApiResponseMessage;
import ai.mindslab.ttsmaker.util.DateUtils;
import ai.mindslab.ttsmaker.util.FfmpegUtils;
import ai.mindslab.ttsmaker.util.PathUtils;
import ai.mindslab.ttsmaker.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TTSApiServiceImpl implements TTSApiService {
    @Autowired
    private TTSApiProperties ttsApiProperties;
    
    @Autowired
    private AWSS3ConfigProperties awsS3ConfigProperties;
    
    @Autowired
    private AWSS3Service awsS3Service;
   
    private static Logger logger = LoggerFactory.getLogger(TTSApiServiceImpl.class.getName());
   
    @Override
    public TTSApiResponseMessage requestTTSStream(String voiceNm, String text, String makeId) {
        String group = DateUtils.asString(LocalDate.now(), "yyyyMMdd");
        String uuid = StringUtils.hasText(makeId) ? makeId : UUID.randomUUID().toString();
        
        log.info("TTSApiService:requestTTSStream() {} >> Start", uuid);
        log.info("TTSApiService:requestTTSStream() {} >> voiceNm : {}", uuid, voiceNm);
        log.info("TTSApiService:requestTTSStream() {} >> text : {}", uuid, text);
        log.info("TTSApiService:requestTTSStream() {} >> makeId = {}", uuid, makeId);
        
        Path finalFilePath = Paths.get(ttsApiProperties.getConvertResultDir(), group, StringUtils.stripAccents(uuid) + ".wav");
        
        log.info("TTSApiService:requestTTSStream() {} >> finalFilePath = {}", uuid, finalFilePath.toAbsolutePath());
        
        // 디렉토리 생성
        PathUtils.createDirectories(finalFilePath.getParent());
        log.info("TTSApiService:requestTTSStream() {} >> PathUtils.createDirectories = {}", uuid, finalFilePath.getParent());
        
        // TTS Stream 호출
        byte[] audioArray = requestStreamTargetByTTSServing(voiceNm, text);
        log.info("TTSApiService:requestTTSStream() {} >> requestStreamTargetByTTSServing() after audioArray size  = {}", uuid, (audioArray != null ? audioArray.length : 0));
        //log.info("TTSApiService:requestTTSStream() {} >> requestStreamTargetByTTSServing() after audioArray  = {}", uuid, audioArray);
        
        if(audioArray.length == 0) {
        	throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0004", String.format("TTS Stream byte array zero!(%s)", uuid));
        }
//        Path originFilePath = Paths.get(ttsApiProperties.getConvertResultDir(), group, StringUtils.stripAccents(uuid) + "_originFilePath.wav");
//        try(FileOutputStream fos = new FileOutputStream(new File(originFilePath.toUri()))) {
//        	fos.write(audioArray);
//            fos.flush();
//            
//        }catch (Exception e) {
//        	throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0005", String.format("WAV File(%s) Create Error : %s", uuid, e.getMessage()));
//        }

        // WAV 생성 
        File outputFile = finalFilePath.toFile();
        try(FileOutputStream fos = new FileOutputStream(outputFile)) {
        	fos.write(audioArray);
            fos.flush();
            
        }catch (Exception e) {
        	throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0005", String.format("WAV File(%s) Create Error : %s", uuid, e.getMessage()));
        }
        
        log.info("TTSApiService:requestTTSStream() {} >> WAV(outputFile) create   = {}", uuid, outputFile.getPath());
        
        float resourceDuration = 0.0f;
        try {
			resourceDuration = getResourceDuration(outputFile);
		} catch (Exception e) {
			throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0006", String.format("WAV File(%s) getResourceDuration() Error : %s", uuid, e.getMessage()));
		}
        log.info("TTSApiService:requestTTSStream() {} >> resourceDuration   = {}", uuid, resourceDuration);
        
        Float speed = 1.0f;
        if(ttsApiProperties.isFfmpegUsed()) {
        	String ffmpegCommand = FfmpegUtils.defaultFFMpegCommand(outputFile.getPath(), speed) + " -y";
        	log.info("TTSApiService:requestTTSStream() {} >> ffmpegCommand  = {}", uuid, ffmpegCommand);
            try {
            	Process processor = Runtime.getRuntime().exec(ffmpegCommand);
            	processor.waitFor();
            	log.info("TTSApiService:requestTTSStream() {} >> ffmpegCommand execute after!!", uuid);
            } catch (Exception e) {
            	throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0007", String.format("ffmpeg Process Error(%s) : %s", uuid, e.getMessage()));
            }
            
        }
        try {
        	log.info("TTSApiService:requestTTSStream() {} >> Audio file before ffmpeg, audioArray.size = ", uuid, audioArray.length);
			audioArray= Files.readAllBytes(finalFilePath);
			log.info("TTSApiService:requestTTSStream() {} >> Audio file after ffmpeg, audioArray.size =  ", uuid, audioArray.length);
		} catch (IOException e) {
			throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0008", String.format("Audio file after ffmpeg Error(%s) : %s", uuid, e.getMessage()));
		}
        
        AWSS3ResponseMessage s3ResponseMessage = null;
//        try {
//        	s3ResponseMessage = uploadS3Bucket(group, uuid, finalFilePath);
//        	log.info("TTSApiService:requestTTSStream() {}  >> s3ResponseMessage(status: {}).objectUrl = {}, after", uuid, s3ResponseMessage.getStatus(), s3ResponseMessage.getObjectUrl());
//        }catch(Exception e) {
//        	throw new MaumAIApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRA0009", String.format("AWS S3 Upload Process Error(%s) : %s", uuid, e.getMessage()));
//        }
        
        s3ResponseMessage = Optional.ofNullable(s3ResponseMessage).orElse(new AWSS3ResponseMessage());
        log.info("TTSApiService:requestTTSStream() {}  >> End", uuid);
        
		return TTSApiResponseMessage.builder()
				.voiceNm(voiceNm)
				.text(text)
				.speed(speed)
				.group(group)
				.uuid(uuid)
				.s3Region(s3ResponseMessage.getRegion())
				.s3BucketNm(s3ResponseMessage.getBucketNm())
				.s3ObjectNm(s3ResponseMessage.getObjectNm())
				.s3ObjectUrl(s3ResponseMessage.getObjectUrl())
				.expiredDate(LocalDate.now().plusDays(7l))
				.outputFilePath(finalFilePath)
				.resource(audioArray)
				.resourceDuration(resourceDuration)
				.build();
    }
    
    private float getResourceDuration(File file) throws IOException, UnsupportedAudioFileException {
    	float resourceDuration = 0.0f;
    	try(AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file)) {
    		AudioFormat format = audioInputStream.getFormat();
        	long frames = audioInputStream.getFrameLength();
        	resourceDuration = (frames+0.0f) / format.getFrameRate();  
    	}
    	
    	return resourceDuration;
    }

	private AWSS3ResponseMessage uploadS3Bucket(String group, String uuid, Path uploadFilePath) {
		String region = awsS3ConfigProperties.getRegion();
		String bucketNm = awsS3ConfigProperties.getBucketNm();
		String objectNm = String.format("temp/%s/%s.wav", group, uuid); 
		
		return awsS3Service.putObject(region, bucketNm, objectNm, uploadFilePath, CannedAccessControlList.PublicRead);
	}

	private byte[] requestStreamTargetByTTSServing(String voiceName, String text) {
		
		WebClient webClient = WebClient.builder()
	        		.defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
	        		.baseUrl(ttsApiProperties.apiHost)
	        		.defaultHeader(HttpHeaders.CACHE_CONTROL, "no-cache")
	        		.build();
		 
		 MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
		 bodyBuilder.part("voiceName", voiceName, MediaType.TEXT_PLAIN);
		 bodyBuilder.part("text", text, MediaType.TEXT_PLAIN);
		 bodyBuilder.part("apiId", ttsApiProperties.apiId, MediaType.TEXT_PLAIN);
		 bodyBuilder.part("apiKey", ttsApiProperties.apiKey, MediaType.TEXT_PLAIN);
		 
		 logger.info("Request [{}]: {}", ttsApiProperties.getCallerHost(), bodyBuilder.toString());
		 
		 return webClient.post().uri(ttsApiProperties.getApiStreamUrl())
		 	.contentType(MediaType.MULTIPART_FORM_DATA)
		 	.body(BodyInserters.fromMultipartData(bodyBuilder.build()))
		 	.exchange()
		 	.flatMap(response -> {
		 		if(response.statusCode().isError()) {
		 			throw makeErrorReponse(response);
		 		}
		 		return response.bodyToMono(ByteArrayResource.class);
		 	})
		 	.map(ByteArrayResource::getByteArray)
		 	.onErrorMap(e-> {
		 		logger.error(e.getMessage());
		 		return e;
		 	}).block();
	}
	
	private MaumAIApiException makeErrorReponse(ClientResponse response) {
		MaumAIApiException exception = null;
        HttpStatus statusCode = response.statusCode();
        if (statusCode != null) {
            switch (statusCode) {
                case FORBIDDEN:
                    logger.error("{}: ERRA0001 - MaumAI API provider 인증 오류 ", response.statusCode());
                    exception = new MaumAIApiException(response.statusCode(), "ERRA0001", "MaumAI API provider 인증 오류");
                    break;
                case BAD_GATEWAY:
                    logger.error("{}: ERRA0002 - MaumAI API provider와의 통신 오류", response.statusCode());
                    exception = new MaumAIApiException(response.statusCode(), "ERRA0002", "MaumAI API provider와의 통신 오류");
                    break;
                case BAD_REQUEST:
                    logger.error("{}: ERRA0003 - The request parameter value is invalid", response.statusCode());
                    exception = new MaumAIApiException(response.statusCode(), "ERRA0003", "The request parameter value is invalid.(MaumAI API)");
                    break;
                default:
                    if (statusCode.is4xxClientError() || statusCode.is5xxServerError()) {
                        logger.error("{}: ERRA0000 - MaumAIAPI provider 통신 오류", response.statusCode());
                        exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
                    }
                    break;
            }
        } else {
            exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
        }
        return exception;
	}
}

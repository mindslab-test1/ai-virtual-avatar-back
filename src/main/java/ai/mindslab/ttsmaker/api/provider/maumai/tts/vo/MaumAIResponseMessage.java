package ai.mindslab.ttsmaker.api.provider.maumai.tts.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class MaumAIResponseMessage {
	private String message;
	private int status;
}

package ai.mindslab.ttsmaker.api.provider.maumai.tts.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder(toBuilder = true)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MaumAIRequestParam {
	String apiId;
	String apiKey;
	String voiceName;
	String text;
	float speed;
}

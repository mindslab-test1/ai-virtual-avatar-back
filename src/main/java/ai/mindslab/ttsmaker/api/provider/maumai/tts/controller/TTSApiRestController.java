package ai.mindslab.ttsmaker.api.provider.maumai.tts.controller;

import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiServiceImpl;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.TTSApiResponseMessage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/provider/tts")
public class TTSApiRestController {

	@Autowired
	private TTSApiServiceImpl maumAiApiService;

	@ResponseBody
	@PostMapping(value = "/requestMakeFile/{voiceName}")
	public ResponseEntity<Resource> requestMakeFile(@PathVariable String voiceName, @RequestParam String text, final HttpServletRequest request, final HttpServletResponse response) throws IOException, InterruptedException {
		log.info("[Make TTS File] : {}, {}", voiceName, text);
		
		TTSApiResponseMessage ttsApiResponseMessage = maumAiApiService.requestTTSStream(voiceName, text, null);
		String contentType = Files.probeContentType(ttsApiResponseMessage.getOutputFilePath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, contentType);
		
		Resource resource = new InputStreamResource(Files.newInputStream(ttsApiResponseMessage.getOutputFilePath()));
		return new ResponseEntity<>(resource, headers, HttpStatus.OK);
	}
}

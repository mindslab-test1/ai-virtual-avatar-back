package ai.mindslab.ttsmaker.api.provider.aws.s3.service;

import java.nio.file.Path;

import com.amazonaws.services.s3.model.CannedAccessControlList;

import ai.mindslab.ttsmaker.api.provider.aws.s3.vo.AWSS3ResponseMessage;

public interface AWSS3Service {
	public AWSS3ResponseMessage putObject(String region, String bucketNm, String objectNm, Path uploadFilePath, CannedAccessControlList cannedAcl);
}

package ai.mindslab.ttsmaker.api.provider.aws.s3.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AWSS3ResponseMessage {
	int status;
	String message;
	String region;
	String bucketNm;
	String objectNm;
	String objectUrl;
	byte[] resources;
}

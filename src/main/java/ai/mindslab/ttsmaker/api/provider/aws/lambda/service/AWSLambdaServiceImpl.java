package ai.mindslab.ttsmaker.api.provider.aws.lambda.service;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.ServiceException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.ttsmaker.api.provider.aws.credentials.properties.AWSIAMConfigProperties;
import ai.mindslab.ttsmaker.api.provider.aws.lambda.vo.AWSLambdaResponseMessage;

@Service
public class AWSLambdaServiceImpl implements AWSLambdaService {

	@Autowired
	private AWSIAMConfigProperties awsIamConfigProperties;

	@Autowired
	private ObjectMapper objectMapper;

	public AWSLambdaResponseMessage invokeRequestAsync(String region, String functionName,  Map<String, ?> payloadMap) {
		AWSLambdaResponseMessage responseMessage = null;
		try {
			 String payload = objectMapper.writeValueAsString(payloadMap);
			 responseMessage = invokeRequestAsync(region, functionName,  payload);
		} catch (JsonProcessingException e) {
	        	responseMessage = AWSLambdaResponseMessage.builder().status(500).message("JsonProcessingException").build();
		}
		 return responseMessage;
	}

	@Override
	public AWSLambdaResponseMessage invokeRequestAsync(String region, String functionName,  String payload) {
		AWSLambdaResponseMessage responseMessage = null;
		
		try {
			BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsIamConfigProperties.getAccessKey(), awsIamConfigProperties.getSecretKey());
			AWSLambda awsLambda = AWSLambdaAsyncClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
					.withRegion(region)
					.build();

			InvokeRequest invokeRequest = new InvokeRequest()
					.withFunctionName(functionName)
					.withInvocationType(InvocationType.Event)
					.withPayload(payload);

			InvokeResult invokeResult = awsLambda.invoke(invokeRequest);
			String AWS_REQUEST_ID = invokeResult.getSdkResponseMetadata().getRequestId();
			String responseString = new String(invokeResult.getPayload().array(), StandardCharsets.UTF_8);
			responseMessage = new AWSLambdaResponseMessage(AWS_REQUEST_ID, invokeResult.getStatusCode(), "요청 완료", responseString);
		} catch (ServiceException e) {
			responseMessage = AWSLambdaResponseMessage.builder().status(500).message("ServiceException").build();
		}
		return responseMessage;
	}

}

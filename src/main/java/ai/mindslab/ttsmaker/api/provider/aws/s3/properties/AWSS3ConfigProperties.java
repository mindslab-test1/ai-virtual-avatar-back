package ai.mindslab.ttsmaker.api.provider.aws.s3.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "ava.aws.s3-config")
public class AWSS3ConfigProperties {
	String region;
	String bucketNm;
}
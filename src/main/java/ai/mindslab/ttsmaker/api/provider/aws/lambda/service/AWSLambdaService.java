package ai.mindslab.ttsmaker.api.provider.aws.lambda.service;

import java.util.Map;

import ai.mindslab.ttsmaker.api.provider.aws.lambda.vo.AWSLambdaResponseMessage;

public interface AWSLambdaService {
	public AWSLambdaResponseMessage invokeRequestAsync(String region, String functionName, Map<String, ?> payloadMap);
	public AWSLambdaResponseMessage invokeRequestAsync(String region, String functionName, String payload);
}

package ai.mindslab.ttsmaker.api.provider.maumai.tts.vo;

import java.io.Serializable;
import java.nio.file.Path;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TTSApiResponseMessage implements Serializable {
	private static final long serialVersionUID = 6884428951274103895L;
	
	String voiceNm; 
	String text;
	Float speed;
	String group; 
	String uuid;
	String s3Region;
	String s3BucketNm;
	String s3ObjectNm;
	String s3ObjectUrl;
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	LocalDate expiredDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	Path outputFilePath;
	
	byte[] resource;
	float resourceDuration;
}

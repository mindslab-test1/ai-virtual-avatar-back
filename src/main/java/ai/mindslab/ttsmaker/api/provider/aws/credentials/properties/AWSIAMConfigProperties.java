package ai.mindslab.ttsmaker.api.provider.aws.credentials.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ConfigurationProperties(prefix = "ava.aws.credentials-config")
public class AWSIAMConfigProperties {
	private String accessKey; 
	private String secretKey;
}

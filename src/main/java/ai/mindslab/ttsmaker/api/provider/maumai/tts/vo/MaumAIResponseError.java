package ai.mindslab.ttsmaker.api.provider.maumai.tts.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MaumAIResponseError {
	private String requestId;
	private int httpStatus;
	private String message;
	private String errorCode;
	private String errorMessage;
}

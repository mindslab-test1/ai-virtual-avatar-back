package ai.mindslab.ttsmaker.api.provider.aws.s3.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;

import ai.mindslab.ttsmaker.api.provider.aws.credentials.properties.AWSIAMConfigProperties;
import ai.mindslab.ttsmaker.api.provider.aws.s3.vo.AWSS3ResponseMessage;

@Service
public class AWSS3ServiceImpl implements AWSS3Service {
	
	@Autowired
	private AWSIAMConfigProperties awsIamConfigProperties;
	
	public AWSS3ResponseMessage putObject(String region, String bucketNm, String objectNm, Path uploadFilePath, CannedAccessControlList cannedAcl) {
		AWSS3ResponseMessage responseMessage = null;
		try {
			File uploadFile = uploadFilePath.toFile();
			ObjectMetadata metadata = new ObjectMetadata();
		    metadata.setContentType(Files.probeContentType(uploadFilePath));
		    metadata.setContentLength(uploadFile.length());
		    
		    BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsIamConfigProperties.getAccessKey(), awsIamConfigProperties.getSecretKey());
		    
	    	AmazonS3 s3Builder = AmazonS3ClientBuilder.standard()
					.withRegion(region)
					.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
					.build();
	    	
	    	PutObjectRequest putObjectParam = new PutObjectRequest(bucketNm, objectNm, Files.newInputStream(uploadFilePath), metadata);
	    	putObjectParam.withCannedAcl(cannedAcl);
	    	
	    	s3Builder.putObject(putObjectParam);
	    	
	    	String objectUrl = String.format("https://%s.s3-%s.amazonaws.com/%s", bucketNm, region, objectNm);
	    	
	    	responseMessage =  AWSS3ResponseMessage.builder()
	    			.status(200)
	    			.message("Upload OK")
	    			.region(region)
	    			.bucketNm(bucketNm)
	    			.objectNm(objectNm)
	    			.objectUrl(objectUrl)
	    			.build();
		} catch (IOException e) {
			responseMessage = AWSS3ResponseMessage.builder().status(500).message(e.getMessage()).build();
	    }
		return responseMessage;
	}
	
	public AWSS3ResponseMessage getObject(String region, String bucketNm, String objectNm) {
		AWSS3ResponseMessage responseMessage = null;
		try {
		    BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsIamConfigProperties.getAccessKey(), awsIamConfigProperties.getSecretKey());
		    
	    	AmazonS3 s3Builder = AmazonS3ClientBuilder.standard()
					.withRegion(region)
					.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
					.build();
	    	
	    	byte[] resources = null;
	    	S3Object obj = s3Builder.getObject(new GetObjectRequest(bucketNm, objectNm));
	    	try(S3ObjectInputStream is = obj.getObjectContent()) {
	    		resources = IOUtils.toByteArray(is);
	    	}catch (Exception e) {
				throw e;
		    }
	    	
	    	if(resources == null || resources.length == 0) {
	    		throw new Exception("resources is empty!");
	    	}
	    	
	    	responseMessage =  AWSS3ResponseMessage.builder()
	    			.status(200)
	    			.message("Upload OK")
	    			.region(region)
	    			.bucketNm(bucketNm)
	    			.objectNm(objectNm)
	    			.resources(resources)
	    			.build();
		} catch (Exception e) {
			responseMessage = AWSS3ResponseMessage.builder().status(500).message(e.getMessage()).build();
	    }
		return responseMessage;
	}
    
    	
    
}

package ai.mindslab.ttsmaker.api.provider.maumai.tts.service;

import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.TTSApiResponseMessage;

public interface TTSApiService {
	public TTSApiResponseMessage requestTTSStream(String voiceName, String text, String makeId);
}

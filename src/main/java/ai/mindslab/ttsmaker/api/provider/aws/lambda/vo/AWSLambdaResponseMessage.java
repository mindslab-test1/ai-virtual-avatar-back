package ai.mindslab.ttsmaker.api.provider.aws.lambda.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AWSLambdaResponseMessage {
	private String awsRequestId;
	private int status;
	private String message;
	private String payload;
}

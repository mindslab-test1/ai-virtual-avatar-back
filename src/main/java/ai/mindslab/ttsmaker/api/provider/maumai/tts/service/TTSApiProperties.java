package ai.mindslab.ttsmaker.api.provider.maumai.tts.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "ava.tts")
@Getter
@Setter
public class TTSApiProperties {
	String callerHost;
	String apiHost;
	String apiStreamUrl;
	String apiId;
	String apiKey;
	String convertResultDir;
	boolean ffmpegUsed = true;
	
}

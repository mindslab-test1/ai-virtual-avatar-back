package ai.mindslab.ttsmaker.api.provider.aws.lambda.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.provider.aws.lambda.service.AWSLambdaService;
import ai.mindslab.ttsmaker.api.provider.aws.lambda.vo.AWSLambdaResponseMessage;

@RestController
@RequestMapping("/provider/aws/lambda")
public class LambdaRestController {

	@Autowired
	private AWSLambdaService lambdaService;

	@ResponseBody
	@PostMapping(path = "/invokeRequestAsync/{region}/{functionName}")
	public AWSLambdaResponseMessage requestMakeFile(@PathVariable String region, @PathVariable String functionName, @RequestBody String payload) throws IOException, InterruptedException {
		return lambdaService.invokeRequestAsync(region, functionName,  payload);
	}
}



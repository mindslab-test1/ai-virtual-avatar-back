package ai.mindslab.ttsmaker.api.provider.aws.lambda.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "ava.aws.lambda-config")
public class AWSLambdaConfigProperties {
	String region;
	String functionNm;
}

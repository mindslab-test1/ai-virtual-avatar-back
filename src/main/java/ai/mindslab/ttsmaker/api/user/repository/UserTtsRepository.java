package ai.mindslab.ttsmaker.api.user.repository;

import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTtsRepository extends JpaRepository<UserTts, Long>, JpaSpecificationExecutor<UserTts> {



}

package ai.mindslab.ttsmaker.api.user.controller;

import ai.mindslab.ttsmaker.api.pay.vo.BillingFormVo;
import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.service.UserInquiryService;
import ai.mindslab.ttsmaker.api.user.vo.request.InquiryRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserInquiryResponseVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/inquiry")
public class UserInquiryController {
    final UserInquiryService userInquiryService;

    public UserInquiryController(UserInquiryService userInquiryService) {
        this.userInquiryService = userInquiryService;
    }


    @GetMapping("/list")
    public ResponseEntity<List<UserInquiryResponseVo>> getUserInquiryList(@RequestParam Long userNo) throws Exception {
        log.info("[User Inquiry] : {}", userNo);
        return userInquiryService.getUserInquiriesListByUserNo(userNo);
    }

    @GetMapping("/detail")
    public ResponseEntity<UserInquiryResponseVo> getUserInquiryDetail(@RequestParam Long inquiryNo) throws Exception {
        log.info("[User Inquiry] : {}", inquiryNo);
        return userInquiryService.getUserInquiryByInquiryNo(inquiryNo);
    }
    
    @ResponseBody
    @PostMapping("/add")
    public String addUserInquiry(@AvaUserParam AvaUser avaUser, @RequestBody InquiryRequestVo requestVo) throws Exception{
        log.info("[New User Inquiry] : UserNo = {}", avaUser.getUserNo());
        userInquiryService.insertInquiry(requestVo, avaUser.getUserNo());
        return "Success";
    }
}

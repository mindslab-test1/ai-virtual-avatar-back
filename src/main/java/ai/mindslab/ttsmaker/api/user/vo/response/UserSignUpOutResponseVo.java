package ai.mindslab.ttsmaker.api.user.vo.response;

import ai.mindslab.ttsmaker.api.user.enums.AuthProcessStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserSignUpOutResponseVo {
	private String processStatus;
	private String processMessage;
	
	public static UserSignUpOutResponseVo of(AuthProcessStatus statusEnum, String processMessage) {
		return UserSignUpOutResponseVo.builder()
        		.processStatus(statusEnum.name())
        		.processMessage(processMessage)
        		.build();
	}
}

package ai.mindslab.ttsmaker.api.user.service;

import java.util.List;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.vo.response.UserPlanListResponseVo;

public interface UserPlanService {
	public List<UserPlan> getUserPlanListFindByUserNo(Long userNo);

    List<UserPlanListResponseVo> getUserPlanListResponseVoFindByUserNo(Long userNo);

	UserPlan deactivateUserPlan(UserPlan userPlan);

	public UserPlan setRegistrationBenefitsPlan(UserM userM);
}

package ai.mindslab.ttsmaker.api.user.repository;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.user.domain.UserRole;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface UserMRoleRepository extends JpaRepository<UserRole, Long> {
    Optional<UserRole> findByUser(UserM userM);

    @Query(value = "insert into UserRole (" +
            "USER_NO, ROLE_NM, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) " +
            "values (?1, ?2, ?3, ?1, null, null)", nativeQuery = true)
    void insertUserRole(Long userNo, UserRoleEnum userRoleEnum, LocalDateTime createDate);
    
    @Query(value = "SELECT ur FROM UserRole ur WHERE ur.user.userNo = ?1 AND ur.roleNm = ?2")
    UserRole getUserRoleByUserNoAndRoleNm(Long userNo, UserRoleEnum userRoleEnum);

    void deleteUserRoleByRoleNo(Long roleNo);
}

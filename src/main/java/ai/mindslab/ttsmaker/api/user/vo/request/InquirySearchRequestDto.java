package ai.mindslab.ttsmaker.api.user.vo.request;

import lombok.*;

@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class InquirySearchRequestDto {
    private String inquiryNo;
    private String userNm;
    private String userId;
    private String inquiryType;
    private String inquiryStatus;
    private String answeredEmail;
}

package ai.mindslab.ttsmaker.api.user.vo.response;

import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import ai.mindslab.ttsmaker.api.user.domain.UserTtsProf;
import ai.mindslab.ttsmaker.api.user.domain.UserTtsProfApi;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * user tts vo
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-08  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-08
 */
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTtsVo {

    private long no;
    private long userNo;
    private String userId;
    private String scriptText;
    private boolean incProf;
    private boolean incProfApi;
    private List<UserTtsProfVo> userTtsProfs;
    private List<UserTtsProfApiVo> userTtsProfApis;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    public static UserTtsVo of(UserTts userTts) {
        UserTtsVo vo = UserTtsVo.builder()
                .no(userTts.getNo())
                .scriptText(userTts.getScriptText())
                .incProf(userTts.getIncProf() != null ? userTts.getIncProf() : false)
                .incProfApi(userTts.getIncProfApi() != null ? userTts.getIncProfApi() : false)
                .userTtsProfs(userTts.getIncProf() != null && userTts.getIncProf() ? userTts.getUserTtsProfs().stream().map(UserTtsProfVo::of).collect(Collectors.toList()) : null)
                .userTtsProfApis(userTts.getIncProfApi() != null && userTts.getIncProfApi() ? userTts.getUserTtsProfApis().stream().map(UserTtsProfApiVo::of).collect(Collectors.toList()) : null)
                .createdAt(userTts.getCreatedAt())
                .build();
        if(userTts.getUser() != null){
            vo.setUserNo(userTts.getUser().getUserNo());
            vo.setUserId(userTts.getUser().getUserId());
        }
        return vo;
    }

    @Builder(toBuilder = true)
    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    static class UserTtsProfVo {
        private long profNo;
        private String profText;
        private int incCount;

        public static UserTtsProfVo of(UserTtsProf userTtsProf){
            return UserTtsProfVo.builder()
                    .profNo(userTtsProf.getProfNo())
                    .profText(userTtsProf.getProfText())
                    .incCount(userTtsProf.getIncCnt())
                    .build();
        }

    }

    @Builder(toBuilder = true)
    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    static class UserTtsProfApiVo {
        private long profApiNo;
        private String category;
        private String label;
        private double reliability;

        public static UserTtsProfApiVo of(UserTtsProfApi userTtsProfApi){
            return UserTtsProfApiVo.builder()
                    .profApiNo(userTtsProfApi.getProfApiNo())
                    .category(userTtsProfApi.getCategory())
                    .label(userTtsProfApi.getLabel())
                    .reliability(userTtsProfApi.getReliability())
                    .build();
        }

    }

}

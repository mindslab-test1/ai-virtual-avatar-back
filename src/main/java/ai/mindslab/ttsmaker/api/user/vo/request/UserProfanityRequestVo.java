package ai.mindslab.ttsmaker.api.user.vo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProfanityRequestVo {
	private String userId;
	private String scriptText;
	private boolean incProf;

	public UserProfanityRequestVo(String userId) {
		this.userId = userId;
	}

}

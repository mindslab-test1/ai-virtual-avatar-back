package ai.mindslab.ttsmaker.api.user.controller;

import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.user.service.UserService;
import ai.mindslab.ttsmaker.api.user.vo.request.UserAuthRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.UserProfanityRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.UserSignUpRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserAuthTokenResponse;
import ai.mindslab.ttsmaker.api.user.vo.response.UserProfanityResponseVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserSignUpOutResponseVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserTtsVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserRestController {
	
	@Autowired
	private UserService userService;
	
	@ResponseBody
    @GetMapping(path = "/me")
	public AvaUser me(@AvaUserParam AvaUser avaUser) {
		return avaUser;
	}
		
	@ResponseBody
    @PostMapping(path = "/sign_out")
	public UserSignUpOutResponseVo logout(@RequestBody UserAuthRequestVo authRequestVo) {
		return userService.userSignOut(authRequestVo);
	}
	
	@ResponseBody
	@PostMapping(path = "/sign_up")
	public UserSignUpOutResponseVo userSignUp(@RequestBody UserSignUpRequestVo userSignUpRequestVo) throws IOException {
		log.info("[UserSignUpRequestVo] : {}", userSignUpRequestVo);
		return userService.userSignUp(userSignUpRequestVo);
	}
	
	@ResponseBody
	@PostMapping(path = "/auth/refresh_token")
	public UserAuthTokenResponse generatorRefreshToken(@RequestBody UserAuthRequestVo authRequestVo) {
		return userService.generatorRefreshToken(authRequestVo.getRefresh_token());
	}

	@ResponseBody
	@GetMapping(path = "/profanity")
	public UserProfanityResponseVo checkProfanityAgree(@AvaUserParam AvaUser avaUser) {
		return userService.getUseProfanityByUserId(new UserProfanityRequestVo(avaUser.getUserId()));
	}

	@ResponseBody
	@PostMapping(path = "/profanity")
	public UserProfanityResponseVo setProfanityAgree(@AvaUserParam AvaUser avaUser) {
		return userService.saveUseProfanity(new UserProfanityRequestVo(avaUser.getUserId()));
	}

	@GetMapping(path = "/tts")
	@ResponseBody
	public List<UserTtsVo> getUserTtsList(final UserSearchRequestDto searchRequestDto) {
		return userService.getUserTtsList(searchRequestDto);
	}
	
}

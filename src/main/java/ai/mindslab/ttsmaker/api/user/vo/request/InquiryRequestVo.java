package ai.mindslab.ttsmaker.api.user.vo.request;


import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InquiryRequestVo {
    private String inquiryType;
    private String inquiryTitle;
    private String inquiryQuestion;
    private String inquiryAnswer;
    private Long inquiryNo;

//    public static InquiryRequestVo of(UserInquiry entity) {
//
//        return InquiryRequestVo.builder()
//                .inquiryType(entity.getInquiryType())
//                .inquiryTitle(entity.getInquiryTitle())
//                .inquiryQuestion(entity.getInquiryQuestion())
//                .inquiryAnswer(entity.getInquiryAnswer())
//                .build();
//    }
}

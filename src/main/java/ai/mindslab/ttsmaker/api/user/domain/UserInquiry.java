package ai.mindslab.ttsmaker.api.user.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "USER_INQUIRY")
public class UserInquiry {

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long inquiryNo;

    //문의 타입
    @Column(name = "INQUIRY_TYPE", columnDefinition = "varchar(10)")
    private String inquiryType;

    //문의 제목
    @Column(name = "INQUIRY_TITLE", columnDefinition = "varchar(100)")
    private String inquiryTitle;
    //문의 내용
    @Column(name = "INQUIRY_QUESTION", columnDefinition = "text(2000)")
    private String inquiryQuestion;

    //답변 내용
    @Column(name = "INQUIRY_ANSWER", columnDefinition = "text(2000)")
    private String inquiryAnswer;

    //문의 상태
    @Column(name = "INQUIRY_STATUS")
    private Boolean inquiryStatus = false;

    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //답변 일시
    @Column(name = "ANSWERED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime answeredAt;

    //답변자
    @Column(name = "ANSWERED_EMAIL", columnDefinition = "varchar(100)")
    private String answeredEmail;

    // User No
    @ManyToOne
    @JoinColumn(name = "USER_NO", foreignKey = @ForeignKey(name="FK_USER_INQUIRY__USER_M"))
    private UserM user;


}

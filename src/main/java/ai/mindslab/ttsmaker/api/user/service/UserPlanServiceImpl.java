package ai.mindslab.ttsmaker.api.user.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.plan.service.PlanService;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.repository.UserPlanRepository;
import ai.mindslab.ttsmaker.api.user.vo.response.UserPlanListResponseVo;

@Service
public class UserPlanServiceImpl implements UserPlanService {
	
	@Autowired
	private UserPlanRepository userPlanRepository; 
	
	@Autowired
	private PlanService planService;

	@Override
	public List<UserPlan> getUserPlanListFindByUserNo(Long userNo) {
		return userPlanRepository.getActiveUserPlanListFindByUserNo(userNo);
	}

	@Override
	public List<UserPlanListResponseVo> getUserPlanListResponseVoFindByUserNo(Long userNo) {
		List<UserPlan> activeUserPlans = userPlanRepository.getActiveUserPlanListFindByUserNo(userNo);
		return activeUserPlans.stream().map(UserPlanListResponseVo::of).collect(Collectors.toList());
	}


	@Override
	public UserPlan deactivateUserPlan(UserPlan userPlan) {
		userPlan.setActive(false);
		userPlanRepository.save(userPlan);

		Plan plan = planService.getPlan(5l);
		LocalDateTime nowLDT = LocalDateTime.now();

		UserPlan freeTierPlan = new UserPlan();
		freeTierPlan.setUser(userPlan.getUser());
		freeTierPlan.setPlan(plan);
		freeTierPlan.setCharUseCnt(0);
		freeTierPlan.setCharMaxLimit(plan.getMaxLimit());
		freeTierPlan.setActive(true);
		freeTierPlan.setMemo("프리티어 햬택");
		freeTierPlan.setPlanStartAt(nowLDT);
		freeTierPlan.setPlanEndAt(nowLDT.plusMonths(1l));
		return userPlanRepository.save(freeTierPlan);
	}

	@Override
	public UserPlan setRegistrationBenefitsPlan(UserM userM) {
		LocalDateTime nowLDT = LocalDateTime.now();
		Plan plan = planService.getPlan(5l);
		UserPlan userPlan = new UserPlan();
		userPlan.setUser(userM);
		userPlan.setPlan(plan);
		userPlan.setCharUseCnt(0);
		userPlan.setCharMaxLimit(plan.getMaxLimit());
		userPlan.setActive(true);
		userPlan.setMemo("첫 가입 혜택");
		userPlan.setPlanStartAt(nowLDT);
		userPlan.setPlanEndAt(nowLDT.plusMonths(1l));
		return userPlanRepository.save(userPlan);
	}
}

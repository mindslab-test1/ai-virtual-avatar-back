package ai.mindslab.ttsmaker.api.user.service;

import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.vo.request.InquiryRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserInquiryResponseVo;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserInquiryService {

    ResponseEntity<List<UserInquiryResponseVo>> getUserInquiriesListByUserNo(Long userNo);

    ResponseEntity<UserInquiryResponseVo> getUserInquiryByInquiryNo(Long inquiryNo);

    void insertInquiry(InquiryRequestVo inquiryRequestVo, Long userNo);
    
    void replyInquiry(InquiryRequestVo inquiryRequestVo, String adminEmail);
}

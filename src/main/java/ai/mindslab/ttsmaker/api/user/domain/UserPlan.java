package ai.mindslab.ttsmaker.api.user.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;

import ai.mindslab.ttsmaker.api.pay.domain.PayM;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import lombok.Getter;
import lombok.Setter;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "USER_PLAN")
public class UserPlan implements Serializable {
	
	private static final long serialVersionUID = -1064932611568571717L;

    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;
    
    // User No
    @ManyToOne
    @JoinColumn(name = "USER_NO", foreignKey = @ForeignKey(name="FK_USER_PLAN__USER_M"))
    private UserM user;
    
    // plan
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PLAN_NO", foreignKey = @ForeignKey(name="FK_USER_PLAN__PLAN"))
    private Plan plan;
    
    // 결제 관리번호
    @OneToOne
    @JoinColumn(name = "PAY_NO", nullable = true, foreignKey = @ForeignKey(name = "FK_USER_PLAN__PAY_M"), referencedColumnName = "NO")
    private PayM payM;

    // 글자 사용수
    @Column(name = "CHAR_USE_CNT")
    private Integer charUseCnt = 0;
    
    // 글자 최대 허용수
    @Column(name = "CHAR_MAX_LIMIT", columnDefinition = "int default 0")
    private Integer charMaxLimit;
    
    // 활성화 여부
    @Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;
    
    //메모
    @Column(name = "MEMO", columnDefinition = "varchar(200)")
    private String memo;
    
    // 계약 시작일시
    @Column(name = "PLAN_START_AT")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planStartAt;
    
    // 계약 종료일시
    @Column(name = "PLAN_END_AT")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planEndAt;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //등록자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CREATED_BY")
    private Long createdBy;

    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
    
    //수정자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Column(name = "PREV_USER_PLAN_NO")
    private Long prevUserPlanNo;

    @Column(name = "CHAR_TRANSFERRED", columnDefinition = "integer default 0")
    private Integer charTransferred;


    public void setCharTransferred(UserPlan prevPlan){
        this.charTransferred = prevPlan.getCharMaxLimit() - prevPlan.getCharUseCnt();
        this.prevUserPlanNo = prevPlan.getNo();
    }
}

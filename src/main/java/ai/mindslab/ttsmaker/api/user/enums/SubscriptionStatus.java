package ai.mindslab.ttsmaker.api.user.enums;

public enum SubscriptionStatus {
    /*
    START: 구독 성공
    EXTENDED: 구독 연장
    CANCEL: 구독 취소
    FAIL: 구독 실패
    END: 구독 만료
    */
    START, EXTENDED, CANCEL, FAIL, END
}

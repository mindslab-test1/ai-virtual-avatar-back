package ai.mindslab.ttsmaker.api.user.vo.request;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserFilterRequestVo {

    private String userNm;
    private String userId;
    private String company;
    private String userPlan;
    private String userCellPhone;
}

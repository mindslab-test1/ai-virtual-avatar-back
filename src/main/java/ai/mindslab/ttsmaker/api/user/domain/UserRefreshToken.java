package ai.mindslab.ttsmaker.api.user.domain;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_REFRESH_TOKEN")
public class UserRefreshToken implements Serializable {
	
	private static final long serialVersionUID = -8265232739044448970L;

	//관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    //user no
    @Column(name = "USER_NO")
    private Long userNo;
    
    //user ID
    @Column(name = "USER_ID", columnDefinition = "varchar(100)")
    private String userId;

    // REFRESH_TOKEN
    @Column(name = "REFRESH_TOKEN", columnDefinition = "varchar(500)")
    private String refreshToken;
    
    @Column(name = "EXPIRE_DATE")
    private LocalDateTime refreshTokenExpireDate;
    
    @Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}

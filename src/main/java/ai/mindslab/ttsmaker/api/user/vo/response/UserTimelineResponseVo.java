package ai.mindslab.ttsmaker.api.user.vo.response;

import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserTimeline;
import ai.mindslab.ttsmaker.api.user.enums.AuthProcessStatus;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTimelineResponseVo implements Serializable {
    private Long userPlanNo;

    // 플랜이름
    private String planNm;

    // 이전 플랜 남은 글자수 (지난달 총 글자수 - 지난달 사용 글자수)
    private Integer prevCharLeftCount;

    // 이전 플랜 사용 글자수 [(현재 플랜 사용수 < 이전 플랜 남은 글자수) ? 현재 플랜 사용수 : 이전 플랜 남은 글자수]
    private Integer prevCharUsedCount;

    // 현재 플랜 남은 글자수 (지난달 총 글자수 - 지난달 사용 글자수)
    private Integer currCharLeftCount;

    // 현재 플랜 사용 글자수 [(현재 플랜 사용수 < 이전 플랜 남은 글자수) ? 현재 플랜 사용수 : 이전 플랜 남은 글자수]
    private Integer currCharUsedCount;

    // 총 글자수 (현재 플랜 글자수 한계 + 이전플랜 남은 글자수)
    private Integer totPlanMaxLimit;

//    // 사용 글자수 (이전플랜 사용 글자수 + 현재 사용 글자수)
//    private Integer charUsedCount;

    // 구독 시작일
    private LocalDateTime planStartAt;

    // 구독 종료 예정일
    private LocalDateTime planEndAt;

    // 실제 구독 종료일
    private LocalDateTime planEndedAt;

    // 비고
    private String memo;

    private Boolean active;

    private Boolean prevPlanActive;




    // 현재 플랜만 있을 경우
    public static UserTimelineResponseVo of(UserPlan currUserPlan) {
        Integer totPlanMaxLimit = currUserPlan.getCharMaxLimit() - currUserPlan.getCharUseCnt();

        return UserTimelineResponseVo.builder()
                .userPlanNo(currUserPlan.getNo())
                .planNm(currUserPlan.getPlan().getPlanNm())
                .prevCharLeftCount(0)
                .prevCharUsedCount(0)
                .currCharLeftCount(currUserPlan.getCharMaxLimit())
                .currCharUsedCount(currUserPlan.getCharUseCnt())
                .totPlanMaxLimit(totPlanMaxLimit)
                .planStartAt(currUserPlan.getPlanStartAt())
                .planEndAt(currUserPlan.getPlanEndAt())
                .planEndedAt(currUserPlan.getUser().getPlanServiceEndAt())
                .active(currUserPlan.getActive())
                .build();
    }

    // 현재 플랜, 이전 플랜 있을 경우
    public static UserTimelineResponseVo of(UserPlan currUserPlan, UserPlan prevUserPlan) {
        Integer charTransferredUseCount = prevUserPlan.getCharUseCnt() - (prevUserPlan.getCharMaxLimit() - currUserPlan.getCharTransferred());
        Integer prevPlanTotMax = prevUserPlan.getActive() ? (currUserPlan.getCharTransferred() - charTransferredUseCount) : 0;
        Integer totPlanMaxLimit = currUserPlan.getCharMaxLimit() - currUserPlan.getCharUseCnt() + prevPlanTotMax;

        return UserTimelineResponseVo.builder()
                .userPlanNo(currUserPlan.getNo())
                .planNm(currUserPlan.getPlan().getPlanNm())
                .prevCharLeftCount(currUserPlan.getCharTransferred())
                .prevCharUsedCount(charTransferredUseCount)
                .currCharLeftCount(currUserPlan.getCharMaxLimit())
                .currCharUsedCount(currUserPlan.getCharUseCnt())
                .totPlanMaxLimit(totPlanMaxLimit)
                .planStartAt(currUserPlan.getPlanStartAt())
                .planEndAt(currUserPlan.getPlanEndAt())
                .planEndedAt(currUserPlan.getUser().getPlanServiceEndAt())
                .active(currUserPlan.getActive())
                .prevPlanActive(prevUserPlan.getActive())
                .build();
    }

    public static UserTimelineResponseVo of(UserPlan currUserPlan, UserPlan prevUserPlan, UserPlan prevPrevUserPlan) {
        Integer charTransferredUseCount = prevUserPlan.getCharUseCnt() - (prevUserPlan.getCharMaxLimit() - currUserPlan.getCharTransferred());

        Integer prevCharTransferredUseCount = prevPrevUserPlan.getCharUseCnt() - (prevPrevUserPlan.getCharMaxLimit() - prevUserPlan.getCharTransferred());

        Integer prevPlanTotMax = prevUserPlan.getActive() ? (currUserPlan.getCharTransferred() - charTransferredUseCount) : 0;
        Integer prevPrevPlanTotMax = prevPrevUserPlan.getActive() ? (prevUserPlan.getCharTransferred() - prevCharTransferredUseCount) : 0;

        Integer totPlanMaxLimit = currUserPlan.getCharMaxLimit() - currUserPlan.getCharUseCnt()
                + prevPlanTotMax
                + prevPrevPlanTotMax;

        return UserTimelineResponseVo.builder()
                .userPlanNo(currUserPlan.getNo())
                .planNm(currUserPlan.getPlan().getPlanNm())
                .prevCharLeftCount(currUserPlan.getCharTransferred())
                .prevCharUsedCount(charTransferredUseCount)
                .currCharLeftCount(currUserPlan.getCharMaxLimit())
                .currCharUsedCount(currUserPlan.getCharUseCnt())
                .totPlanMaxLimit(totPlanMaxLimit)
                .planStartAt(currUserPlan.getPlanStartAt())
                .planEndAt(currUserPlan.getPlanEndAt())
                .planEndedAt(currUserPlan.getUser().getPlanServiceEndAt())
                .active(currUserPlan.getActive())
                .prevPlanActive(prevUserPlan.getActive())
                .build();
    }
}

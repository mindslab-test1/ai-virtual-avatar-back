package ai.mindslab.ttsmaker.api.user.enums;

public enum UserRoleEnum {
	ROLE_USER, ROLE_DOWNLOAD, ROLE_ADMIN
}

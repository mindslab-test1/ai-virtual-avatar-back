package ai.mindslab.ttsmaker.api.user.vo.response;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserPlanListResponseVo {
    Long userPlanNo;
    Long userNo;
    Long planNo;
    String planNm;
    String planNmKr;
    Integer maxLimit;
    Integer price;
    String expiryDate;
    Boolean active;

    public static UserPlanListResponseVo of(UserPlan entity) {
        Plan plan = entity.getPlan();
        return UserPlanListResponseVo.builder()
                .userPlanNo(entity.getNo())
                .userNo(entity.getUser().getUserNo())
                .planNo(plan.getPlanNo())
                .planNm(plan.getPlanNm())
                .planNmKr(plan.getPlanNmKr())
                .maxLimit(plan.getMaxLimit())
                .price(plan.getPlanPrice())
                .expiryDate(DateUtils.asString(entity.getPlanEndAt(), "YYYY-MM-DD"))
                .active(entity.getActive())
                .build();
    }
}

package ai.mindslab.ttsmaker.api.user.repository;

import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserTimeline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserTimelineRepository extends JpaRepository<UserTimeline, Long> {
    @Query("select utl from UserTimeline utl where utl.user.userNo = ?1 ORDER BY utl.no DESC ")
    List<UserTimeline> findTimelineByUserNo(Long userNo);

}

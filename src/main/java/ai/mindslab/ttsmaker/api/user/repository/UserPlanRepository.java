package ai.mindslab.ttsmaker.api.user.repository;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserPlanRepository extends JpaRepository<UserPlan, Long> {
    @Query(value = "insert into UserPlan (" +
            "USER_NO, PLAN_NO, PAY_NO, CHAR_USE_CNT, CHAR_MAX_LIMIT, " +
            "ACTIVE, MEMO, PLAN_START_AT, PLAN_END_AT, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) " +
            "values (?1, ?2, ?3, 0, ?4, 1, null, ?5, ?6, ?5, ?1, null, null)", nativeQuery = true)
    void insertUserPlan(Long userNo, Long planNo, Long payNo, Integer maxLimit, LocalDateTime createDate, LocalDateTime payDate);

    @Query("select up from UserPlan up where up.payM.payNo = ?1 ")
    UserPlan findUserPlanByPayNo(Long payNo);

    @Query("select up from UserPlan up where up.plan.planNo = ?1 AND up.active= true ")
    List<UserPlan> findActiveUserPlanByPlanNo(Long planNo);

    @Query("select up from UserPlan up where up.no = ?1 ")
    UserPlan findUserPlanByUserPlanNo(Long userPlanNo);

    @Transactional
    @Modifying
    @Query("Update UserPlan up set up.active = 1 Where up.user = ?1")
    int updateUserPlan(Long userNo, int newCharLimit, Long planNo);

    @Query("SELECT up FROM UserPlan up WHERE up.user.userNo = ?1 AND up.active=?2 ORDER BY up.planEndAt DESC ")
    List<UserPlan> findActiveUserPlansByUserNoDesc(Long userNo, Boolean active);

    @Query("select p from UserPlan p where p.active = 1 and p.user.userNo = :userNo and now() BETWEEN p.planStartAt AND p.planEndAt ")
    List<UserPlan> getActiveUserPlanListFindByUserNo(@Param("userNo") Long userNo);
}

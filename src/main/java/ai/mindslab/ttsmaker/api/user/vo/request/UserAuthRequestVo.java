package ai.mindslab.ttsmaker.api.user.vo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAuthRequestVo {
	private Long userNo;
	private String refresh_token;
}

package ai.mindslab.ttsmaker.api.user.domain;

import ai.mindslab.ttsmaker.api.user.enums.SubscriptionStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "USER_TIMELINE")
public class UserTimeline implements Serializable {
    
    private static final long serialVersionUID = -8148522212617390551L;
    
    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    //유저 관리번호
    @OneToOne
    @JoinColumn(name = "USER_NO", nullable = false, foreignKey = @ForeignKey(name = "FK_USER"))
    private UserM user;

    //유저플랜 관리번호
    @OneToOne
    @JoinColumn(name = "USER_PLAN_NO", foreignKey = @ForeignKey(name = "FK_USER_PLAN"))
    private UserPlan userPlan;

    //유저 액션 상황
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "SUBSCRIPTION_STATUS")
    private SubscriptionStatus subscriptionStatus;

    //활성화 여부
    @Column(name = "ACTIVE")
    private Boolean active;

    //비고 (메모)
    @Column(name = "MEMO", columnDefinition = "varchar(255)")
    private String memo;

    //등록일시
    @Column(name = "CREATED_AT", updatable = false)
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //등록자
    @Column(name = "CREATED_BY")
    private Long createdBy;
}

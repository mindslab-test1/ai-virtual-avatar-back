package ai.mindslab.ttsmaker.api.user.service;

import ai.mindslab.ttsmaker.api.manage.user.service.conditions.UserSearchSpecs;
import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.user.domain.*;
import ai.mindslab.ttsmaker.api.user.enums.AuthProcessStatus;
import ai.mindslab.ttsmaker.api.user.enums.ProfanityStatus;
import ai.mindslab.ttsmaker.api.user.enums.SubscriptionStatus;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import ai.mindslab.ttsmaker.api.user.repository.*;
import ai.mindslab.ttsmaker.api.user.vo.request.UserAuthRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.UserProfanityRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.UserSignUpRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthService authService;
    
    @Autowired
    private UserPlanService userPlanService;

    @Autowired
    private UserMRepository userMRepository;

    @Autowired
    private UserMRoleRepository userMRoleRepository;

    @Autowired
    private UserPlanRepository userPlanRepository;

    @Autowired
    private UserDRepository userDRepository;

    @Autowired
    private UserTtsRepository userTtsRepository;

    @Autowired
    private UserTimelineRepository userTimelineRepository;


    @Override
    public UserVo getUserVoFindByUserId(String userId) {
        Optional<UserM> userOpt = userMRepository.findByUserId(userId);
        UserM userM = userOpt.get();
        userM.setUserPlans(userPlanRepository.getActiveUserPlanListFindByUserNo(userM.getUserNo()));
        if (userOpt.isPresent()) {
            return UserVo.of(userM);
        }
        return null;
    }

    @Override
    public UserM findByUserId(String userId) {
        Optional<UserM> userOpt = userMRepository.findByUserId(userId);
        if (userOpt.isPresent()) {
            return userOpt.get();
        }
        return null;
    }

    @Override
    public UserM getUserByUserNo(Long userNo) {
        return userMRepository.findByUserNo(userNo).orElse(null);
    }

    @Override
    public List<UserM> getUnsubscribingUsers() {
        return userMRepository.findByPlanCancelReqYn(true);
    }

    @Override
    public void resetUserPlanCancelReq(Long userNo) {
        userMRepository.resetUserPlanCancelReq(userNo);
    }


    @Override
    @Transactional
    public void deleteUserRole(UserM user, UserRoleEnum userRoleEnum) {
        UserRole userRole = userMRoleRepository.getUserRoleByUserNoAndRoleNm(user.getUserNo(), userRoleEnum);
        if (userRole == null) {
            throw new EntityNotFoundException("유저 권한 조회에 실패 하였습니다.");
        }
        
        userMRoleRepository.deleteUserRoleByRoleNo(userRole.getRoleNo());
    }

    @Override
    @Transactional
    public UserTimeline addUserTimeline(UserM user, UserPlan userPlan, String memo, SubscriptionStatus status, boolean active){
        UserTimeline userTimeline = new UserTimeline();
        userTimeline.setUser(user);
        userTimeline.setSubscriptionStatus(status);
        userTimeline.setUserPlan(userPlan);
        userTimeline.setMemo(memo);
        userTimeline.setActive(active);

        return userTimelineRepository.save(userTimeline);
    }

    @Override
    @Transactional
    public UserSignUpOutResponseVo userSignUp(UserSignUpRequestVo signUpVo) {
        Optional<UserM> userOpt = userMRepository.findByUserId(signUpVo.getUserId());
        if (userOpt.isPresent()) {
            String errorMessage = "이미 가입된 이메일입니다. 이전에 가입하셨다면, 아이디를 찾아서 로그인해보세요.";
            return UserSignUpOutResponseVo.of(AuthProcessStatus.SIGNUP_DUPLICATE, errorMessage);
        }

        UserM userM = new UserM();
        userM.setUserId(signUpVo.getUserId());
        userM.setUserPw(passwordEncoder.encode(signUpVo.getUserPw()));
        userM.setUserNm(signUpVo.getUserNm());
        userM.setCompanyNm(signUpVo.getCompanyNm());
        userM.setCellPhone(signUpVo.getCellphone());
        userM.setCreatedBy(0l);
        userM.setUpdatedBy(0l);

        userM = userMRepository.save(userM);

        UserRole userMRole = new UserRole();
        userMRole.setUser(userM);
        userMRole.setRoleNm(UserRoleEnum.ROLE_USER);
        userMRole.setCreatedBy(userM.getUserNo());
        userMRole.setUpdatedBy(userM.getUserNo());
        userMRoleRepository.save(userMRole);

        // 첫 가입 혜택 설정
        //UserPlan userPlan = userPlanService.setRegistrationBenefitsPlan(userM);

        //UserTimeline userTimeline = new UserTimeline();
        //userTimeline.setUser(userM);
        //userTimeline.setSubscriptionStatus(SubscriptionStatus.START);
        //userTimeline.setUserPlan(userPlan);
        //userTimeline.setActive(true);
        //userTimeline.setMemo("첫 가입 햬택");

        //userTimelineRepository.save(userTimeline);

        return UserSignUpOutResponseVo.of(AuthProcessStatus.SIGNUP_COMPLETED, "회원 가입 완료 되었습니다.");
    }

    @Override
    public UserSignUpOutResponseVo userSignOut(UserAuthRequestVo authRequestVo) {
        try {
            authService.setRefreshTokenBlacklist(authRequestVo.getRefresh_token());
        } catch (Exception ex) {
        }
        return UserSignUpOutResponseVo.of(AuthProcessStatus.SIGNOUT_COMPLETED, "로그아웃 처리 되었습니다.");
    }

    @Override
    public UserAuthTokenResponse generatorRefreshToken(String refresh_token) {
        return authService.generatorRefreshToken(refresh_token);
    }

    @Override
    public UserProfanityResponseVo getUseProfanityByUserId(UserProfanityRequestVo userProfanityRequestVo) {
        Optional<UserM> userMOpt = userMRepository.findByUserId(userProfanityRequestVo.getUserId());
        if (userMOpt.isPresent()) {
            LocalDateTime agreedAt = LocalDateTime.now().minusDays(1);
            if (userDRepository.countUserDByUserEqualsAndAgreedAtAfter(userMOpt.get(), agreedAt) > 0) {
                return UserProfanityResponseVo.of(true, ProfanityStatus.AGREED, "욕설사용 동의 이력이 존재합니다.");
            } else {
                return UserProfanityResponseVo.of(false, ProfanityStatus.NOT_AGREED, "욕설사용 동의 이력이 존재하지 않습니다.");
            }
        } else {
            return UserProfanityResponseVo.of(ProfanityStatus.FAIL, "회원이 존재하지 않습니다.");
        }
    }

    @Override
    public UserProfanityResponseVo saveUseProfanity(UserProfanityRequestVo userProfanityRequestVo) {
        Optional<UserM> userMOpt = userMRepository.findByUserId(userProfanityRequestVo.getUserId());
        if (userMOpt.isPresent()) {
            UserD userD = new UserD(userMOpt.get());
            userD = userDRepository.save(userD);
            if (userD.getNo() != null) {
                return UserProfanityResponseVo.of(ProfanityStatus.SUCCESS, "욕설사용 동의가 완료되었습니다.");
            } else {
                return UserProfanityResponseVo.of(ProfanityStatus.FAIL, "욕설사용 동의에 실패하였습니다.");
            }
        } else {
            return UserProfanityResponseVo.of(ProfanityStatus.FAIL, "회원이 존재하지 않습니다.");
        }
    }

    @Override
    public boolean saveUserTts(UserTts userTts) {
        return userTtsRepository.save(userTts).getNo() > 0;
    }

    @Override
    public List<UserTtsVo> getUserTtsList(UserSearchRequestDto searchDto) {
        if (StringUtils.hasText(searchDto.getUserId())) {
            Optional<UserM> opt = userMRepository.findByUserId(searchDto.getUserId());
            if (opt.isPresent()) {
                searchDto.setUserM(opt.get());
            }
        }
        return userTtsRepository.findAll(UserSearchSpecs.ttsSearchSpecs(searchDto)).stream().map(UserTtsVo::of).collect(Collectors.toList());
    }

}

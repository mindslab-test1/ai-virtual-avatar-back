package ai.mindslab.ttsmaker.api.user.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@EqualsAndHashCode
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USER_ROLE", uniqueConstraints = {@UniqueConstraint(name = "UK_USER_NO_ROLE_NM", columnNames = {"USER_NO", "ROLE_NM"})})
public class UserRole implements Serializable {
	
	private static final long serialVersionUID = -1028131151223137171L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
	private Long roleNo;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_NO", foreignKey = @ForeignKey(name = "FK_USER_ROLE__USER_M"))
	private UserM user;

	@Column(name = "ROLE_NM")
    @Enumerated(EnumType.STRING)
    private UserRoleEnum roleNm;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //등록자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CREATED_BY")
    private Long createdBy;

    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
    
    //수정자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UPDATED_BY")
    private Long updatedBy;
}
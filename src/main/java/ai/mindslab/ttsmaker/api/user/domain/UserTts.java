package ai.mindslab.ttsmaker.api.user.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_TTS", indexes = {
        @Index(name = "IDX_USER_TTS_01", columnList = "CREATED_AT"),
        @Index(name = "IDX_USER_TTS_02", columnList = "USER_NO,CREATED_AT"),
        @Index(name = "IDX_USER_TTS_03", columnList = "CHARACTER_ID"),
        @Index(name = "IDX_USER_TTS_04", columnList = "CHARACTER_ID,SCRIPT_ID"),
})
public class UserTts implements Serializable {

    private static final long serialVersionUID = -4779992413208812162L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    // User No
    @ManyToOne
    @JoinColumn(name = "USER_NO", foreignKey = @ForeignKey(name="FK_USER_TTS__USER_M"))
    private UserM user;

    // script text
    @Column(name = "SCRIPT_TEXT", columnDefinition = "varchar(1500)")
    private String scriptText;

    // scriptId
    @Column(name = "SCRIPT_ID", columnDefinition = "varchar(36)")
    private String scriptId;

    // characterId
    @Column(name = "CHARACTER_ID", columnDefinition = "varchar(36)")
    private String characterId;

    // 욕설 포함 여부
    @Column(name = "INC_PROF")
    private Boolean incProf;

    // 욕설 포함 여부 (API)
    @Column(name = "INC_PROF_API")
    private Boolean incProfApi;

    // 생성일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "NO",  referencedColumnName = "NO", foreignKey = @ForeignKey(name="FK_USER_TTS_PROF_API__USER_TTS"))
    Set<UserTtsProfApi> userTtsProfApis;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "NO",  referencedColumnName = "NO", foreignKey = @ForeignKey(name="FK_USER_TTS_PROF__USER_TTS"))
    Set<UserTtsProf> userTtsProfs;

}

package ai.mindslab.ttsmaker.api.user.enums;

public enum ActivityType {
    LOGIN, PAYMENT
}

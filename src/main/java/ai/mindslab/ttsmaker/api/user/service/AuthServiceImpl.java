package ai.mindslab.ttsmaker.api.user.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.user.domain.UserRefreshToken;
import ai.mindslab.ttsmaker.api.user.enums.AuthProcessStatus;
import ai.mindslab.ttsmaker.api.user.repository.UserRefreshTokenRepository;
import ai.mindslab.ttsmaker.api.user.vo.response.UserAuthTokenResponse;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthConst;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthData;
import ai.mindslab.ttsmaker.configurers.auth.JwtProvider;
import ai.mindslab.ttsmaker.util.DateUtils;

@Service
public class AuthServiceImpl implements AuthService {
	
	@Autowired
	private JwtProvider jwtProvider;
	
	@Autowired
	private UserRefreshTokenRepository userRefreshTokenRepository;
	
	public AVAAuthData makejwtToken(Long userNo, String userId) {
		String access_token = jwtProvider.generateJwtToken(userNo, userId, Duration.ofMinutes(30)); //Duration.ofSeconds(10));
		String refresh_token = jwtProvider.generateJwtToken(userNo, userId, Duration.ofHours(2)); //Duration.ofSeconds(30));;
		
		String accessTokenBody = access_token.replace(AVAAuthConst.AUTH_TOKEN_PREFIX,"");
		String refreshTokenBody = refresh_token.replace(AVAAuthConst.AUTH_TOKEN_PREFIX,"");
		LocalDateTime accessTokenExpirationDate = DateUtils.convertToLocalDateTime(jwtProvider.getClaims(accessTokenBody).getExpiration());
		LocalDateTime refreshTokenExpirationDate = DateUtils.convertToLocalDateTime(jwtProvider.getClaims(refreshTokenBody).getExpiration());
		
		return AVAAuthData.builder()
				.access_token(accessTokenBody)
				.access_token_expire(accessTokenExpirationDate)
				.refresh_token(refreshTokenBody)
				.refresh_token_expire(refreshTokenExpirationDate)
				.build();
	}
	
	@Override
	public boolean saveRefreshToken(Long userNo, String userId, String refresh_token, LocalDateTime refresh_token_expire) {
		Optional<UserRefreshToken> userRefreshTokenOpt = userRefreshTokenRepository.findByRefreshToken(refresh_token);
		
		boolean save = false;
		try {
			UserRefreshToken userRefreshToken = null;
			if(userRefreshTokenOpt.isPresent()) {
				userRefreshToken = userRefreshTokenOpt.get();
				userRefreshToken.setRefreshTokenExpireDate(refresh_token_expire);
			}else {
				userRefreshToken = new UserRefreshToken();
				userRefreshToken.setUserNo(userNo);
				userRefreshToken.setUserId(userId);
				userRefreshToken.setRefreshToken(refresh_token);
				userRefreshToken.setRefreshTokenExpireDate(refresh_token_expire);
			}
			userRefreshTokenRepository.save(userRefreshToken);
			save = true;
		}catch (Exception e) {
			throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRSU002", "userRefreshToken 생성중 오류가 발생 했습니다.");
		}
		
		return save;
	}

	@Override
	public boolean setRefreshTokenBlacklist(String refresh_token) {
		return userRefreshTokenRepository.setRefreshTokenBlacklist(refresh_token) > 0;
	}

	@Override
	public UserAuthTokenResponse generatorRefreshToken(String refresh_token) {
		Optional<UserRefreshToken> userRefreshTokenOpt = userRefreshTokenRepository.findByRefreshToken(refresh_token);
		if(userRefreshTokenOpt.isPresent()) {
			UserRefreshToken userRefreshToken = userRefreshTokenOpt.get();
			LocalDateTime now = LocalDateTime.now();
			if(now.isAfter(userRefreshToken.getRefreshTokenExpireDate())) {
				return UserAuthTokenResponse.of(AuthProcessStatus.REFRESH_TOKEN_EXPIRED, String.format("refresh token expired(%s).", DateUtils.asString(userRefreshToken.getRefreshTokenExpireDate(), "yyyy-MM-dd")));
			}else {
				Long userNo = userRefreshToken.getUserNo();
				String userId = userRefreshToken.getUserId();
				String base_refresh_token = userRefreshToken.getRefreshToken();
				if(now.minusMinutes(30).isAfter(userRefreshToken.getRefreshTokenExpireDate())) {
					AVAAuthData authData =  this.makejwtToken(userNo, userId);
					userRefreshTokenRepository.setRefreshTokenBlacklist(base_refresh_token);
			       return UserAuthTokenResponse.newAccessRefreshToken(authData);
				}else {
					AVAAuthData authData =  this.makejwtToken(userNo, userId);
					return UserAuthTokenResponse.newAccessToken(authData);
				}
			}
		}
		return UserAuthTokenResponse.of(AuthProcessStatus.REFRESH_TOKEN_NOT_FOUND, "refresh token does not exist.");
	}
}

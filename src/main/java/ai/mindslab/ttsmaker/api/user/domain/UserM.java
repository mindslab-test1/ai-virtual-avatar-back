package ai.mindslab.ttsmaker.api.user.domain;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "USER_M", uniqueConstraints = {@UniqueConstraint(name = "UK_USER_ID", columnNames = {"USER_ID"})})
public class UserM implements Serializable {

    private static final long serialVersionUID = -1064932611568571717L;

    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long userNo;

    //user ID
    @Column(name = "USER_ID", columnDefinition = "varchar(150)")
    private String userId;

    //이름
    @Column(name = "USER_NM", columnDefinition = "varchar(100)")
    private String userNm;

    //user PW
    @Column(name = "USER_PW", columnDefinition = "varchar(100)")
    private String userPw;

    //전화번호
    @Column(name = "CELL_PHONE", columnDefinition = "varchar(11)")
    private String cellPhone;

    //회사
    @Column(name = "COMPANY_NM", columnDefinition = "varchar(20)")
    private String companyNm;

    //유저 권한 리스트
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    //@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserRole> userRoles = new ArrayList<>();

    //유저 구독 플랜 리스트
    @Where(clause = "active=true")
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserPlan> userPlans = new ArrayList<>();

    //활성화 여부
    @Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;

    //구독 취소 요청 여부
    @Column(name = "PLAN_CANCEL_REQ_YN", columnDefinition = "boolean default false")
    private Boolean planCancelReqYn;

    //구독 서비스 요청일시
    @Column(name = "PLAN_CANCEL_REQ_AT")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planCancelReqAt;

    //구독 서비스 종료일시
    @Column(name = "PLAN_SERVICE_END_AT")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planServiceEndAt;

    @Column(name = "PLAN_MOD_CNT")
    private Integer planModCnt;

    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //등록자
    @Column(name = "CREATED_BY")
    private Long createdBy;

    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;

    //수정자
    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    public void addRole(final UserRole userRole) {
        userRoles.add(userRole);
    }

    public void deleteRole(final UserRoleEnum userRoleEnum) {
        userRoles.removeIf(ur-> ur.getRoleNm().equals(userRoleEnum));
    }
}

package ai.mindslab.ttsmaker.api.user.vo.response;

import ai.mindslab.ttsmaker.api.user.enums.AuthProcessStatus;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthData;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthTokenResponse {
	private String processStatus;
	private String processMessage;
	
	private String access_token;
	private String access_token_expire;
	
	private String refresh_token;
	private String refresh_token_expire;
	
	public static UserAuthTokenResponse of(AuthProcessStatus statusEnum, String processMessage) {
		return UserAuthTokenResponse.builder()
        		.processStatus(statusEnum.name())
        		.processMessage(processMessage)
        		.build();
	}
		
	public static UserAuthTokenResponse newAccessToken(AVAAuthData authData) {
		return UserAuthTokenResponse.builder()
        		.processStatus(AuthProcessStatus.NEW_ACCESS_TOKEN_ISSUE_COMPLETE.name())
        		.processMessage("")
        		.access_token(authData.getAccess_token())
        		.access_token_expire(DateUtils.asString(authData.getAccess_token_expire(), "yyyy-MM-dd HH:mm:ss"))
        		.build();
	}
	
	public static UserAuthTokenResponse newAccessRefreshToken(AVAAuthData authData) {
		return UserAuthTokenResponse.builder()
        		.processStatus(AuthProcessStatus.NEW_TOKEN_ISSUE_COMPLETE.name())
        		.processMessage("")
        		.access_token(authData.getAccess_token())
        		.access_token_expire(DateUtils.asString(authData.getAccess_token_expire(), "yyyy-MM-dd HH:mm:ss"))
        		.refresh_token(authData.getRefresh_token())
        		.refresh_token_expire(DateUtils.asString(authData.getRefresh_token_expire(), "yyyy-MM-dd HH:mm:ss"))
        		.build();
	}
}

package ai.mindslab.ttsmaker.api.user.repository;


import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface UserInquiryRepository extends JpaRepository<UserInquiry, Long>, JpaSpecificationExecutor<UserInquiry> {


    @Query("SELECT ui FROM UserInquiry ui WHERE ui.user.userNo = ?1")
    List<UserInquiry> findAllByUserNo(Long userNo);

    @Query("SELECT ui FROM UserInquiry ui WHERE ui.inquiryNo = ?1")
    UserInquiry findInquiryByInquiryNo(Long inquiryNo);

    @Query(value = "INSERT INTO User_Inquiry (INQUIRY_TYPE, INQUIRY_TITLE, INQUIRY_QUESTION, INQUIRY_ANSWER, " +
            "INQUIRY_STATUS, CREATED_AT, ANSWERED_AT, ANSWERED_EMAIL, USER_NO) values (?1, ?2, ?3, null, " +
            "false, now(), null, null, ?4)", nativeQuery = true)
    void insertUserInquiry(String inquiryType, String inquiryTitle, String inquiryQuestion, Long userNo);

    @Transactional
    @Modifying
    @Query("UPDATE UserInquiry u set u.inquiryStatus = true, u.answeredAt = ?1, u.answeredEmail =?2, u.inquiryAnswer = ?3 " +
            "where u.inquiryNo = ?4")
    void updateUserInquiry(LocalDateTime answeredAt, String answeredEmail, String inquiryAnswer, Long inquiryNo);
    
    Page<UserInquiry> findAll(Specification<UserInquiry> specs, Pageable pageable);


//    @Query(value = "insert into Billing (ACTIVE, BILL_KEY, PAY_NO, FAIL_REASON, CARD_NUM, " +
//            "PAYMENT, PAYMENT_DATE, CARD_APPL_CODE, PLAN_NO, USER_NO, " +
//            "CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) " +
//            "values (1, ?1, ?3, ?4, ?5, " +
//            "?6, ?7, ?8, ?9, ?2," +
//            "?10, ?2, null, null)", nativeQuery = true)
//    void insertBill(String billKey, Long userNo, Long payNo, String failReason, String cardNum,
//                    String payMethod, LocalDateTime payDate, String cardApplCode, Long planNo, LocalDateTime createDate);
//
//    @Transactional
//    @Modifying
//    @Query("update Billing b set b.billKey = ?1 ,b.paymentDate = ?2, b.updatedAt = ?3, " +
//            "b.failReason = ?5, b.payNo = ?6 where b.no = ?4 ")
//    void updateFailedBillingInfo(String failReason, Long billingNo);
}

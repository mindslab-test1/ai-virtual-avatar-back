package ai.mindslab.ttsmaker.api.user.vo.response;

import ai.mindslab.ttsmaker.api.user.enums.ProfanityStatus;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserProfanityResponseVo {

	private Boolean agree;
	private String profanityStatus;
	private String message;
	
	public static UserProfanityResponseVo of(ProfanityStatus profanityStatus, String msg) {
		return UserProfanityResponseVo.builder()
        		.profanityStatus(profanityStatus.name())
        		.message(msg)
        		.build();
	}

	public static UserProfanityResponseVo of(boolean agree, ProfanityStatus profanityStatus, String msg) {
		return UserProfanityResponseVo.builder()
				.agree(agree)
				.profanityStatus(profanityStatus.name())
				.message(msg)
				.build();
	}

}

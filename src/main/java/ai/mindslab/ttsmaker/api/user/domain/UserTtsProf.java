package ai.mindslab.ttsmaker.api.user.domain;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_TTS_PROF")
public class UserTtsProf implements Serializable {

    private static final long serialVersionUID = -828413259849686823L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROF_NO")
    private Long profNo;

    @Column(name = "NO")
    private Long no;

    // 욕설 text
    @Column(name = "PROF_TEXT", columnDefinition = "varchar(24)")
    private String profText;

    // 욕설 포함 수
    @Column(name = "INC_COUNT")
    private int incCnt;

}

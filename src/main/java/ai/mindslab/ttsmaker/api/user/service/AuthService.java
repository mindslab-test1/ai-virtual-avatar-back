package ai.mindslab.ttsmaker.api.user.service;

import java.time.LocalDateTime;

import ai.mindslab.ttsmaker.api.user.vo.response.UserAuthTokenResponse;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthData;

public interface AuthService {
	
	public AVAAuthData makejwtToken(Long userNo, String userId);
	public boolean saveRefreshToken(Long userNo, String userId, String refresh_token, LocalDateTime refresh_token_expire);
	public boolean setRefreshTokenBlacklist(String refresh_token);
	public UserAuthTokenResponse generatorRefreshToken(String refresh_token);

}

package ai.mindslab.ttsmaker.api.user.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_D", indexes = {@Index(name = "IDX_USER_D_01", columnList = "AGREED_AT")})
public class UserD implements Serializable {

    private static final long serialVersionUID = 6910057134561616709L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    // User No (FIXME : 영속관계 헤제)
    @ManyToOne
    @JoinColumn(name = "USER_NO", foreignKey = @ForeignKey(name="FK_USER_D__USER_M"))
    private UserM user;

    // 동의일시
    @Column(name = "AGREED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime agreedAt;

    public UserD(UserM user) {
        this.user = user;
    }

}

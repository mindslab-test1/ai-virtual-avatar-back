package ai.mindslab.ttsmaker.api.user.domain;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_TTS_PROF_API")
public class UserTtsProfApi implements Serializable {

    private static final long serialVersionUID = -7680811805395252153L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROF_API_NO")
    private Long profApiNo;

    @Column(name = "NO")
    private Long no;

    // category
    @Column(name = "CATEGORY", columnDefinition = "varchar(24)", nullable = false)
    private String category;

    // label
    @Column(name = "LABEL", columnDefinition = "varchar(24)", nullable = false)
    private String label;

    // reliability
    @Column(name = "RELIABILITY")
    private double reliability;

}

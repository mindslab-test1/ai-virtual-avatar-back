package ai.mindslab.ttsmaker.api.user.vo.response;


import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInquiryResponseVo {
    private static List<UserInquiryResponseVo> userInquiryResponseVoList = new ArrayList<>();
    private Long inquiryNo;
    private String inquiryType;
    private String inquiryTitle;
    private String inquiryQuestion;
    private String inquiryAnswer;
    private Long userNo;

    public static UserInquiryResponseVo of(UserInquiry entity) {

        return UserInquiryResponseVo.builder()
                .inquiryNo(entity.getInquiryNo())
                .inquiryType(entity.getInquiryType())
                .inquiryTitle(entity.getInquiryTitle())
                .inquiryQuestion(entity.getInquiryQuestion())
                .inquiryAnswer(entity.getInquiryAnswer())
                .userNo(entity.getUser().getUserNo())
                .build();
    }

    public static List<UserInquiryResponseVo> of(List<UserInquiry> entity) {
        for (UserInquiry userInquiry : entity) {
            userInquiryResponseVoList.add(UserInquiryResponseVo.builder()
                    .inquiryNo(userInquiry.getInquiryNo())
                    .inquiryType(userInquiry.getInquiryType())
                    .inquiryTitle(userInquiry.getInquiryTitle())
                    .inquiryQuestion(userInquiry.getInquiryQuestion())
                    .inquiryAnswer(userInquiry.getInquiryAnswer())
                    .userNo(userInquiry.getUser().getUserNo())
                    .build());
        }
        return userInquiryResponseVoList;
    }

}

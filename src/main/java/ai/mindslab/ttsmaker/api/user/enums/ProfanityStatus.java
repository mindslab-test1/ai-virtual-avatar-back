package ai.mindslab.ttsmaker.api.user.enums;

public enum ProfanityStatus {
    SUCCESS, FAIL, INCLUDE, NON_INCLUDE, AGREED, NOT_AGREED
}

package ai.mindslab.ttsmaker.api.user.service;

import ai.mindslab.ttsmaker.api.user.domain.UserInquiry;
import ai.mindslab.ttsmaker.api.user.repository.UserInquiryRepository;
import ai.mindslab.ttsmaker.api.user.vo.request.InquiryRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserInquiryResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class UserInquiryServiceImpl implements UserInquiryService {
    @Autowired
    private UserInquiryRepository userInquiryRepository;


    @Override
    public ResponseEntity<List<UserInquiryResponseVo>> getUserInquiriesListByUserNo(Long userNo) throws DataAccessException {
        log.info("userNo[{}] >> 문의 목록 조회", userNo);
        List<UserInquiry> userInquiries = userInquiryRepository.findAllByUserNo(userNo);
        log.info("userNo[{}] >> 문의 목록 조회 완료. userInquiries = {}", userNo, userInquiries);
        List<UserInquiryResponseVo> userInquiryResponseVo = UserInquiryResponseVo.of(userInquiries);

        return ResponseEntity.ok(userInquiryResponseVo);
    }

    @Override
    public ResponseEntity<UserInquiryResponseVo> getUserInquiryByInquiryNo(Long inquiryNo) throws DataAccessException {
        log.info("inquiryNo[{}] >> 특정 문의 조회", inquiryNo);
        UserInquiry userInquiry = userInquiryRepository.findInquiryByInquiryNo(inquiryNo);
        log.info("inquiryNo[{}] >> 특정 문의 조회 완료. userInquiry = {}", inquiryNo, userInquiry);
        UserInquiryResponseVo userInquiryResponseVo = UserInquiryResponseVo.of(userInquiry);
        return ResponseEntity.ok(userInquiryResponseVo);
    }

    @Override
    public void insertInquiry(InquiryRequestVo inquiryRequestVo, Long userNo) throws DataAccessException {
        log.info("userNo[{}] >> 문의 등록. inquiryRequestVo = {}", userNo, inquiryRequestVo);
        userInquiryRepository.insertUserInquiry(inquiryRequestVo.getInquiryType(),
                inquiryRequestVo.getInquiryTitle(), inquiryRequestVo.getInquiryQuestion(),
                userNo);
    }

    @Override
    public void replyInquiry(InquiryRequestVo inquiryRequestVo, String adminEmail) throws DataAccessException {
        log.info("inquiryNo[{}] >> 문의 답변 등록. adminEmail[{}]", inquiryRequestVo.getInquiryNo(), adminEmail);
        userInquiryRepository.updateUserInquiry(LocalDateTime.now(), adminEmail,
            inquiryRequestVo.getInquiryAnswer(), inquiryRequestVo.getInquiryNo());
    }


}

package ai.mindslab.ttsmaker.api.user.vo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSignUpRequestVo {
	private String userId;
	private String userPw;
	private String userNm;
	private String cellphone;
	private String companyNm;
}

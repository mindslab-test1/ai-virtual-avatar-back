package ai.mindslab.ttsmaker.api.user.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.ttsmaker.api.user.domain.UserM;

@Repository
public interface UserMRepository extends JpaRepository<UserM, Long>, JpaSpecificationExecutor<UserM> {

    @EntityGraph(attributePaths = {"userPlans", "userPlans.plan"}, type = EntityGraph.EntityGraphType.LOAD)
    Page<UserM> findAll(Specification<UserM> specs, Pageable pageable);

    @Query("SELECT m FROM UserM m LEFT JOIN FETCH m.userRoles where m.userId = :userId")
    Optional<UserM> findByUserId(@Param("userId") String userId);

    @Query("SELECT m FROM UserM m LEFT JOIN FETCH m.userRoles where m.userNo = :userNo")
    Optional<UserM> findByUserNo(@Param("userNo") Long userNo);
    
    @Query("SELECT m FROM UserM m where m.userNo = :userNo")
    UserM findByUserNoWithoutUserRoles(@Param("userNo") Long userNo);

    @Transactional
    @Modifying
    @Query("Update UserM u set u.planServiceEndAt = ?2 Where u.userNo = ?1 ")
    int updatePlanServiceEndAt(Long userNo, LocalDateTime endAt);


    @Query("SELECT m FROM UserM m LEFT JOIN FETCH m.userRoles where m.planCancelReqYn = :cancelReq")
    List<UserM> findByPlanCancelReqYn(@Param("cancelReq") Boolean cancelReq);

    @Transactional
    @Modifying
    @Query("update UserM u set u.planCancelReqYn = false , u.planCancelReqAt = null,u.planServiceEndAt = null  where u.userNo = :userNo ")
    void resetUserPlanCancelReq(@Param("userNo") Long userNo);
}

package ai.mindslab.ttsmaker.api.user.service;

import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserTimeline;
import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import ai.mindslab.ttsmaker.api.user.enums.SubscriptionStatus;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import ai.mindslab.ttsmaker.api.user.vo.request.UserAuthRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.UserProfanityRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.request.UserSignUpRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.*;

import javax.transaction.Transactional;
import java.util.List;

public interface UserService {
	public UserVo getUserVoFindByUserId(String userId);

	public UserM findByUserId(String userId);

	void deleteUserRole(UserM user, UserRoleEnum userRoleEnum);

	public UserTimeline addUserTimeline(UserM user, UserPlan userPlan, String memo, SubscriptionStatus status,  boolean active);

	public UserSignUpOutResponseVo userSignUp(UserSignUpRequestVo userSignUpRequestVo);

	public UserSignUpOutResponseVo userSignOut(UserAuthRequestVo authRequestVo);

	public UserAuthTokenResponse generatorRefreshToken(String refresh_token);

	UserM getUserByUserNo(Long userNo);

    List<UserM> getUnsubscribingUsers();

	public void resetUserPlanCancelReq(Long userNo);

	public UserProfanityResponseVo getUseProfanityByUserId(UserProfanityRequestVo userUseProfanityRequestVo);

	public UserProfanityResponseVo saveUseProfanity(UserProfanityRequestVo userUseProfanityRequestVo);

	public boolean saveUserTts (UserTts userTts);

	public List<UserTtsVo> getUserTtsList(UserSearchRequestDto searchDto);

}

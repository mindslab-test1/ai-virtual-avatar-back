package ai.mindslab.ttsmaker.api.user.repository;

import ai.mindslab.ttsmaker.api.user.domain.UserD;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface UserDRepository extends JpaRepository<UserD, Long>, JpaSpecificationExecutor<UserD> {

    long countUserDByUserEqualsAndAgreedAtAfter(UserM userM, LocalDateTime agreedAt);

}

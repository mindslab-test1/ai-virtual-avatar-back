package ai.mindslab.ttsmaker.api.user.vo.response;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import org.springframework.util.CollectionUtils;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserRole;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserVo implements Serializable {
    private static final long serialVersionUID = 5374884932662896408L;
    private Long userNo;
    private String userId;
    private String userPw;
    private String userNm;
    private String companyNm;
    private Set<String> roles;
    private List<Long> userPlanNo;
    private Boolean active;

    public static UserVo of(UserM entity) {

        List<UserRole> roleList = entity.getUserRoles();
        Set<String> roles = new HashSet<>();
        if (!CollectionUtils.isEmpty(roleList)) {
            roles = roleList.stream().map(UserRole::getRoleNm).map(UserRoleEnum::name).collect(HashSet::new, HashSet::add, HashSet::addAll);
        } else {
            roles.add("ROLE_USER");
        }

        return UserVo.builder()
                .userNo(entity.getUserNo())
                .userId(entity.getUserId())
                .userPw(entity.getUserPw())
                .userNm(entity.getUserNm())
                .companyNm(entity.getCompanyNm())
                .roles(roles)
                .userPlanNo(entity.getUserPlans()
                        .stream()
                        .map(UserPlan::getNo)
                        .collect(Collectors.toList()))
                .active(entity.getActive())
                .build();
    }
}

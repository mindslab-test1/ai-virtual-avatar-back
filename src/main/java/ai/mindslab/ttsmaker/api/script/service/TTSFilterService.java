package ai.mindslab.ttsmaker.api.script.service;

import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;

import java.util.List;

public interface TTSFilterService {
	List<String> swearWords();
	void setTtsConversionData(String jobId, String scriptId, String characterId, String text, AvaUser avaUser);
}

package ai.mindslab.ttsmaker.api.script.service;

import ai.mindslab.ttsmaker.api.character.service.VFCharacterService;
import ai.mindslab.ttsmaker.api.character.vo.response.VFCharacterResponseVo;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiServiceImpl;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.TTSApiResponseMessage;
import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TTSMakeServiceImpl implements TTSMakeService {

	@Autowired
    private TTSApiServiceImpl maumAiApiService;
	
	@Autowired
	private VFCharacterService vfCharacterService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private TTSFilterService ttsFilterService;

    @Value("${spring.grpc.active}")
    boolean textFilterActive;

    @Override
    public TTSMakeResultVo requestTTSMakeFile(String jobId, String scriptId, String characterId, String text, AvaUser avaUser) throws IOException {
    	// TODO : 이후 캐싱 처리 필요
    	List<VFCharacterResponseVo> voiceFontList = vfCharacterService.getAllList();
    	Map<String, String> voiceFontMap = voiceFontList.stream().collect(Collectors.toMap(VFCharacterResponseVo::getCharacterId, VFCharacterResponseVo::getVoiceFontNm));
    	String voiceName = voiceFontMap.get(characterId);
    	log.info(voiceName);
    	// TTS 변환정보 DB적재
        if(textFilterActive){
            Executors.newSingleThreadExecutor().submit(() -> ttsFilterService.setTtsConversionData(jobId, scriptId, characterId, text, avaUser));
        }else{
            log.info("grpc prop is not active.");
        }
        // TTS 변환 요청
    	log.info("TTSMakeService:requestTTSMakeFile() = TTS 변환 요청 / START");
        TTSApiResponseMessage ttsApiResponseMessage = maumAiApiService.requestTTSStream(voiceName, text, scriptId);
        log.info("TTSMakeService:requestTTSMakeFile() = TTS 변환 요청 / END");

        // TTS 변환 결과 Redis 임시 보관 처리
        log.info("TTSMakeService:requestTTSMakeFile() = TTS 변환 결과 Redis 임시 보관 처리 / START");
        setTTSResultTagetByRedis(jobId, scriptId, ttsApiResponseMessage);
        log.info("TTSMakeService:requestTTSMakeFile() = TTS 변환 결과 Redis 임시 보관 처리 / END");

		return TTSMakeResultVo.of(ttsApiResponseMessage);
    }

	public boolean setTTSResultTagetByRedis(String jobId, String scriptId, TTSApiResponseMessage ttsApiResponseMessage) {
        try {
            String key = String.format("ava:job:%s:tts-result", jobId);
            redisTemplate.opsForHash().put(key, scriptId, ttsApiResponseMessage);
            redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0010", "TTSApiResponseMessage 업데이트 처리 중 오류가 발생했습니다.");
        }
    }
}

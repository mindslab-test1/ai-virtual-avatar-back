package ai.mindslab.ttsmaker.api.script.service;

import java.io.IOException;

import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;

public interface TTSMakeService {
	public TTSMakeResultVo requestTTSMakeFile(String jobId, String scriptId, String characterId, String text, AvaUser avaUser) throws IOException;
}

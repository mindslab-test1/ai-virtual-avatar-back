package ai.mindslab.ttsmaker.api.script.service;

import ai.mindslab.ttsmaker.configurers.grpc.TextFilterStubFactory;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.textfilter.TextFilterProto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * ai.mindslab.ttsmaker.api.script.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-02-09  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-02-09
 */
@Slf4j
@Service
public class TextFilterService {

    final TextFilterStubFactory textFilterStubFactory;

    @Value("${spring.grpc.active}")
    boolean isActive;

    public TextFilterService(TextFilterStubFactory textFilterStubFactory) {
        this.textFilterStubFactory = textFilterStubFactory;
    }

    public void sendBlokingPredictTextLabel(TextFilterProto.TextFilterRequest textFilterRequest) {
        if(isActive){
            log.info("TextFilter gRPC Blocking request. textFilterRequest = {}", textFilterRequest);
            TextFilterProto.TextFilterResponse textFilterResponse = textFilterStubFactory.getBlockingStub().predictTextLabel(textFilterRequest);
            log.info("TextFilter gRPC Blocking response. textFilterResponse = {}", textFilterResponse);
        }else{
            log.info("grpc prop is not active.");
        }
    }

    public void sendAsyncPredictTextLabel(TextFilterProto.TextFilterRequest textFilterRequest, StreamObserver<TextFilterProto.TextFilterResponse> responseStreamObserver) {
        if(isActive){
            log.info("TextFilter gRPC Async request. textFilterRequest = {}", textFilterRequest);
            if(responseStreamObserver == null){
                textFilterStubFactory.getAsyncStub().predictTextLabel(textFilterRequest, new StreamObserver<TextFilterProto.TextFilterResponse>() {
                    @Override
                    public void onNext(TextFilterProto.TextFilterResponse value) {
                        log.info("TextFilter gRPC Async response. textFilterResponse = {}", value);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        log.error("TextFilter gRPC Async request error. message = {}", throwable.getMessage());
                    }

                    @Override
                    public void onCompleted() {
                        log.info("TextFilter gRPC Async reqeust completed.");
                    }
                });
            }else{
                textFilterStubFactory.getAsyncStub().predictTextLabel(textFilterRequest, responseStreamObserver);
            }
        }else{
            log.info("grpc prop is not active.");
        }
    }

    public void sendFuturePredictTextLabel(TextFilterProto.TextFilterRequest textFilterRequest) {
        if(isActive){
            log.info("TextFilter gRPC Future request. textFilterRequest = {}", textFilterRequest);
            TextFilterProto.TextFilterResponse textFilterResponse = null;
            ListenableFuture<TextFilterProto.TextFilterResponse> listenableFuture = textFilterStubFactory.getFutureStub().predictTextLabel(textFilterRequest);
            try {
                textFilterResponse = listenableFuture.get(2, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                log.error("TextFilter gRPC Future request error. message = {}", e.getMessage());
            }
            log.info("TextFilter gRPC Future response. textFilterResponse = {}", textFilterResponse);
        }else{
            log.info("grpc prop is not active.");
        }
    }

}

package ai.mindslab.ttsmaker.api.script.service;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import ai.mindslab.ttsmaker.api.user.domain.UserTtsProf;
import ai.mindslab.ttsmaker.api.user.domain.UserTtsProfApi;
import ai.mindslab.ttsmaker.api.user.service.UserServiceImpl;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import com.google.common.collect.Lists;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.textfilter.TextFilterProto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TTSFilterServiceImpl implements TTSFilterService {

	@Autowired
	private TextFilterService textFilterService;

	@Autowired
	private UserServiceImpl userService;

	@Autowired
	private TTSFilterService ttsFilterService;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	public List<String> swearWords() {
		List<String> swearWords = Lists.newArrayList();
		try {
			ClassPathResource classPathResource = new ClassPathResource("words.txt");
			InputStreamReader isr = new InputStreamReader(classPathResource.getInputStream(), StandardCharsets.UTF_8);
			swearWords = new BufferedReader(isr).lines().collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return swearWords;
	}

	@Override
	public void setTtsConversionData(String jobId, String scriptId, String characterId, String text, AvaUser avaUser) {
		textFilterService.sendAsyncPredictTextLabel(TextFilterProto.TextFilterRequest.newBuilder().setText(text).build(),  new StreamObserver<TextFilterProto.TextFilterResponse>() {
			@Override
			public void onNext(TextFilterProto.TextFilterResponse value) {
				UserTts userTts = new UserTts();
				UserM userM = userService.findByUserId(avaUser.getUserId());
				String scriptCountKey = "ava:default:filter:count:"+scriptId;
				Map<Object, Object> count = (Map<Object, Object>) redisTemplate.opsForValue().get(scriptCountKey);
				if(count == null){
					String key = "ava:default:filter:words";
					count = Optional.ofNullable((List<String>) redisTemplate.opsForValue().get(key))
							.orElseGet(() -> {
								List<String> words = ttsFilterService.swearWords();
								redisTemplate.opsForValue().set(key, words);
								return words;
							})
							.stream()
							.filter(text::contains)
							.map(v -> {
								long cnt = 0;
								Matcher matcher = Pattern.compile(v).matcher(text);
								while (matcher.find()) cnt++;
								return new Object[]{v, cnt};
							}).collect((Collectors.toMap(v -> v[0], v -> v[1], (o, n) -> o)));
				}
				Map<Object, Object> finalCount = count;
				log.info("TextFilter gRPC Async response. textFilterResponse = {}", value);
				userTts.setScriptText(text);
				if(finalCount != null){
					userTts.setIncProf(finalCount.size() > 0 ? true : false);
				}else{
					userTts.setIncProf(false);
				}
				userTts.setCharacterId(characterId);
				userTts.setScriptId(scriptId);
				userTts.setUser(userM);
				userTts.setUserTtsProfApis(value.getPredictionsList().stream()
						.map(v -> {
							UserTtsProfApi userTtsProfApi = new UserTtsProfApi();
							userTtsProfApi.setCategory(v.getCategory());
							userTtsProfApi.setLabel(v.getLabel());
							userTtsProfApi.setReliability(v.getReliability());
							if(userTts.getIncProfApi() == null || !userTts.getIncProfApi()){
								userTts.setIncProfApi(v.getReliability() >= 0.8);
							}
							return userTtsProfApi;
						}).collect(Collectors.toSet()));
				if(finalCount != null && finalCount.size() > 0){
					userTts.setUserTtsProfs(
							finalCount.entrySet().stream()
									.map(v -> {
										UserTtsProf userTtsProf = new UserTtsProf();
										userTtsProf.setProfText((String)v.getKey());
										userTtsProf.setIncCnt(Math.toIntExact((Long)v.getValue()));
										return userTtsProf;
									}).collect(Collectors.toSet())
					);
				}
				userService.saveUserTts(userTts);
			}

			@Override
			public void onError(Throwable throwable) {
				log.error("TextFilter gRPC Async request error. message = {}", throwable.getMessage());
			}

			@Override
			public void onCompleted() {
				log.info("TextFilter gRPC Async reqeust completed.");
			}
		});
	}

}

package ai.mindslab.ttsmaker.api.script.domain.dto.response;

import java.time.LocalDate;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.TTSApiResponseMessage;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder(toBuilder = true)
public class TTSMakeResultVo {
	private String requestId;		// 요청 UUID
	private String voiceName;		// 화자 
	private String text;			// TTS Text
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate expiryDate;
	private byte[] resource;
	private float resourceDuration;
	
	public static TTSMakeResultVo of(TTSApiResponseMessage ttsApiResponseMessage) {
		return TTSMakeResultVo.builder()
				.requestId(UUID.randomUUID().toString())
				.voiceName(ttsApiResponseMessage.getVoiceNm())
				.text(ttsApiResponseMessage.getText())
				.expiryDate(ttsApiResponseMessage.getExpiredDate())
				.resource(ttsApiResponseMessage.getResource())
				.resourceDuration(ttsApiResponseMessage.getResourceDuration())
				.build();
	}
}

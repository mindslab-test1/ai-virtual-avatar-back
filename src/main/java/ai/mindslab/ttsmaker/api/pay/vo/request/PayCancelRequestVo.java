package ai.mindslab.ttsmaker.api.pay.vo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayCancelRequestVo {
    private Long payNo;
    private String msg;
}

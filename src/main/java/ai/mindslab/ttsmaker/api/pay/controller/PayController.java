package ai.mindslab.ttsmaker.api.pay.controller;

import ai.mindslab.ttsmaker.api.pay.service.PayService;
import ai.mindslab.ttsmaker.api.pay.vo.BillingFormVo;
import ai.mindslab.ttsmaker.api.pay.vo.ActiveBillingVo;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {
    final PayService payService;

    private static Logger logger = LoggerFactory.getLogger(PayController.class.getName());


    public PayController(PayService payService) {
        this.payService = payService;
    }

    @GetMapping("/billingForm")
    public ResponseEntity<BillingFormVo> basicBilling(@AvaUserParam AvaUser avaUser,
                                                      @RequestParam String planNo) throws Exception {
        logger.info("[Basic Billing] : {} {}", avaUser.getUserNo(), planNo);
        return payService.basicBilling(avaUser, planNo);
    }

    /*
     ** Date: 2019. 07. 12. (By belsnake)
     ** Desc: 요금 정책 변경으로 첫달 무료, 이후 정기 결제로 변경됨.
     */
    @PostMapping(value = "/billingPay")
    public RedirectView billingPay(@RequestParam(value="oid") String oid,
                                   HttpServletRequest request, RedirectAttributes redirectAttr) {
        logger.info("[Billing Pay] : {}", oid);

        RedirectView redirectView = payService.billingPay(oid, request, redirectAttr);
//        RedirectView redirectView = new RedirectView(redirectUrl);
        return redirectView;
    }

    @GetMapping(value = "/close")
    public String close() {
        logger.info("[Close Billing]");

        return "<script type=\"text/javascript\">\n" +
                "    parent.INIStdPay.viewOff();\n" +
                "    location.reload()\n" +
                "</script>";
    }

    @PostMapping(value = "/planCancel")
    public Map<String, Object> planCancel(@AvaUserParam AvaUser avaUser) {
        Long userNo = avaUser.getUserNo();
        logger.info("[Plan Cancel] : {}", userNo);
        return payService.planCancel(userNo);
    }
    
    @GetMapping(value = "/getActiveBilling")
    public ResponseEntity<ActiveBillingVo> getActiveBilling(@AvaUserParam AvaUser avaUser) throws Exception{
        Long userNo = avaUser.getUserNo();
        logger.info("[Get Active Billing] : {}", userNo);
        return payService.getUserActiveBilling(userNo);
    }
}

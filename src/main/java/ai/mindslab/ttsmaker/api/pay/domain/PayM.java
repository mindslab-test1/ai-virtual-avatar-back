package ai.mindslab.ttsmaker.api.pay.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ai.mindslab.ttsmaker.api.pay.enums.PayType;
import ai.mindslab.ttsmaker.api.user.enums.SubscriptionStatus;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.pay.enums.CardCheckFlagEnum;
import lombok.Getter;
import lombok.Setter;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "PAY_M")
public class PayM implements Serializable {
 
	private static final long serialVersionUID = 8167740438751037371L;

	//관리번호
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long payNo;
    
	//구독 플랜 관리번호
    @Column(name = "PLAN_NO")
    private Long planNo;

    //유저 관리번호
    @Column(name = "USER_NO")
    private Long userNo;
    
    //인증 결과 코드
    @Column(name = "AUTH_RESULT_CODE", columnDefinition = "VARCHAR(15)")
    private String authResultCode;
    
    //인증 결과 메세지
    @Column(name = "AUTH_RESULT_MSG", columnDefinition = "VARCHAR(150)")
    private String authResultMsg;
    
    //승인 결과 코드
    @Column(name = "APPL_RESULT_CODE", columnDefinition = "VARCHAR(15)")
    private String applResultCode;
    
    //승인 결과 메세지
    @Column(name = "APPL_RESULT_MSG", columnDefinition = "VARCHAR(150)")
    private String applResultMsg;
    
    //상점 아이디 (Merchant ID)
    @Column(name = "MID", columnDefinition = "VARCHAR(10)")
    private String mid;
    
    //거래번호 (Transaction ID)
    @Column(name = "TID", columnDefinition = "VARCHAR(60)")
    private String tid;
    
    //상품명칭
    @Column(name = "GOOD_NAME", columnDefinition = "VARCHAR(60)")
    private String goodName;
    
    //이벤트 코드
    @Column(name = "EVENT_CODE", columnDefinition = "VARCHAR(200)")
    private String eventCode;
    
    //거래 금액
    @Column(name = "TOT_PRICE", columnDefinition = "VARCHAR(30)")
    private String totPrice;
    
    //주문번호 (결제 요청시 oid 필드에 설정된 값)
    @Column(name = "MOID", columnDefinition = "VARCHAR(60)")
    private String moid;
    
    //지불수단
    @Column(name = "PAY_METHOD", columnDefinition = "VARCHAR(30)")
    private String payMethod;
    
    //승인번호 (결제수단에 따라 미전송)
    @Column(name = "APPL_NUM", columnDefinition = "VARCHAR(12)")
    private String applNum;
    
    //승인일자 (YYYYMMDD)
    @Column(name = "APPL_DATE", columnDefinition = "VARCHAR(12)")
    private String applDate;
    
    //승인시간 [hh24miss]
    @Column(name = "APPL_TIME", columnDefinition = "VARCHAR(12)")
    private String applTime;
    
    //구매자 Email
    @Column(name = "BUYER_EMAIL", columnDefinition = "VARCHAR(150)")
    private String buyerEmail;
    
    //최종 Email
    @Column(name = "CUST_EMAIL", columnDefinition = "VARCHAR(150)")
    private String custEmail;
    
    //구매자 휴대폰번호
    @Column(name = "BUYER_TEL", columnDefinition = "VARCHAR(20)")
    private String buyerTel;
    
    //구매자명
    @Column(name = "BUYER_NAME", columnDefinition = "VARCHAR(45)")
    private String buyerName;
    
    //신용카드번호
    @Column(name = "CARD_NUM", columnDefinition = "VARCHAR(30)")
    private String cardNum;
    
    //"1": 무이자할부
    @Column(name = "CARD_INTEREST", columnDefinition = "VARCHAR(10)")
    private String cardInterest;
    
    //카드 할부기간
    @Column(name = "CARD_QUOTA", columnDefinition = "VARCHAR(10)")
    private String cardQuota;
    
    //카드사 코드
    @Column(name = "CARD_APPL_CODE", columnDefinition = "VARCHAR(10)")
    private String cardApplCode;
    
    //카드 구분 ["0":신용 , "1":체크, "2":기프트]
    @Column(name = "CARD_CHECK_FLAG", columnDefinition = "VARCHAR(10)")
    private String cardCheckFlag;
    
    //부분취소 가능여부 ["1":가능 , "0":불가능]
    @Column(name = "CARD_PRTC_CODE", columnDefinition = "VARCHAR(10)")
    private String cardPrtcCode;
    
    //카드발급사(은행) 코드
    @Column(name = "CARD_BANK_CODE", columnDefinition = "VARCHAR(10)")
    private String cardBankCode;
    
    //간편(앱)결제구분
    @Column(name = "CARD_SRC_CODE", columnDefinition = "VARCHAR(10)")
    private String cardSrcCode;
    
    //카드포인트 사용여부 ["":카드 포인트 사용안함, "1":카드 포인트 사용]
    @Column(name = "CARD_POINT", columnDefinition = "VARCHAR(10)")
    private String cardPoint;
    
    //통화코드
    @Column(name = "CURRENCY", columnDefinition = "VARCHAR(10)")
    private String currency;
    
    //달러 환전금액
    @Column(name = "ORIG_PRICE", columnDefinition = "VARCHAR(20)")
    private String origPrice;
    
    //신용카드 빌링키
    @Column(name = "CARD_BILLKEY", columnDefinition = "VARCHAR(60)")
    private String cardBillkey;

    //결제 결과 코드
    @Column(name = "PAYL_RESULT_CODE", columnDefinition = "VARCHAR(15)")
    private String payResultCode;

    //결제 결과 메세지
    @Column(name = "PAY_RESULT_MSG", columnDefinition = "VARCHAR(150)")
    private String payResultMsg;

    //거래번호 (Transaction ID)
    @Column(name = "PAY_TID", columnDefinition = "VARCHAR(60)")
    private String payTid;
    
    //결제 취소 결과 코드
    @Column(name = "CANCEL_RESULT_CODE", columnDefinition = "VARCHAR(15)")
    private String cancelResultCode;
    
    //결제 취소 결과 메세지
    @Column(name = "CANCEL_RESULT_MSG", columnDefinition = "VARCHAR(150)")
    private String cancelResultMsg;
    
    //결제 취소 일자
    @Column(name = "CANCEL_DATE", columnDefinition = "VARCHAR(15)")
    private String cancelDate;
    
    //결제 취소 시간
    @Column(name = "CANCEL_TIME", columnDefinition = "VARCHAR(15)")
    private String cancelTime;
    
    //결제 부분취소 결과 코드
    @Column(name = "PRTC_RESULT_CODE", columnDefinition = "VARCHAR(15)")
    private String prtcResultCode;
    
    //결제 부분취소 결과 메세지
    @Column(name = "PRTC_RESULT_MSG", columnDefinition = "VARCHAR(150)")
    private String prtcResultMsg;
    
    //결제 부분취소 tid (결제 tid와 다름)
    @Column(name = "PRTC_TID", columnDefinition = "VARCHAR(60)")
    private String prtcTid;
    
    //결제 부분취소 금액
    @Column(name = "PRTC_PRICE", columnDefinition = "VARCHAR(20)")
    private String prtcPrice;
    
    //결제 부분취소 후 남은 금액
    @Column(name = "PRTC_REMAINS", columnDefinition = "VARCHAR(20)")
    private String prtcRemains;
    
    //결제 부분취소 요청 횟수
    @Column(name = "PRTC_CNT", columnDefinition = "VARCHAR(4)")
    private String prtcCnt;
    
    //결제 부분취소 구분 (0: 재승인방식, 1: 부분취소)
    @Column(name = "PRTC_TYPE", columnDefinition = "VARCHAR(4)")
    private String prtcType;
    
    //결제 부분취소 일자
    @Column(name = "PRTC_DATE", columnDefinition = "VARCHAR(15)")
    private String prtcDate;
    
    //결제 부분취소 시간
    @Column(name = "PRTC_TIME", columnDefinition = "VARCHAR(15)")
    private String prtcTime;
    
    //취소 사유 코드
    @Column(name = "CANCEL_REASON", columnDefinition = "VARCHAR(10)")
    private String cancelReason;
    
    //취소 사유 상세설명란
    @Column(name = "CANCEL_REASON_ETC", columnDefinition = "VARCHAR(255)")
    private String cancelReasonEtc;
    
    //등록일시
    @Column(name = "CREATED_AT", updatable = false)
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "PAY_TYPE")
    private PayType payType;
}

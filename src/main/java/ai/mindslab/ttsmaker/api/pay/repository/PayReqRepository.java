package ai.mindslab.ttsmaker.api.pay.repository;

import ai.mindslab.ttsmaker.api.pay.domain.PayReq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PayReqRepository extends JpaRepository<PayReq, Long> {
    @Query("select p from PayReq p where p.planNo = ?1")
    PayReq findPayReqByPlanNo(Long planNo);

    @Query("select p from PayReq p where p.OID = ?1")
    PayReq findPayReqByOid(String oid);
}

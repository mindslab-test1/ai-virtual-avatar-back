package ai.mindslab.ttsmaker.api.pay.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "BILLING")
public class Billing implements Serializable {
	
	private static final long serialVersionUID = -860476531669638777L;

	//관리번호
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

	//활성화 여부
    @Column(name = "ACTIVE", columnDefinition = "boolean default false")
    private Boolean active;

    //카드 빌링키
    @Column(name = "BILL_KEY", columnDefinition = "varchar(200)")
    private String billKey;

    //PAY_M NO
    @Column(name = "PAY_NO")
    private Long payNo;

    //실패사유 상세란
    @Column(name = "FAIL_REASON", columnDefinition = "varchar(255)")
    private String failReason;

    //카드번호
    @Column(name = "CARD_NUM", columnDefinition = "varchar(30)")
    private String cardNum;

    //결제수단
    @Column(name = "PAYMENT", columnDefinition = "varchar(50)")
    private String payment;

    //결제일시
    @Column(name = "PAYMENT_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime paymentDate;

    //카드사 코드
    @Column(name = "CARD_APPL_CODE", columnDefinition = "varchar(10)")
    private String cardApplCode;
    
    //구독 플랜 관리번호
    @Column(name = "PLAN_NO")
    private Long planNo;

    //유저 관리 번호
    @Column(name = "USER_NO")
    private Long userNo;
   
    //등록일시
    @Column(name = "CREATED_AT", updatable = false)
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //등록자
    @Column(name = "CREATED_BY")
    private Long createdBy;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;

    //수정자
    @Column(name = "UPDATED_BY")
    private Long updatedBy;
}

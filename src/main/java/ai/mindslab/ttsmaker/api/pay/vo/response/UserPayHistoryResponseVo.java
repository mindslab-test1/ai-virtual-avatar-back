package ai.mindslab.ttsmaker.api.pay.vo.response;

import ai.mindslab.ttsmaker.api.pay.domain.PayM;
import ai.mindslab.ttsmaker.api.pay.enums.PayType;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserPayHistoryResponseVo {
    Long payNo;
    String planNm;
    String price;
    String cardCompany;
    String cardNo;
    String applDate;
    String payType;
    String cancelDate;
    // TODO: 비고

    public static UserPayHistoryResponseVo of(PayM entity, String planNm, String cardCompany) {
        String applDate = entity.getApplDate();
        String cancelDate = entity.getCancelDate();

        applDate = applDate != null ? DateUtils.parseLocalDate(entity.getApplDate(), "yyyyMMdd").toString() : "";
        cancelDate = cancelDate != null ? DateUtils.parseLocalDate(entity.getCancelDate(), "yyyyMMdd").toString() : "";

        PayType payType = entity.getPayType();
        String payTypeStr = "-";
        if(payType != null){
            payTypeStr = payType.getDesc();
        }


        return UserPayHistoryResponseVo.builder()
                .payNo(entity.getPayNo())
                .planNm(planNm)
                .price(entity.getTotPrice())
                .cardCompany(cardCompany)
                .cardNo(entity.getCardNum())
                .applDate(applDate)
                .payType(payTypeStr)
                .cancelDate(cancelDate)
                .build();
    }
}

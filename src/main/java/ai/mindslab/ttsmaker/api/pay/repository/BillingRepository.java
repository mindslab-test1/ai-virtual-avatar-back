package ai.mindslab.ttsmaker.api.pay.repository;

import ai.mindslab.ttsmaker.api.pay.domain.Billing;
import ai.mindslab.ttsmaker.api.pay.dto.BillingDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface BillingRepository extends JpaRepository<Billing, Long> {


    @Query(value = "insert into Billing (ACTIVE, BILL_KEY, PAY_NO, FAIL_REASON, CARD_NUM, " +
            "PAYMENT, PAYMENT_DATE, CARD_APPL_CODE, PLAN_NO, USER_NO, " +
            "CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY) " +
            "values (1, ?1, ?3, ?4, ?5, " +
            "?6, ?7, ?8, ?9, ?2," +
            "?10, ?2, null, null)", nativeQuery = true)
    void insertBill(String billKey, Long userNo, Long payNo, String failReason, String cardNum,
                    String payMethod, LocalDateTime payDate, String cardApplCode, Long planNo, LocalDateTime createDate);




    @Transactional
    @Modifying
    @Query("Update Billing b set b.paymentDate=?2 Where b.userNo = ?1")
    int updatePaymentDate(Long userNo, LocalDateTime paymentDate);

    @Query("select new ai.mindslab.ttsmaker.api.pay.dto.BillingDto(b.no, u.userNo, b.billKey, u.userNm, u.userId, b.planNo , p.planPrice, b.paymentDate, b.payNo) " +
            " FROM Billing b inner join UserM u on b.userNo = u.userNo" +
            " join Plan p ON b.planNo = p.planNo where b.active = true " +
            "and b.paymentDate <= ?1")
    Collection<BillingDto> getRoutineBilling(LocalDateTime currDate);


    @Query("select b.payment from Billing b Where b.payNo = ?1")
    String getBillingPaymentMethod(Long payNo);
    
    @Query("select b from Billing b where b.payNo = ?1")
    Billing getBillingByPayNo(Long payNo);

    @Transactional
    @Modifying
    @Query("update Billing b set b.billKey = ?1 ,b.paymentDate = ?2, b.updatedAt = ?3, " +
            "b.failReason = ?5, b.payNo = ?6 where b.no = ?4 ")
    void updateSuccessBillingInfo(String billKey, LocalDateTime paymentDate, LocalDateTime updatedAt,
                           Long billingNo, String failReason, Long payNo);

    @Transactional
    @Modifying
    @Query("update Billing b set b.billKey = ?1 ,b.paymentDate = ?2, b.updatedAt = ?3, " +
            "b.failReason = ?5, b.payNo = ?6 where b.no = ?4 ")
    void updateFailedBillingInfo(String failReason, Long billingNo);

    @Query("SELECT b FROM Billing b WHERE b.userNo = ?1 AND b.active = true")
    Billing getActiveBillingByUserNo(Long userNo);

    @Transactional
    @Modifying
    @Query("update Billing b set b.active = 0, b.updatedAt = ?2, b.updatedBy = ?1 where b.userNo = ?1")
    void deactivateBillingByUserNo(Long userNo, LocalDateTime updatedAt);

    @Query("SELECT b FROM Billing b WHERE b.active = false")
    List<Billing> getInactiveBillings();

    @Query(value ="select * from Billing b where b.active = 1 AND DATEDIFF(NOW(),  b.PAYMENT_DATE ) >= 5", nativeQuery = true)
    List<Billing> getUnpaidBilling();

    int deleteBillingByNo(Long no);

    @Query("SELECT b FROM Billing b WHERE b.active = false AND b.userNo = ?1")
    List<Billing> getInactiveBillingByUserNo(Long userNo);
    
    @Query("SELECT b FROM Billing b WHERE b.active = false AND b.userNo = ?1 ORDER BY b.no DESC")
    List<Billing> getInactiveBillingByUserNoDesc(Long userNo);
}

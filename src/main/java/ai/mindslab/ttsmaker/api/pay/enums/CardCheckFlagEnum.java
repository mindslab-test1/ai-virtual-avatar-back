package ai.mindslab.ttsmaker.api.pay.enums;

public enum CardCheckFlagEnum {
    CREDIT,  // 신용카드 
    CHECK,   // 체크카드
    GIFT	// 기프트카드
}

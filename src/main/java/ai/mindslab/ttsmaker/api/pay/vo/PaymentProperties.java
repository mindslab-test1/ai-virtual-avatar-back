package ai.mindslab.ttsmaker.api.pay.vo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ava.payment")
public class PaymentProperties {
	private String inipayHome;
	private String mid;
	private String signKey;
	private String iniApiKey;
	private String charset;
	private String format;
	private String paymentUrl;
	private String redirectUrl;
}

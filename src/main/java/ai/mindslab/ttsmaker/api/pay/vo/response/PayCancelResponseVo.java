package ai.mindslab.ttsmaker.api.pay.vo.response;

import lombok.Data;

@Data
public class PayCancelResponseVo {
    private String resultCode;
    private String resultMsg;
    private String cancelDate;
    private String cancelTime;
    private String receiptInfo;
    private String cshrCancelNum;
}
package ai.mindslab.ttsmaker.api.pay.vo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayCancelPartialRequestVo {
    private String tid;
    private String msg;
    private String cancelPrice;
    private String priceAfterCancel;
}

package ai.mindslab.ttsmaker.api.pay.vo.response;

import lombok.Data;

@Data
public class PayCancelPartialResponseVo {
    private String resultCode;
    private String resultMsg;
    private String tid;
    private String prtcTid;
    private String prtcRemains;
    private String prtcPrice;
    private String prtcType;
    private String prtcCnt;
    private String prtcDate;
    private String prtcTime;
    private String receiptInfo;
}

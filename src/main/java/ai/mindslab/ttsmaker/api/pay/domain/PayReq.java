package ai.mindslab.ttsmaker.api.pay.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Builder
@Entity
@Table(name = "PAY_REQ")
public class PayReq implements Serializable {
    
    private static final long serialVersionUID = -3322460890471964291L;
    
    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private long id;
    
    //주문번호 (Order ID)
    @Column(name = "OID", columnDefinition = "varchar(60)")
    private String OID;

    //구독 플랜 명칭
    @Column(name = "PLAN_NM", columnDefinition = "varchar(100)")
    private String planName;

    //구독 플랜 관리번호
    @Column(name = "PLAN_NO")
    private Long planNo;

    //구독 플랜 가격
    @Column(name = "PLAN_PRICE")
    private Integer planPrice;

    //유저 아이디 (이메일주소)
    @Column(name = "USER_ID", columnDefinition = "varchar(150)")
    private String userId;

    //유저 관리번호
    @Column(name = "USER_NO")
    private Long userNo;

//    //결제 관리번호
//    @Column(name = "PAY_NO")
//    private Long payNo;
//
    //등록일시
    @Column(name = "CREATED_AT", updatable = false)
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
}

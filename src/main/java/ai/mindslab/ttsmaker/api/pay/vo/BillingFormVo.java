package ai.mindslab.ttsmaker.api.pay.vo;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillingFormVo {
    private String mid;
    private String timestamp;
    private String oid;
    private Long planNo;
    private Integer planPrice;
    private String planNm;
    private String mKey;
    private String billingSignature;
    private String siteDomain;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planStartAt;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planEndAt;
    
    private String method;
    
    private Long userNo;
    private String userId;
    private String userNm;
    private String userCellPhone;

}

package ai.mindslab.ttsmaker.api.pay.dto;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Value
public class BillingDto {
    Long billingNo;
    Long userNo;                // 회원번호
    String billingKey;        // 빌링키
    String userName;        // 회원이름
    String userEmail;        // 회원이메일
    Long goodCode;            // 상품 정보 코드
    Integer price;                // 가격
    LocalDateTime paymentDate;
    Long payNo;
}


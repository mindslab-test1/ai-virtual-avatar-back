package ai.mindslab.ttsmaker.api.pay.repository;

import ai.mindslab.ttsmaker.api.pay.domain.PayM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PayMRepository extends JpaRepository<PayM, Long> {
    Optional<PayM> findByPayTid(String payTid);

    List<PayM> findByUserNoOrderByPayNoDesc(Long userNo);

    Optional<PayM> findByPayNo(Long payNo);
}

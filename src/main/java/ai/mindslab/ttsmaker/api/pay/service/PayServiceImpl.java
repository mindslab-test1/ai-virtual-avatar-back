package ai.mindslab.ttsmaker.api.pay.service;

import ai.mindslab.ttsmaker.api.code.domain.CodeDetail;
import ai.mindslab.ttsmaker.api.code.repository.CodeDetailRepository;
import ai.mindslab.ttsmaker.api.pay.domain.Billing;
import ai.mindslab.ttsmaker.api.pay.domain.PayM;
import ai.mindslab.ttsmaker.api.pay.domain.PayReq;
import ai.mindslab.ttsmaker.api.pay.dto.BillingDto;
import ai.mindslab.ttsmaker.api.pay.enums.PayType;
import ai.mindslab.ttsmaker.api.pay.repository.BillingRepository;
import ai.mindslab.ttsmaker.api.pay.repository.PayMRepository;
import ai.mindslab.ttsmaker.api.pay.repository.PayReqRepository;
import ai.mindslab.ttsmaker.api.pay.vo.ActiveBillingVo;
import ai.mindslab.ttsmaker.api.pay.vo.BillingFormVo;
import ai.mindslab.ttsmaker.api.pay.vo.PaymentProperties;
import ai.mindslab.ttsmaker.api.pay.vo.response.PayCancelPartialResponseVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.PayCancelResponseVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.UserPayHistoryResponseVo;
import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.plan.repository.PlanRepository;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserRole;
import ai.mindslab.ttsmaker.api.user.domain.UserTimeline;
import ai.mindslab.ttsmaker.api.user.enums.SubscriptionStatus;
import ai.mindslab.ttsmaker.api.user.enums.UserRoleEnum;
import ai.mindslab.ttsmaker.api.user.repository.UserMRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserMRoleRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserPlanRepository;
import ai.mindslab.ttsmaker.api.user.service.UserService;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.util.DateUtils;
import ai.mindslab.ttsmaker.util.StringUtils;
import com.inicis.inipay4.INIpay;
import com.inicis.inipay4.util.INIdata;
import com.inicis.std.util.HttpUtil;
import com.inicis.std.util.ParseUtil;
import com.inicis.std.util.SignatureUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.net.Inet4Address;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.now;

@Slf4j
@Service
public class PayServiceImpl implements PayService {

    private static Logger logger = LoggerFactory.getLogger(PayService.class.getName());


    @Autowired
    private PaymentProperties paymentProperties;

    @Autowired
    private UserMRoleRepository userMRoleRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PayReqRepository payReqRepository;

    @Autowired
    private UserPlanRepository userPlanRepository;

    @Autowired
    private UserMRepository userMRepository;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private PayMRepository payMRepository;

    @Autowired
    private CodeDetailRepository codeDetailRepository;

    @Autowired
    private UserService userService;


    @Override
    public ResponseEntity<BillingFormVo> basicBilling(AvaUser avaUser, String planNoStr) throws Exception {

        Long userNo = avaUser.getUserNo();
        Long planNo = StringUtils.toLong(planNoStr);

        log.info("1. 유저 및 플랜 정보 조회. userNo = {}, planNo = {}", userNo, planNo);
        UserM userM = userMRepository.findByUserNoWithoutUserRoles(userNo);
        if (userM == null) {
            throw new EntityNotFoundException("유저 정보가 존재하지 않습니다.");
        }
        log.info("1-2. 플랜 정보 조회 요청");
        Plan plan = planRepository.findPlanByPlanNo(planNo);
        if (planNo == 0 || plan == null) {
            throw new EntityNotFoundException("플랜 정보가 존재하지 않습니다.");
        }
        log.info("1-3. 플랜 정보 조회 요청완료 plan = {}", plan);


        log.info("1-4. 결제 BillingFormVo 데이터 가공 요청");
        BillingFormVo billingFormVo = makeInipayBillingForm(userM, plan, paymentProperties);
        log.info("1-5. 결제 BillingFormVo 데이터 가공 요청 완료 = {}", billingFormVo);

        String oid = billingFormVo.getOid();
        logger.info("OID[{}] >> 2-1. PayReq 정보 저장", oid);
        PayReq payReq = PayReq.builder()
                .OID(billingFormVo.getOid())
                .planName(billingFormVo.getPlanNm())
                .planNo(billingFormVo.getPlanNo())
                .planPrice(billingFormVo.getPlanPrice())
                .userId(billingFormVo.getUserId())
                .userNo(billingFormVo.getUserNo())
                .build();
        logger.info("OID[{}] >> 2-2. PayReq 정보 payReq = {}", oid, payReq);

        payReqRepository.save(payReq);
        logger.info("OID[{}] >> 2-3. PayReq 정보 저장 완료.", oid, payReq);

        return ResponseEntity.ok(billingFormVo);
    }

    private BillingFormVo makeInipayBillingForm(UserM userM, Plan plan, PaymentProperties paymentProperties) throws Exception {
        Long userNo = userM.getUserNo();
        String userId = userM.getUserId();
        String userNm = userM.getUserNm();
        String userCellPhone = userM.getCellPhone();

        Long planNo = plan.getPlanNo();
        String planNm = plan.getPlanNm();
        Integer planPrice = plan.getPlanPrice();

        String timestamp = SignatureUtil.getTimestamp();
        String mid = paymentProperties.getMid();
        String oid = mid + "_" + userNo + "_" + timestamp;

        logger.info("OID[{}] >> 플랜 이름 = {}, 플랜 가격 = {}", oid, planNm, planPrice);

        String mKey = SignatureUtil.hash(paymentProperties.getSignKey(), "SHA-256");

        // 결제 제공기간 설정 (오늘 ~ 오늘 + 30일)
        // 계약 시작일시, 계약 종료일시 
        LocalDateTime planStartAt = now();
        LocalDateTime planEndAt = planStartAt.plusMonths(1);

        Map<String, String> billingSignParam = new HashMap<String, String>();
        billingSignParam.put("oid", oid);
        billingSignParam.put("price", planPrice.toString());
        billingSignParam.put("timestamp", timestamp);

        String billingSignature = SignatureUtil.makeSignature(billingSignParam);
        log.info("OID[{}] >> billingSignature = {}", oid, billingSignature);


        BillingFormVo vo = BillingFormVo.builder()
                .mid(mid)
                .timestamp(timestamp)
                .oid(oid)
                .planNo(planNo)
                .planPrice(planPrice)
                .planNm(planNm)
                .mKey(mKey)
                .billingSignature(billingSignature)
                .siteDomain(paymentProperties.getPaymentUrl())
                .planStartAt(planStartAt)
                .planEndAt(planEndAt)
                .method("billingPay")
                .userNo(userNo)
                .userId(userId)
                .userNm(userNm)
                .userCellPhone(userCellPhone)
                .build();
        log.info("OID[{}] >> BillingFormVo = {}", oid, vo);

        return vo;
    }

    @Override
    public RedirectView billingPay(String oid, HttpServletRequest request, RedirectAttributes redirectAttr) {
        Map<String, String> resultMap = new HashMap<>();

        PayReq payReq = new PayReq();


        try {
            //#############################
            // 인증결과 파라미터 일괄 수신
            //#############################
            request.setCharacterEncoding("UTF-8");

            try {
                payReq = payReqRepository.findPayReqByOid(oid);
                log.info("OID[{}] >> PayReq 조회 성공. payReq = {} ", oid, payReq);

            } catch (NullPointerException e) {
                log.error("OID[{}] >> PayReq 조회 실패. {}", oid, e);
            }

            Long userNo = payReq.getUserNo();
            Long planNo = payReq.getPlanNo();

            //2. 이니시스 전송결과 파라미터 가공(request)
            log.info("(userNo = {}, oid = {}) >> 2. 이니시스 전송결과 파라미터 가공(request) ", userNo, oid);
            Map<String, String> paramMap = makeIniPayRequest(request, userNo, oid);

            // 3. 인증결과 코드 및 메세지 저장
            log.info("OID[{}] >> 3. 인증결과 코드 및 메세지 저장 ", oid);

            PayM payM = new PayM();
            payM.setPlanNo(planNo);
            payM.setUserNo(userNo);
            payM.setAuthResultCode(paramMap.get("resultCode"));
            payM.setAuthResultMsg(paramMap.get("resultMsg"));
            payM = payMRepository.save(payM);

            // 인증 성공 여부 
            boolean authResultFlg = "0000".equals(paramMap.get("resultCode"));

            log.info("OID[{}] >> PayM.AuthResultCode = {}", oid, paramMap.get("resultCode"));
            log.info("OID[{}] >> PayM.AuthResultMsg = {}", oid, paramMap.get("resultMsg"));
            log.info("OID[{}] >> payM.getPayNo = {}", oid, payM.getPayNo());
            log.info("OID[{}] >> 인증 결과 :  {}", oid, authResultFlg);

            // 4. 인증이 성공일 경우만
            log.info("OID[{}] >> 4. 인증 유효성 검사", oid);
            if (authResultFlg) {
                log.info("OID[{}] >> 4-1. 인증 결과 성공!", oid);

                String timestamp = SignatureUtil.getTimestamp();                // util에 의해서 자동생성

                String authUrl = paramMap.get("authUrl");                    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                String netCancel = paramMap.get("netCancelUrl");                // 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                String ackUrl = paramMap.get("checkAckUrl");                // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
                String merchantData = paramMap.get("merchantData");            // 가맹점 관리데이터 수신

                log.info("OID[{}] >> authUrl = {}", oid, authUrl);
                log.info("OID[{}] >> netCancel = {}", oid, netCancel);
                log.info("OID[{}] >> ackUrl = {}", oid, ackUrl);
                log.info("OID[{}] >> merchantData = {}", oid, merchantData);

                HttpUtil httpUtil = new HttpUtil();

                try {
                    log.info("OID[{}] >> 4-2. 인증 가공 파라미터 가공", oid);
                    Map<String, String> authMap = makeAuthParamMap(paramMap, timestamp);
                    log.info("OID[{}] >> 4-3. 인증 가공 파라미터 MAP(authMap) : {}", oid, authMap);
                    log.info("OID[{}] >> 4-4. 승인요청 API 호출", oid);
                    String authResultString = httpUtil.processHTTP(authMap, authUrl);
                    log.info("OID[{}] >> 4-5. API 통신결과 authResultString = {}", oid, authResultString);

                    String test = authResultString.replace(",", "&").replace(":", "=").replace("\"", "").replace(" ", "").replace("\n", "").replace("}", "").replace("{", "");
                    resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱
                    log.info("OID[{}] >> 4-6. 인증 결과 MAP(resultMap) : {}", oid, resultMap);

                    // signature 데이터 생성
                    String secureSignature = makeSecureSignatureString(resultMap, timestamp);
                    log.info("OID[{}] >> 4-7. signature 데이터 생성 : {}", oid, secureSignature);


                    // 5. 결제 인증 결과 저장
                    log.info("OID[{}] >> 4-8. 인증 결과 저장 resultMap = {}", oid, resultMap);
                    PayM payResult = payMEntityData(resultMap, payM);

                    if (!secureSignature.equals(resultMap.get("authSignature"))) {
                        log.error("OID[{}] >> 4-9. 이니시스 시그니쳐 정보 불일치");
                    }

                    // 정상 인증 일경우 
                    if ("0000".equals(resultMap.get("resultCode"))) {
                        /*****************************************************************************
                         * 여기에 가맹점 내부 DB에 결제 결과를 반영하는 관련 프로그램 코드를 구현한다.

                         [중요!] 승인내용에 이상이 없음을 확인한 뒤 가맹점 DB에 해당건이 정상처리 되었음을 반영함
                         처리중 에러 발생시 망취소를 한다.
                         ******************************************************************************/

                        log.info("OID[{}] >> 5. 승인 요청", oid);

                        log.info("OID[{}] >> 5-1. 승인 요청 파라미터 가공", oid);
                        INIdata applData = makeApplINIdata(request, resultMap);
                        log.info("OID[{}] >> 5-2. 승인 요청 파라미터 INIdata applData = {}", oid, applData);
                        INIpay inipay = new INIpay();
                        log.info("OID[{}] >> 5-3. 승인 요청 inipay.payRequest() before ", oid);
                        INIdata resultData = inipay.payRequest(applData);
                        log.info("OID[{}] >> 5-4. 승인 요청 inipay.payRequest() after", oid);
                        log.info("OID[{}] >> 5-5. TID = {},  ResultCode = {}, ResultDetailMsg = {}", oid, resultData.getData("tid"), resultData.getData("ResultCode"), resultData.getData("ResultDetailMsg"));

                        log.info("OID[{}] >> 7. 승인 결과값 저장", oid);
                        payResult.setPayResultCode(resultData.getData("ResultCode"));
                        payResult.setPayResultMsg(resultData.getData("ResultDetailMsg"));
                        payResult.setPayMethod(resultData.getData("paymethod"));
                        payResult.setPayTid(resultData.getData("tid"));
                        payMRepository.save(payResult);
                        log.info("OID[{}] >> 7-1. 승인 결과값 저장 성공. TID = {}", oid, resultData.getData("tid"));

                        LocalDateTime ldtPayDate = now().plusMonths(1);
                        Date date = DateUtils.asDate(ldtPayDate);
                        LocalDateTime ldtDateFrom = now();

                        if (resultData.getData("ResultCode").equals("00")) {
                            log.info("OID[{}] >> Billing/UserPlan/UserRole 정보 저장.", oid);

                            handlePayReqSuccess(oid, payResult, ldtPayDate, ldtDateFrom, payResult.getPayTid());

                            // 결제 성공 시 이동 할 url
                            RedirectView redirectView = new RedirectView(paymentProperties.getRedirectUrl() + "/");

                            return redirectView;
                        } else {

                            logger.info("payM : " + payResult.toString());
                            payResult.setPayType(PayType.FAIL);
                            payMRepository.save(payResult);

                            String reason = resultData.getData("ResultDetailMsg");

                            StringTokenizer st = new StringTokenizer(reason, "|");
                            st.nextToken();

                            RedirectView redirectView = new RedirectView(paymentProperties.getRedirectUrl() + "/");

                            return redirectView;
                        }

                        //////////////////////////////////
                    } else {
                        payResult.setPayType(PayType.FAIL);
                        payMRepository.save(payResult);

                        //결제보안키가 다른 경우
                        if (!secureSignature.equals(resultMap.get("authSignature")) && "0000".equals(resultMap.get("resultCode"))) {

                            //auth signature 가 어떤 상황(사용자의 입력 정보 오류 or 시그니처 변경)에 의해 달라졌을 경우, 망취소
                            if ("0000".equals(resultMap.get("resultCode"))) {
                                throw new Exception("데이터 위변조 체크 실패");
                            }
                        }
                    }

                    // 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
                    // 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시

                    // payViewType을 popup으로 해서 결제를 하셨을 경우
                    // 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요

                    //throw new Exception("강제 Exception");
                } catch (Exception ex) {

                    //####################################
                    // 실패시 처리(***가맹점 개발수정***)
                    //####################################

                    //---- db 저장 실패시 등 예외처리----//
                    logger.error(ex.getMessage());

                    payM.setPayType(PayType.FAIL);
                    payMRepository.save(payM);
                    //#####################
                    // 망취소 API
                    //#####################
                    //					String netcancelResultString = httpUtil.processHTTP(authMap, netCancel);	// 망취소 요청 API url(고정, 임의 세팅 금지)

                    ex.printStackTrace();
                }

            } else {
                //#############
                // 인증 실패시
                //#############
                logger.info("message : " + resultMap.get("fail"));
                logger.info("moid : " + resultMap.get("MOID"));

                payM.setPayType(PayType.FAIL);
                payMRepository.save(payM);

                RedirectView redirectView = new RedirectView(paymentProperties.getRedirectUrl() + "/");
                redirectAttr.addFlashAttribute("reason", resultMap.get("resultMsg"));
                redirectAttr.addFlashAttribute("moid", resultMap.get("MOID"));

                logger.info(resultMap.toString());

                return redirectView;
                //return "redirect:/support/krPricingFail";

            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("message : " + resultMap.get("resultMsg"));
        logger.info("moid : " + resultMap.get("MOID"));

        RedirectView redirectView = new RedirectView(paymentProperties.getRedirectUrl() + "/");
        redirectAttr.addAttribute("reason", resultMap.get("resultMsg"));
        redirectAttr.addAttribute("moid", resultMap.get("MOID").replace("mindslab02_", ""));
        logger.info(resultMap.toString());

        return redirectView;
    }

    private INIdata makeApplINIdata(HttpServletRequest request, Map<String, String> resultMap) {
        INIdata data = new INIdata();
        data.setData("inipayHome", paymentProperties.getInipayHome());         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
        data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
        data.setData("type", "reqrealbill");            // type (고정)
        data.setData("paymethod", "Card");              // 지불수단 (고정)
        data.setData("crypto", "execure");              // 암복호화모듈 (고정)

        data.setData("uip", this.getUip(request));
        data.setData("url", paymentProperties.getPaymentUrl());
        data.setData("mid", paymentProperties.getMid());
        data.setData("uid", paymentProperties.getMid());
        data.setData("keyPW", "1111");

        data.setData("moid", resultMap.get("MOID"));
        data.setData("goodname", resultMap.get("goodsName"));
        data.setData("price", resultMap.get("TotPrice"));
        data.setData("currency", "WON");

        data.setData("buyername", resultMap.get("buyerName"));
        data.setData("buyertel", resultMap.get("buyerTel"));
        data.setData("buyeremail", resultMap.get("buyerEmail"));

        data.setData("cardquota", resultMap.get("CARD_Quota"));
        data.setData("quotaInterest", "0");
        data.setData("billkey", resultMap.get("CARD_BillKey"));
        data.setData("authentification", "00");

        return data;
    }

    private INIdata makeRoutineBillINIdata(String uip, String moid, String goodname, String price, String currency,
                                           String buyerName, String buyerTel, String buyerEmail, String cardQuota,
                                           String quotaInterest, String billKey, String authentification) {
        INIdata data = new INIdata();
        data.setData("inipayHome", paymentProperties.getInipayHome());         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
        data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
        data.setData("type", "reqrealbill");            // type (고정)
        data.setData("paymethod", "Card");              // 지불수단 (고정)
        data.setData("crypto", "execure");              // 암복호화모듈 (고정)

        data.setData("uip", uip);
        data.setData("url", paymentProperties.getPaymentUrl());
        data.setData("mid", paymentProperties.getMid());
        data.setData("uid", paymentProperties.getMid());
        data.setData("keyPW", "1111");

        data.setData("moid", moid);
        data.setData("goodname", goodname);
        data.setData("price", price);
        data.setData("currency", currency);

        data.setData("buyername", buyerName);
        data.setData("buyertel", buyerTel);
        data.setData("buyeremail", buyerEmail);

        data.setData("cardquota", cardQuota);
        data.setData("quotaInterest", quotaInterest);
        data.setData("billkey", billKey);
        data.setData("authentification", authentification);
        return data;
    }

    private String makeSecureSignatureString(Map<String, String> resultMap, String timestamp) throws Exception {
        /*************************  결제보안 강화 2016-05-18 START ****************************/
        Map<String, String> secureMap = new HashMap<String, String>();
        secureMap.put("mid", paymentProperties.getMid());                                //mid
        secureMap.put("tstamp", timestamp);                        //timestemp
        secureMap.put("MOID", resultMap.get("MOID"));            //MOID
        secureMap.put("TotPrice", resultMap.get("TotPrice"));        //TotPrice

        // signature 데이터 생성
        return SignatureUtil.makeSignatureAuth(secureMap);
    }


    private Map<String, String> makeAuthParamMap(Map<String, String> paramMap, String timestamp) throws Exception {

        String authToken = paramMap.get("authToken");                // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)

        //#####################
        // 2.signature 생성
        //#####################
        Map<String, String> signParam = new HashMap<String, String>();

        signParam.put("authToken", authToken);        // 필수
        signParam.put("timestamp", timestamp);        // 필수

        // signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
        String signature = SignatureUtil.makeSignature(signParam);

        //#####################
        // 3.API 요청 전문 생성
        //#####################
        Map<String, String> authMap = new Hashtable<String, String>();

        authMap.put("mid", paymentProperties.getMid());            // 필수
        authMap.put("authToken", authToken);    // 필수
        authMap.put("signature", signature);    // 필수
        authMap.put("timestamp", timestamp);    // 필수
        authMap.put("charset", paymentProperties.getCharset());      // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
        authMap.put("format", paymentProperties.getFormat());        // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
        //authMap.put("price" 		,price);		// 가격위변조체크기능 (선택사용)
        return authMap;
    }


    private Map<String, String> makeIniPayRequest(HttpServletRequest request, Long userNo, String oid) {
        Map<String, String> paramMap = new Hashtable<String, String>();

        // 인증결과 파라미터 Map
        Enumeration<String> elems = request.getParameterNames();
        while (elems.hasMoreElements()) {
            String key = (String) elems.nextElement();
            String val = request.getParameter(key);
            paramMap.put(key, val);
            log.info("INIPAY Request PARAM(userNo:{}, oid:{}) key:{} = val:{}", userNo, oid, key, val);
        }

        return paramMap;
    }


    private void handlePayReqSuccess(String oid, PayM payResult, LocalDateTime ldtPayDate, LocalDateTime ldtDateFrom, String tid) {
        // 이전 빌링정보 있을 시, deavtive 후 새로운 빌링정보 생성 작업 진행
        log.info("OID[{}] >> oldBilling 정보 조회. userNo = {}", oid, payResult.getUserNo());
        Billing oldBilling = billingRepository.getActiveBillingByUserNo(payResult.getUserNo());
        log.info("OID[{}] >> oldBilling 정보 조회. oldBilling = {}", oid, oldBilling);

        if (oldBilling != null) {
            log.info("OID[{}] >> oldBilling 비활성화 진행", oid);
            billingRepository.deactivateBillingByUserNo(payResult.getUserNo(), now());
            log.info("OID[{}] >> oldBilling 비활성화 완료", oid);
        }

        payResult.setPayType(PayType.REG);
        payMRepository.save(payResult);

        log.info("OID[{}] >> 8. Billing 정보 Insert", oid);
        Billing billing = new Billing();
        billing.setActive(true);
        billing.setBillKey(payResult.getCardBillkey());
        billing.setPayNo(payResult.getPayNo());
        billing.setCardNum(payResult.getCardNum());
        billing.setPayment(payResult.getPayMethod());
        billing.setPaymentDate(ldtPayDate);
        billing.setCardApplCode(payResult.getCardApplCode());
        billing.setPlanNo(payResult.getPlanNo());
        billing.setUserNo(payResult.getUserNo());
        billing.setCreatedBy(payResult.getUserNo());
        billingRepository.save(billing);

        log.info("OID[{}] >> 8-1. Billing 정보 저장 완료 = {}", oid, billing);

        UserM user = userMRepository.findByUserNo(payResult.getUserNo()).orElseThrow(() -> new EntityNotFoundException());
        Plan plan = planRepository.findById(payResult.getPlanNo()).orElseThrow(() -> new EntityNotFoundException());

        // UserPlan 정보 추가
        log.info("OID[{}] >> 9. UserPlan 정보 Insert", oid);
        UserPlan userPlan = new UserPlan();
        userPlan.setUser(user);
        userPlan.setPlan(plan);
        userPlan.setPayM(payResult);
        userPlan.setCharMaxLimit(plan.getMaxLimit());
        userPlan.setPlanStartAt(ldtDateFrom);
        userPlan.setCharUseCnt(0);
        userPlan.setPlanEndAt(ldtPayDate);
        userPlan.setCreatedBy(payResult.getUserNo());

        List<UserPlan> activeUserPlans = userPlanRepository.findActiveUserPlansByUserNoDesc(user.getUserNo(), true);

        if (!activeUserPlans.isEmpty()) {
            UserPlan prevPlan = activeUserPlans.get(0);
            log.info("OID[{}] >> transferring previous user plan no[{}]", prevPlan.getNo());
            userPlan.setCharTransferred(prevPlan);
        }

        UserPlan userPlanResult = userPlanRepository.save(userPlan);
        log.info("OID[{}] >> 9-1. UserPlan 정보 저장 완료 = {}", oid, userPlan);

        log.info("OID[{}] >> 10. 유저 정보 PLAN_SERVICE_END_AT 업데이트. UserNo = {}", oid, payResult.getUserNo());
        userMRepository.updatePlanServiceEndAt(payResult.getUserNo(), ldtPayDate);
        log.info("OID[{}] >> 10-1. 유저 정보 PLAN_SERVICE_END_AT 업데이트 완료", oid);


        // 다운로드 권한 추가
        if (!userHasDownloadRole(user.getUserNo(), UserRoleEnum.ROLE_DOWNLOAD)) {
            log.info("OID[{}] >> 11. UserRole 정보 Insert", oid);

            UserRole userRole = new UserRole();

            userRole.setUser(user);
            userRole.setRoleNm(UserRoleEnum.ROLE_DOWNLOAD);
            userRole.setCreatedBy(payResult.getUserNo());
            userMRoleRepository.save(userRole);
            log.info("OID[{}] >> 11-1. UserRole 정보 저장 완료. User = {}", oid, user);

        }

        log.info("OID[{}] >> 12. 유저 타임라인 정보 Insert", oid);

        UserTimeline userTimeline = userService.addUserTimeline(user, userPlanResult, "구독 성공", SubscriptionStatus.START, true);

        log.info("OID[{}] >> 12-1. 유저 타임라인 구독 성공 정보 저장 완료. User = {}", oid, userTimeline);

        logger.info("@ Saved Billing & User Information : tid = {}, userNo = {}", tid, payResult.getUserNo());
    }

    private PayM payMEntityData(Map<String, String> resultMap, PayM payM) {
        // 결과코드
        payM.setApplResultCode(resultMap.get("resultCode"));
        // 결과메세지
        payM.setApplResultMsg(resultMap.get("resultMsg"));
        // 거래번호
        payM.setTid(resultMap.get("tid"));
        payM.setGoodName(resultMap.get("goodName"));
        // 이벤트코드 (카드 할부 및 행사 적용 코드)
        payM.setEventCode(resultMap.get("EventCode"));
        // 거래금액
        payM.setTotPrice(resultMap.get("TotPrice"));
        // 주문번호
        payM.setMoid(resultMap.get("MOID"));
        // 지불수단
        payM.setPayMethod(resultMap.get("PayMethod"));
        payM.setApplNum(resultMap.get("CARD_ApplNum"));
        payM.setApplDate(resultMap.get("applDate"));
        payM.setApplTime(resultMap.get("applTime"));
        payM.setBuyerEmail(resultMap.get("buyerEmail"));
        payM.setCustEmail(resultMap.get("custEmail"));
        payM.setBuyerTel(resultMap.get("buyerTel"));
        payM.setBuyerName(resultMap.get("buyerName"));

        payM.setCardNum(resultMap.get("CARD_Num"));
        payM.setCardInterest(resultMap.get("CARD_Interest"));
        payM.setCardQuota(resultMap.get("CARD_Quota"));

        payM.setCardApplCode(resultMap.get("CARD_Code"));

        payM.setCardCheckFlag(resultMap.get("CARD_CheckFlag"));

        payM.setCardPrtcCode(resultMap.get("CARD_PrtcCode"));

        payM.setCardBankCode(resultMap.get("CARD_BankCode"));

        payM.setCardSrcCode(resultMap.get("CARD_SrcCode"));
        payM.setCardPoint(resultMap.get("CARD_Point"));

        payM.setCurrency(resultMap.get("currency"));
        payM.setCardBillkey(resultMap.get("CARD_BillKey"));

        return payMRepository.save(payM);
    }

    @Override
    public String getUip(HttpServletRequest request) {

        String uip = request.getHeader("X-FORWARDED-FOR");

        if (uip == null) {
            uip = request.getHeader("Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (uip == null) {
            uip = request.getRemoteAddr();
        }

        return uip;
    }

    @Override
    public PayCancelResponseVo cancelPay(Long payNo, String msg) {
        logger.info("Cancelling payment for --> payNo[{}], MSG[{}]", payNo, msg);

        PayCancelResponseVo response = new PayCancelResponseVo();

        try {
            //결제 취소하고자 하는 결제정보를 TID로 검색
            log.info("PayNo[{}] >> 결제취소 요청 정보 조회", payNo);

            PayM payM = payMRepository.findByPayNo(payNo).orElseThrow(() -> new EntityNotFoundException());
            String tid = payM.getPayTid();
            log.info("TID[{}] >> 결제취소 요청 정보 조회 결과 payM = {}", tid, payM);

            String type = "Refund";
            String paymethod = "Card";
            SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
            String timestamp = f.format(new Date());
            String clientIp = Inet4Address.getLocalHost().getHostAddress();

            log.info("TID[{}] >> 결제취소 시그니쳐 데이터 생성", tid);
            String hashInput = paymentProperties.getIniApiKey() + type + paymethod
                    + timestamp + clientIp + paymentProperties.getMid() + tid;

            String hashData = SignatureUtil.hash(hashInput, "SHA-512");
            log.info("TID[{}] >> 결제취소 시그니쳐 데이터 생성 완료", tid);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.add("Accept", "*/*");


            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("type", type);
            map.add("paymethod", paymethod);
            map.add("timestamp", timestamp);
            map.add("clientIp", clientIp);
            map.add("mid", paymentProperties.getMid());
            map.add("tid", tid);
            map.add("msg", msg);
            map.add("hashData", hashData);

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            log.info("TID[{}] >> 결제취소 POST 요청. map 정보 = {}", tid, map);

            response = restTemplate.postForObject("https://iniapi.inicis.com/api/v1/refund", entity, PayCancelResponseVo.class);

            log.info("TID[{}] >> 결제취소 결과 정보 저장 {}", tid, response);

            LocalDate parsedDate = DateUtils.parseLocalDate(response.getCancelDate(), "yyyyMMdd");

            if (response.getResultCode().equals("00")) {
                payM.setCancelResultCode(response.getResultCode());
                payM.setCancelResultMsg(response.getResultMsg());
                payM.setCancelDate(parsedDate.toString());
                payM.setCancelTime(response.getCancelTime());
                payM.setPayType(PayType.ADMIN);
                payMRepository.save(payM);
            }

            response.setCancelDate(parsedDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

            log.info("TID[{}] >> 결제취소 결과 정보 저장 완료. resultCode = {}, resultMsg = {}",
                    tid, response.getResultCode(), response.getResultMsg());

        } catch (EntityNotFoundException e) {
            logger.error("PayM not found with id = {}", payNo);
            response.setResultMsg("결제 정보를 찾을수 없습니다");
            response.setResultCode("500");
            return response;
        } catch (RestClientException e) {
            logger.error("INIAPI Payment Cancel request error");
            response.setResultMsg("이니시스 결제 취소 요청시 오류가 발생 했습니다");
            response.setResultCode("500");
            return response;
        } catch (Exception e) {
            logger.error("Server error");
            response.setResultMsg("서버 오류가 발생 했습니다");
            response.setResultCode("500");
            return response;
        }

        return response;
    }


    @Override
    public PayCancelPartialResponseVo cancelPayPartial(String tid, String msg, String cancelPrice, String priceAfterCancel) {
        logger.info("Cancelling payment partially for --> tid[{}], msg[{}], price[{}], priceAfterCancel[{}]", tid, msg, cancelPrice, priceAfterCancel);

        PayCancelPartialResponseVo response = new PayCancelPartialResponseVo();

        try {
            //결제 부분 취소하고자 하는 결제정보를 TID로 검색
            log.info("TID[{}] >> 부분결제취소 요청 정보 조회", tid);
            PayM payM = payMRepository.findByPayTid(tid).orElseThrow(() -> new EntityNotFoundException());
            log.info("TID[{}] >> 부분결제취소 요청 정보 조회 결과 payM = {}", tid, payM);

            String type = "PartialRefund";
            String paymethod = "Card";
            SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
            String timestamp = f.format(new Date());
            String clientIp = Inet4Address.getLocalHost().getHostAddress();

            log.info("TID[{}] >> 부분결제취소 시그니쳐 데이터 생성", tid);
            String hashInput = paymentProperties.getIniApiKey() + type + paymethod + timestamp
                    + clientIp + paymentProperties.getMid() + tid + cancelPrice + priceAfterCancel;

            String hashData = SignatureUtil.hash(hashInput, "SHA-512");
            log.info("TID[{}] >> 부분결제취소 시그니쳐 데이터 생성 완료", tid);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.add("Accept", "*/*");

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("type", type);
            map.add("paymethod", paymethod);
            map.add("timestamp", timestamp);
            map.add("clientIp", clientIp);
            map.add("mid", paymentProperties.getMid());
            map.add("tid", tid);
            map.add("msg", msg);
            map.add("price", cancelPrice);
            map.add("confirmPrice", priceAfterCancel);
            map.add("hashData", hashData);

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            response = restTemplate.postForObject("https://iniapi.inicis.com/api/v1/refund", entity, PayCancelPartialResponseVo.class);

            log.info("TID[{}] >> 부분결제취소 결과 정보 저장", tid);
            payM.setPrtcResultCode(response.getResultCode());
            payM.setPrtcResultMsg(response.getResultMsg());
            payM.setPrtcTid(response.getTid());
            payM.setPrtcRemains(response.getPrtcRemains());
            payM.setPrtcPrice(response.getPrtcPrice());
            payM.setPrtcType(response.getPrtcType());
            payM.setPrtcCnt(response.getPrtcCnt());
            payM.setPrtcDate(response.getPrtcDate());
            payM.setPrtcTime(response.getPrtcTime());
            payM.setPayType(PayType.EARLY_CANCEL);
            payMRepository.save(payM);

            log.info("TID[{}] >> 부분결제취소 결과 정보 저장 완료. resultCode = {}, resultMsg = {}",
                    tid, response.getResultCode(), response.getResultMsg());
        } catch (EntityNotFoundException e) {
            logger.error("PayM not found with tid = {}", tid);
        } catch (RestClientException e) {
            logger.error("INIAPI Payment Partial Cancel request error");
        } catch (Exception e) {
            logger.error("Server error");
        }
        return response;
    }

    @Override
    @Transactional
    public Map<String, Object> planCancel(Long userNo) {
        logger.info("Plan cancelling for user --> {}", userNo);

        // 성공 메세지 / 실패 메세지 returnMap에 저장
        Map<String, Object> returnMap = new HashMap<>();

        try {
            // 유저정보와 유저플랜, 유저빌링 정보 조회
            log.info("userNo[{}] >> planCancel 유저정보 조회", userNo);
            UserM user = userMRepository.findById(userNo).orElse(null);
            if (user == null) {
                throw new EntityNotFoundException("유저정보 조회에 실패하였습니다.");
            }
            log.info("userNo[{}] >> planCancel 유저정보 조회 결과 User = {}", userNo, user);

            log.info("userNo[{}] >> planCancel 유저의 활성화 플랜 리스트 조회", userNo);
            List<UserPlan> userPlans = userPlanRepository.getActiveUserPlanListFindByUserNo(userNo);
            userPlans = userPlans.stream().filter(p -> p.getPlan().getPlanNo() <= 4).collect(Collectors.toList());
            log.info("userNo[{}] >> planCancel 유저의 활성화 플랜 리스트 조회 UserPlans = {}", userNo, userPlans);

            for (UserPlan userPlan : userPlans) {

                if (userPlan == null) {
                    throw new EntityNotFoundException("유저 구독정보 조회에 실패하였습니다.");
                }

                log.info("userPlan[{}] >> planCancel 구독취소 시작", userPlan.getNo());

                LocalDateTime nowLDT = LocalDateTime.now();
                LocalDateTime planStartLDT = userPlan.getPlanStartAt();

                log.info("userPlan[{}] >> planCancel 유저 플랜과 관련된 결제정보 조회", userPlan.getNo());
                PayM payM = userPlan.getPayM();
                log.info("userPlan[{}] >> planCancel 유저 플랜과 관련된 결제정보 조회 결과 PayM = {}", userPlan.getNo(), payM);

                if (payM == null) {
                    throw new EntityNotFoundException("결제정보 조회에 실패하였습니다.");
                }

                log.info("userPlan[{}] >> planCancel 유저 플랜과 관련된 빌링정보 조회", userPlan.getNo());
                Billing billing = billingRepository.getBillingByPayNo(payM.getPayNo());
                log.info("userPlan[{}] >> planCancel 유저 플랜과 관련된 빌링정보 조회 결과 PayM = {}", userPlan.getNo(), billing);

                if (billing == null) {
                    throw new EntityNotFoundException("빌링정보 조회에 실패하였습니다.");
                }

                if (nowLDT.isBefore(planStartLDT.plusDays(7))) {
                    log.info("userPlan[{}] >> planCancel 유저 플랜 결제일로부터 7일 이내. 부분환불 처리.", userPlan.getNo());

                    String origPrice = payM.getTotPrice();
                    String cancelPrice = calculateRefundAmount(userPlan, origPrice);
                    String priceAfterCancel = Integer.toString(Integer.parseInt(origPrice) - Integer.parseInt(cancelPrice));

                    log.info("userPlan[{}] >> planCancel 이니시스 부분환불 요청 tid = {}, cancelPrice = {}, priceAfterCancel = {}", userPlan.getNo(),
                            payM.getPayTid(), cancelPrice, priceAfterCancel);
                    PayCancelPartialResponseVo vo = cancelPayPartial(payM.getPayTid(), "구독취소에 의한 결제 부분환불", cancelPrice, priceAfterCancel);
                    log.info("userPlan[{}] >> planCancel 이니시스 부분환불 요청 결과 tid = {}, cancelPrice = {}, priceAfterCancel = {}, result = {}", userPlan.getNo(),
                            payM.getPayTid(), cancelPrice, priceAfterCancel, vo);

                    if ("00".equals(vo.getResultCode())) {
                        log.info("userPlan[{}] >> planCancel 이니시스 부분환불 요청 결과 성공", userPlan.getNo());
                        userPlan.setActive(false);
                        UserPlan userPlanResult = userPlanRepository.save(userPlan);
                        deleteBillInfo(billing.getNo());

                        UserTimeline userTimeline = userService.addUserTimeline(user, userPlanResult, "7일 이내 구독 취소", SubscriptionStatus.CANCEL, false);

                        log.info("userTimeline[{}] >> 12-1. 유저 타임라인 7일 이내 구독 취소 정보 저장 완료. User = {}", userTimeline.getNo(), user.getUserNo());
                    } else {
                        log.info("userPlan[{}] >> planCancel 이니시스 부분환불 요청 결과 실패", userPlan.getNo());
                    }
                } else {
                    log.info("userPlan[{}] >> planCancel 유저 플랜 결제일로부터 7일 지남. 정기결제 정지 처리.", userPlan.getNo());
                    // 유저테이블에서 불러온 후, 취소 정보를 등록 후 저장

                    log.info("userPlan[{}] >> planCancel 유저 취소요청 여부 (false -> true) 저장", userPlan.getNo());
                    user.setPlanCancelReqYn(true);
                    user.setPlanCancelReqAt(DateUtils.convertToLocalDateTime(new Date()));
                    userMRepository.save(user);
                    log.info("userPlan[{}] >> planCancel 유저 취소요청 여부 (false -> true) 저장 완료.  PlanCancelReqYn = {}", userPlan.getNo(), user.getPlanCancelReqYn());

                    // 빌링 테이블에서 활성화 여부 0 등록 후 저장
                    log.info("userPlan[{}] >> planCancel 빌링 활성화 여부 (true -> false) 저장", userPlan.getNo());
                    billing.setActive(false);
                    billingRepository.save(billing);
                    log.info("userPlan[{}] >> planCancel 빌링 활성화 여부 (true -> false) 저장 완료. Active = {}", userPlan.getNo(), billing.getActive());
                }
            }
            returnMap.put("msg", "SUCCESS");
            returnMap.put("detailMsg", "구독취소에 성공하였습니다.");
            log.info("userNo[{}] >> planCancel 성공", userNo);
        } catch (Exception e) {
            returnMap.put("msg", "FAIL");
            returnMap.put("detailMsg", "구독취소에 실패하였습니다.");
            returnMap.put("error", e.getMessage());
            log.error("userNo[{}] >> planCancel 실패", userNo);
        }

        return returnMap;
    }

    private String calculateRefundAmount(UserPlan userPlan, String origPrice) {
        int origPriceInt = Integer.parseInt(origPrice);
        int charUseCnt = userPlan.getCharUseCnt();
        int charMaxLimit = userPlan.getCharMaxLimit();
        int refundPriceInt = origPriceInt * (charMaxLimit - charUseCnt) / charMaxLimit;
        return Integer.toString(refundPriceInt);
    }


    @Transactional
    @Override
    public int deleteBillInfo(Long billingNo) {
        return billingRepository.deleteBillingByNo(billingNo);
    }

    @Override
    public Collection<BillingDto> getRoutineBilling(LocalDateTime currentDate) {
        return billingRepository.getRoutineBilling(currentDate);
    }

    @Override
    public List<Billing> getInactiveBillings() {
        return billingRepository.getInactiveBillings();
    }

    @Override
    public List<Billing> getInactiveBillingsByUserNo(Long userNo) {
        return billingRepository.getInactiveBillingByUserNo(userNo);
    }

    @Override
    public String getPaymentMethod(Long payNo) {
        return billingRepository.getBillingPaymentMethod(payNo);
    }


    @Override
    public List<UserPayHistoryResponseVo> getUserPayHistory(Long userNo) {
        log.info("Getting pay history for user --> {}", userNo);
        List<PayM> userPayHistory = payMRepository.findByUserNoOrderByPayNoDesc(userNo);
        List<UserPayHistoryResponseVo> userPayHistoryResponseVos = new ArrayList<>();

        if (!userPayHistory.isEmpty()) {
            userPayHistoryResponseVos = userPayHistory.stream().map(payM -> {
                Plan plan = planRepository.findPlanByPlanNo(payM.getPlanNo());
                CodeDetail codeDetail = codeDetailRepository.findCodeDetailByDetailCodeAndGroupCode(payM.getCardApplCode(), "1");
                String codeDetailName = codeDetail != null ? codeDetail.getDetailName() : "";

                return UserPayHistoryResponseVo.of(payM, plan.getPlanNm(), codeDetailName);
            }).collect(Collectors.toList());
        }
        log.info("{} pay history found for user --> {}", userPayHistoryResponseVos.size(), userNo);

        return userPayHistoryResponseVos;
    }

    @Override
    public ResponseEntity<ActiveBillingVo> getUserActiveBilling(Long userNo) {
        log.info("Getting active billing info for user --> {}", userNo);

        log.info("userNo[{}] >> getUserActiveBilling 정기결제 정보 조회", userNo);
        Billing billing = billingRepository.getActiveBillingByUserNo(userNo);
        if (billing == null) {
            throw new EntityNotFoundException("정기결제 정보가 존재하지 않습니다.");
        }
        log.info("userNo[{}] >> getUserActiveBilling 정기결제 정보 조회 결과 Billing = {}", userNo, billing);

        log.info("planNo[{}] >> getUserActiveBilling 플랜 정보 조회", billing.getPlanNo());
        Plan plan = planRepository.findPlanByPlanNo(billing.getPlanNo());
        if (plan == null) {
            throw new EntityNotFoundException("플랜 정보가 존재하지 않습니다.");
        }
        log.info("planNo[{}] >> getUserActiveBilling 플랜 정보 조회 결과 Plan = {}", billing.getPlanNo(), plan);

        log.info("payNo[{}] >> getUserActiveBilling 유저 플랜 정보 조회", billing.getPayNo());
        UserPlan userPlan = userPlanRepository.findUserPlanByPayNo(billing.getPayNo());
        if (userPlan == null) {
            throw new EntityNotFoundException("유저 플랜 정보가 존재하지 않습니다.");
        }
        log.info("payNo[{}] >> getUserActiveBilling 유저 플랜 정보 조회 결과 UserPlan = {}", billing.getPayNo(), userPlan);

        NumberFormat numFormat = NumberFormat.getInstance();
        numFormat.setGroupingUsed(true);
        String maxLimitWithComma = numFormat.format(plan.getMaxLimit());

        String planDesc = "무제한 생성 및 미리듣기 + ";

        if (billing.getPlanNo() == 4L) {
            planDesc += "기업용 맞춤 플랜";
        } else {
            planDesc += "월 최대 " + maxLimitWithComma + "자 다운로드";
        }

        log.info("userNo[{}] >> getUserActiveBilling 유저 결제 상세정보 가공", userNo);
        ActiveBillingVo activeBillingVo = ActiveBillingVo.builder()
                .planNmEn(plan.getPlanNm())
                .planNmKr(plan.getPlanNmKr())
                .planDesc(planDesc)
                .planStartAt(userPlan.getPlanStartAt())
                .planEndAt(userPlan.getPlanEndAt())
                .paymentAt(billing.getPaymentDate())
                .build();
        log.info("userNo[{}] >> getUserActiveBilling 유저 결제 상세정보 가공 완료 = {}", userNo, activeBillingVo);

        return ResponseEntity.ok(activeBillingVo);
    }

    @Override
    public List<Billing> getUnpaidBills() {
        return billingRepository.getUnpaidBilling();
    }

    @Override
    public UserPlan getUserPlanByPayNo(Long payNo) {
        return userPlanRepository.findUserPlanByPayNo(payNo);
    }


    /**
     * 정기결제 진행
     */
    @Transactional
    public void routineBill(BillingDto billing) throws Exception {
        INIpay inipay = new INIpay();

        log.info("2. 유저 정보 조회. UserNo = {}", billing.getUserNo());
        UserM user = userMRepository.findByUserNo(billing.getUserNo()).orElse(null);
        log.info("2-1. 유저플랜 정보 조회. User = {}", user);
        List<UserPlan> userPlans = user.getUserPlans();
        Hibernate.initialize(userPlans);
        log.info("2-2. 최신 유저플랜 정보 조회. UserPlan = {}", userPlans);
        UserPlan currUserPlan = userPlans.stream()
                .filter(userPlan -> userPlan.getPayM().getPayNo().equals(billing.getPayNo()))
                .collect(Collectors.toList())
                .get(0);

        Plan currPlan = currUserPlan.getPlan();
        log.info("2-3. 최신 유저플랜 정보. currPlan = {}", currPlan);
        String routineBillPrice = currPlan.getPlanPrice().toString();
        String goodName = currPlan.getPlanNm();
        String moid = paymentProperties.getMid() + "_" + billing.getUserNo() + "_" + SignatureUtil.getTimestamp();

        logger.info("3. 이니시스 데이터 세팅. @ routineBill ==> planNo = " + currPlan.getPlanNo() + " / routineBillPrice = " + routineBillPrice);
        INIdata data = makeRoutineBillINIdata("127.0.0.1", moid, goodName, routineBillPrice,
                "WON", user.getUserNm(), user.getCellPhone(), user.getUserId(),
                "0", "0", billing.getBillingKey(), "00");

        logger.info("3-1. INICIS request DATA : {}", data);
        INIdata resultData = inipay.payRequest(data);
        logger.info("3-2. INICIS RESULT DATA : {}", resultData.toString());

        log.info("4. 이니시스 승인 결과 업데이트.");
        PayM payM = new PayM();
        payM.setPlanNo(currPlan.getPlanNo());
        payM.setUserNo(billing.getUserNo());
        payM.setGoodName(goodName);
        payM.setTotPrice(billing.getPrice().toString());
        payM.setMoid(resultData.getData("moid"));
        payM.setPayMethod(resultData.getData("paymethod"));
        payM.setBuyerEmail(resultData.getData("buyeremail"));
        payM.setBuyerTel(resultData.getData("buyertel"));
        payM.setBuyerName(resultData.getData("buyername"));
        payM.setCardNum(resultData.getData("CardResultNumber") + "****");
        payM.setCardQuota(resultData.getData("cardquota"));
        payM.setCardApplCode(resultData.getData("CardResultCode"));
        payM.setApplResultCode(resultData.getData("ResultCode"));
        payM.setApplResultMsg(resultData.getData("ResultMsg"));
        payM.setCardPrtcCode(resultData.getData("CARD_PrtcCode"));
        payM.setCardSrcCode(resultData.getData("CARD_SrcCode"));
        payM.setCardPoint(resultData.getData("CardPoint"));
        payM.setCurrency(resultData.getData("currency"));
        payM.setCardBillkey(resultData.getData("billkey"));
        payM.setPayTid(resultData.getData("tid"));

        if (resultData.getData("ResultCode").equals("00")) {
            handleRoutineBillingSuccess(resultData, currUserPlan, payM, billing.getBillingNo());
        } else {
            handleRoutineBillingFail(resultData, currUserPlan, payM, billing.getBillingNo());
        }
    }

    private void handleRoutineBillingSuccess(INIdata resultData, UserPlan currUserPlan, PayM payM, Long billingNo) {
        logger.info("4-2. routinBill Success");
        logger.info(resultData.toString());

        LocalDateTime billingStartDate = now();
        LocalDateTime billingEndDate = billingStartDate.plusMonths(1);

        logger.info("5. PAY_M 정보 저장");
        payM.setPayType(PayType.ROUTINE);
        PayM payResult = payMRepository.save(payM);
        logger.info("5-1. PAY_M 정보 저장 완료. payResult = {}", payResult);

        currUserPlan.setActive(false);
        userPlanRepository.save(currUserPlan);

        UserPlan newUserPlan = new UserPlan();
        newUserPlan.setUser(currUserPlan.getUser());
        newUserPlan.setPlan(currUserPlan.getPlan());
        newUserPlan.setPayM(payResult);
        newUserPlan.setCharMaxLimit(currUserPlan.getCharMaxLimit());
        newUserPlan.setCharUseCnt(0);
        newUserPlan.setPlanStartAt(billingStartDate);
        newUserPlan.setPlanEndAt(billingEndDate);
        newUserPlan.setCreatedBy(payResult.getUserNo());
        UserPlan userPlanResult = userPlanRepository.save(newUserPlan);
        logger.info("@ save new user plan");

        logger.info("6. Billing 결제 날짜 정보 업데이트 userNo = {}, payNo = {}", payResult.getUserNo(), payResult.getPayNo());
        billingRepository.updateSuccessBillingInfo(payM.getCardBillkey(), billingEndDate,
                LocalDateTime.now(), billingNo, null, payResult.getPayNo());

        UserTimeline userTimeline = userService.addUserTimeline(currUserPlan.getUser(), userPlanResult, "구독 연장", SubscriptionStatus.EXTENDED, true);

        log.info("userTimeline[{}] >> 유저 타임라인 구독 연장 정보 저장 완료. User = {}", userTimeline.getNo(), currUserPlan.getUser().getUserNo());

        userMRepository.updatePlanServiceEndAt(payResult.getUserNo(), billingEndDate);
        logger.info("@ update successful billing");
    }

    private void handleRoutineBillingFail(INIdata resultData, UserPlan currUserPlan, PayM payM, Long billingNo) {
        logger.info("routineBill Fail");
        logger.info(resultData.toString());

        logger.info("5. PAY_M 정보 저장");
        payM.setPayType(PayType.FAIL);
        PayM payResult = payMRepository.save(payM);
        logger.info("5-1. PAY_M 정보 저장 완료. payResult = {}", payResult);

        UserTimeline userTimeline = userService.addUserTimeline(currUserPlan.getUser(), currUserPlan, "구독 연장", SubscriptionStatus.FAIL, false);

        currUserPlan.setMemo("구독 연장 실패");
        currUserPlan.setPayM(payResult);
        userPlanRepository.save(currUserPlan);
        log.info("userTimeline[{}] >> 유저 타임라인 구독 연장 실패 정보 저장 완료. User = {}", userTimeline.getNo(), currUserPlan.getUser().getUserNo());


        billingRepository.updateFailedBillingInfo(payM.getPayNo() + "_" + resultData.getData("ResultMsg"), billingNo);
        logger.info("@ update failed billing");
    }

    private boolean userHasDownloadRole(Long userNo, UserRoleEnum userRoleEnum) {
        UserRole userRole = userMRoleRepository.getUserRoleByUserNoAndRoleNm(userNo, userRoleEnum);
        return userRole != null;
    }


}

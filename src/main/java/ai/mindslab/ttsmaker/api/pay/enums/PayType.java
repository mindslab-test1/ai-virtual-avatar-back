package ai.mindslab.ttsmaker.api.pay.enums;

public enum PayType {
    REG("결제"),
    ROUTINE("정기 결제"),
    CANCEL("결제 취소"),
    EARLY_CANCEL("환불 (7일이내)"),
    ADMIN("관리자처리"),
    FAIL("실패");

    public final String desc;

    private PayType(String desc){
        this.desc = desc;
    }

    public String getDesc() {
        return this.desc;
    }
}

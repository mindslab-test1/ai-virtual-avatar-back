package ai.mindslab.ttsmaker.api.pay.service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import ai.mindslab.ttsmaker.api.pay.vo.ActiveBillingVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.UserPayHistoryResponseVo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ai.mindslab.ttsmaker.api.pay.domain.Billing;
import ai.mindslab.ttsmaker.api.pay.dto.BillingDto;
import ai.mindslab.ttsmaker.api.pay.vo.BillingFormVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.PayCancelPartialResponseVo;
import ai.mindslab.ttsmaker.api.pay.vo.response.PayCancelResponseVo;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;

public interface PayService {

    ResponseEntity<BillingFormVo> basicBilling(AvaUser avaUser, String planNo) throws Exception;

    RedirectView billingPay(String oid, HttpServletRequest request, RedirectAttributes redirectAttr);

    String getUip(HttpServletRequest request);

    PayCancelResponseVo cancelPay(Long payNo, String msg) throws Exception;

    PayCancelPartialResponseVo cancelPayPartial(String tid, String msg, String price, String confirmPrice) throws Exception;

    Collection<BillingDto> getRoutineBilling(LocalDateTime currentDate);

    List<Billing> getInactiveBillingsByUserNo(Long userNo);

    String getPaymentMethod(Long userId);

    void routineBill(BillingDto billing) throws Exception;

    List<UserPayHistoryResponseVo> getUserPayHistory(Long userNo);

    ResponseEntity<ActiveBillingVo> getUserActiveBilling(Long userNo) throws Exception;

    Map<String, Object> planCancel(Long userNo);

    int deleteBillInfo(Long billingNo);

    List<Billing> getInactiveBillings();

    List<Billing> getUnpaidBills();

    UserPlan getUserPlanByPayNo(Long payNo);
}

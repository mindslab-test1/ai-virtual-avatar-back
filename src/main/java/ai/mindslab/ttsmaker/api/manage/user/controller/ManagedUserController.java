package ai.mindslab.ttsmaker.api.manage.user.controller;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.manage.user.service.ManagedUserService;
import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.response.UserAccessResponseDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.response.UserListResponseDto;
import ai.mindslab.ttsmaker.api.script.service.TTSFilterService;
import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.common.page.PageRequest;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping(path = "/manage/users")
public class ManagedUserController {

	@Autowired
	private ManagedUserService managedUserService;
	
	@Autowired
	private TTSFilterService ttsFilterService; 
	
	@GetMapping(path = "/test")
	@ResponseBody
	public List<String> test() throws IOException {
        return ttsFilterService.swearWords();
	}
	
    @GetMapping
    @ResponseBody
    public AvaPage<UserListResponseDto> getUserList(final PageRequest pageable, final UserSearchRequestDto searchRequestDto) {
		pageable.setSortColumn("userNo");
		return managedUserService.getList(pageable.of(), searchRequestDto);
	}
    
    @GetMapping(path = "/access")
    @ResponseBody
    public List<UserAccessResponseDto> getAccessList(@RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
    	if(StringUtils.isEmpty(startDate)) {
    		startDate = DateUtils.asString(LocalDate.now(), "yyyy-MM-dd");
    	}
    	if(StringUtils.isEmpty(endDate)) {
    		endDate = DateUtils.asString(LocalDate.now(), "yyyy-MM-dd");
    	}
    	return managedUserService.getAccessList(startDate, endDate);
    }
    
    @GetMapping(path = "/access/count")
    @ResponseBody
    public Map<String, Long> getAccessListGroupByUserId(@RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
    	return this.getAccessList(startDate, endDate).stream().collect(Collectors.groupingBy(UserAccessResponseDto::getUserId, Collectors.counting()));
    }
    
}

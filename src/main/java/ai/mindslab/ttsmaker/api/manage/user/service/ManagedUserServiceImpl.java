package ai.mindslab.ttsmaker.api.manage.user.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import ai.mindslab.ttsmaker.api.user.repository.UserTtsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.manage.user.service.conditions.UserSearchSpecs;
import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.response.UserAccessResponseDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.response.UserListResponseDto;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserRefreshToken;
import ai.mindslab.ttsmaker.api.user.repository.UserMRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserRefreshTokenRepository;
import ai.mindslab.ttsmaker.common.page.AvaPageImpl;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.util.DateUtils;
import reactor.util.StringUtils;

@Service
public class ManagedUserServiceImpl implements ManagedUserService {

	@Autowired
	private UserMRepository userMRepository;

	@Autowired
	private UserTtsRepository userTtsRepository;
	
	@Autowired
	private UserRefreshTokenRepository userRefreshTokenRepository;
	
	@Override
	public AvaPage<UserListResponseDto> getList(PageRequest pageable, UserSearchRequestDto searchDto) {
		Specification<UserM> specification  = UserSearchSpecs.chainingSpecs(searchDto);
		Page<UserM> page = userMRepository.findAll(specification, pageable);
		List<UserListResponseDto> list = page.getContent().stream().map(UserListResponseDto::of).collect(Collectors.toList());
		return new AvaPageImpl<>(list, pageable, page.getTotalElements());
	}

	@Override
	public List<UserAccessResponseDto> getAccessList(String startDate, String endDate) {
		LocalDateTime startDatetime = LocalDateTime.of(DateUtils.parseLocalDate(startDate, "yyyy-MM-dd"), LocalDateTime.MIN.toLocalTime());
		LocalDateTime endDatetime = LocalDateTime.of(DateUtils.parseLocalDate(endDate, "yyyy-MM-dd"), LocalDateTime.MAX.toLocalTime());
		List<UserRefreshToken> list = userRefreshTokenRepository.findAllByCreatedAtBetween(startDatetime, endDatetime);
		return list.stream().map(UserAccessResponseDto::of).collect(Collectors.toList());
	}

}

package ai.mindslab.ttsmaker.api.manage.user.vo.response;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserListReponseDto {
	private Long userNo;
	private String userNm;
	private String userEmail;
	private String userCellPhone;
	private String companyNm;
	private List<UserPlanReponseDto> plans;
		
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
	private LocalDateTime createdAt;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
	private LocalDateTime updatedAt;
	
	public static UserListReponseDto of(UserM entity) {
		List<UserPlan> userPlans = entity.getUserPlans();
		List<UserPlanReponseDto> plans = userPlans.stream().map(UserPlanReponseDto::of).collect(Collectors.toList());
		return UserListReponseDto.builder()
				.userNo(entity.getUserNo())
				.userNm(entity.getUserNm())
				.userEmail(entity.getUserId())
				.userCellPhone(entity.getCellPhone())
				.companyNm(entity.getCompanyNm())
				.plans(plans)
				.createdAt(entity.getCreatedAt())
				.updatedAt(entity.getUpdatedAt())
				.build();
	}
}

package ai.mindslab.ttsmaker.api.manage.user.vo.request;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.common.page.PageRequest;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.*;

import java.time.LocalDateTime;

@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public final class UserSearchRequestDto extends PageRequest {
	private String userNm;
	private String userId;
	private String userCellPhone;
	private String companyNm;
	private String planNo;
	private UserM userM;
	private Boolean incProf;
	private Boolean incProfApi;
	private String createdAt;
	private LocalDateTime createdAtDt;

	public LocalDateTime getCreatedAtDt() {
		return LocalDateTime.of(DateUtils.parseLocalDate(getCreatedAt(), "yyyy-MM-dd"), LocalDateTime.MIN.toLocalTime());
	}

}

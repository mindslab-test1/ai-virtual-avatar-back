package ai.mindslab.ttsmaker.api.manage.user.vo.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.user.domain.UserRefreshToken;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserAccessResponseDto {
	private Long userNo;;
	private String userId;
		
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdAt;
	
	public static UserAccessResponseDto of(UserRefreshToken entity) {
		return UserAccessResponseDto.builder()
				.userNo(entity.getUserNo())
				.userId(entity.getUserId())
				.createdAt(entity.getCreatedAt())
				.build();
	}
}

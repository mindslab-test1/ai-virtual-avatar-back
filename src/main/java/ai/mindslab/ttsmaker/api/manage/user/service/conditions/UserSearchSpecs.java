package ai.mindslab.ttsmaker.api.manage.user.service.conditions;

import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import com.google.common.collect.Lists;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class UserSearchSpecs {
	
	public static Specification<UserM> chainingSpecs(UserSearchRequestDto searchDto) {
		
		List<Specification<UserM>> specs = Lists.newArrayList();
		// 사용자 이름 조회
		if(StringUtils.hasText(searchDto.getUserNm())) {
			specs.add(userNmLike(searchDto.getUserNm()));
		}
		
		// 사용자 E-mail 조회
		if(StringUtils.hasText(searchDto.getUserId())) {
			specs.add(userIdLike(searchDto.getUserId()));
		}
		
		// 사용자 연락처 조회
		if(StringUtils.hasText(searchDto.getUserCellPhone())) {
			specs.add(userCellPhoneEqual(searchDto.getUserCellPhone()));
		}
		
		// 회사 이름 조회
		if(StringUtils.hasText(searchDto.getCompanyNm())) {
			specs.add(companyNmLike(searchDto.getCompanyNm()));
		}
		
		// 구독 상품 조회
		if(StringUtils.hasText(searchDto.getPlanNo())) {
			specs.add(planNoEqual(ai.mindslab.ttsmaker.util.StringUtils.toLong(searchDto.getPlanNo())));
		}
		
		Specification<UserM> specification = Specification.where(null);
		if(!CollectionUtils.isEmpty(specs)) {
			specification = specs.stream().reduce(specification, (specs1, specs2) -> specs1.and(specs2));
		}
		return specification;
	}
	
	/**
	 * <pre>
	 * 사용자 이름 조회(Specification)
	 * <pre> 
	 * @param userNm
	 * @return
	 */
	public static Specification<UserM> userNmLike(final String userNm) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.like(root.get("userNm"), "%" + userNm + "%");
		 });
    }
	
	/**
	 * <pre>
	 * 사용자 이메일 조회(Specification)
	 * <pre> 
	 * @param userId
	 * @return
	 */
	public static Specification<UserM> userIdLike(final String userId) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.like(root.get("userId"), "%" + userId + "%");
		 });
    }
	
	/**
	 * <pre>
	 * 사용자 휴대폰 조회(Specification)
	 * <pre> 
	 * @param cellPhone
	 * @return
	 */
	public static Specification<UserM> userCellPhoneEqual(final String cellPhone) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.equal(root.get("cellPhone"), cellPhone);
		 });
    }
	
	/**
	 * <pre>
	 * 회사 이름 조회(Specification)
	 * <pre> 
	 * @param companyNm
	 * @return
	 */
	public static Specification<UserM> companyNmLike(final String companyNm) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.like(root.get("companyNm"), "%" + companyNm + "%");
		 });
    }
    
    public static Specification<UserM> planNoEqual(final Long planNo) {
		return (Specification<UserM>) ((root, query, builder) -> {
			return builder.equal(root.join("userPlans").get("plan").get("planNo"), planNo);
		});
	}

	public static Specification<UserTts> ttsSearchSpecs(UserSearchRequestDto searchDto) {

		List<Specification<UserTts>> specs = Lists.newArrayList();
		// 사용자로 조회
		if(searchDto.getUserM() != null) {
			specs.add(userEqual(searchDto.getUserM()));
		}

		// 욕설포함 여부로 조회
		if(searchDto.getIncProf() != null) {
			specs.add(incProfEqual(searchDto.getIncProf()));
		}

		// 욕설포함(API) 여부로 조회
		if(searchDto.getIncProfApi() != null) {
			specs.add(incProfApiEqual(searchDto.getIncProfApi()));
		}

		// 시작일로 조회
		if(StringUtils.hasText(searchDto.getCreatedAt())) {
			specs.add(createAtGt(searchDto.getCreatedAtDt()));
		}

		Specification<UserTts> specification = Specification.where(null);
		if(!CollectionUtils.isEmpty(specs)) {
			specification = specs.stream().reduce(specification, (specs1, specs2) -> specs1.and(specs2));
		}

		return specification;

	}

	/**
	 * <pre>
	 * 회원 조회(Specification)
	 * <pre>
	 * @param userM
	 * @return
	 */
	public static Specification<UserTts> userEqual(final UserM userM) {
		return (Specification<UserTts>) ((root, query, builder) -> {
			return builder.equal(root.get("user"), userM);
		});
	}

	/**
	 * <pre>
	 * 욕설 포함 여부(Specification)
	 * <pre>
	 * @param incProf
	 * @return
	 */
	public static Specification<UserTts> incProfEqual(final boolean incProf) {
		return (Specification<UserTts>) ((root, query, builder) -> {
			return builder.equal(root.get("incProf"), incProf);
		});
	}

	/**
	 * <pre>
	 * 욕설 포함(API) 여부(Specification)
	 * <pre>
	 * @param incProfApi
	 * @return
	 */
	public static Specification<UserTts> incProfApiEqual(final boolean incProfApi) {
		return (Specification<UserTts>) ((root, query, builder) -> {
			return builder.equal(root.get("incProfApi"), incProfApi);
		});
	}

	/**
	 * <pre>
	 * 조회일 이후(Specification)
	 * <pre>
	 * @param createdAt
	 * @return
	 */
	public static Specification<UserTts> createAtGt(final LocalDateTime createdAt) {
		return (Specification<UserTts>) ((root, query, builder) -> {
			return builder.greaterThan(root.get("createdAt"), createdAt);
		});
	}

}

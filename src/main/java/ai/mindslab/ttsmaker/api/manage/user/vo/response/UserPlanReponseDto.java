package ai.mindslab.ttsmaker.api.manage.user.vo.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;
import ai.mindslab.ttsmaker.api.user.domain.UserPlan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserPlanReponseDto {
    private Long planNo;
    private String planNm;
    private Integer charUseCnt;
    private Integer charMaxLimit;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planStartAt;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime planEndAt;
	
	public static UserPlanReponseDto of(UserPlan entity) {
		Plan plan = entity.getPlan();
		return UserPlanReponseDto.builder()
				.planNo(plan.getPlanNo())
				.planNm(plan.getPlanNm())
				.charUseCnt(entity.getCharUseCnt())
				.charMaxLimit(entity.getCharMaxLimit())
				.build();
	}
	
}

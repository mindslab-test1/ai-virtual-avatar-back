package ai.mindslab.ttsmaker.api.manage.user.service;

import java.util.List;

import ai.mindslab.ttsmaker.api.user.domain.UserTts;
import org.springframework.data.domain.PageRequest;

import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.response.UserAccessResponseDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.response.UserListResponseDto;
import ai.mindslab.ttsmaker.common.page.AvaPage;

public interface ManagedUserService {
	public AvaPage<UserListResponseDto> getList(PageRequest pageable, UserSearchRequestDto searchDto);

	public List<UserAccessResponseDto> getAccessList(String startDate, String endDate);

}

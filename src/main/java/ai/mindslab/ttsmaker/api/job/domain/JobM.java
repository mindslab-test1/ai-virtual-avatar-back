package ai.mindslab.ttsmaker.api.job.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "JOB_M")
public class JobM implements Serializable {
	
	private static final long serialVersionUID = 4111328443959601779L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    //JOB ID
    @Column(name = "JOB_ID", columnDefinition = "varchar(36)")
    private String jobId;
    
    //등록자
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OWNER_BY")
    private Long ownerBy;
   
    //JOB_TITLE
    @Column(name = "JOB_TITLE", columnDefinition = "varchar(255)")
    private String jobTitle;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy ="jobM", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    private List<JobScript> jobScripts = Lists.newArrayList();
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;
    
    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
    
    public void deleteJobScript(JobScript jobScript)
    {
    	jobScripts.remove(jobScript);
    	jobScript.setJobM(null);
    }
}

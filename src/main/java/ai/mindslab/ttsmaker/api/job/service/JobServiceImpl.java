package ai.mindslab.ttsmaker.api.job.service;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.service.VFCharacterService;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.job.domain.JobM;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import ai.mindslab.ttsmaker.api.job.repository.JobMRepository;
import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.*;
import ai.mindslab.ttsmaker.api.job.vo.response.preview.JobPreviewContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.preview.JobPreviewPreferVFCharacterVo;
import ai.mindslab.ttsmaker.api.job.vo.response.preview.JobPreviewScriptVo;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.TTSApiResponseMessage;
import ai.mindslab.ttsmaker.api.script.service.TTSFilterService;
import ai.mindslab.ttsmaker.api.script.service.TextFilterService;
import ai.mindslab.ttsmaker.util.DateUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import maum.brain.textfilter.TextFilterProto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JobServiceImpl implements JobService {

    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    @Autowired
    private JobMRepository jobMRepository;
    
    @Autowired
    private VFCharacterService vfCharacterService;  
    
    @Autowired
    private TTSFilterService ttsFilterService;

    @Autowired
    private TextFilterService textFilterService;

    @Value("${spring.grpc.active}")
    boolean textFilterActive;

    @Override
    public List<JobListItemVo> getJobList() {
        try {
            List<JobM> list = jobMRepository.getActiveJobList(Sort.by(Sort.Direction.DESC, "updatedAt"));
            return list.stream().map(JobListItemVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작업 리스트 조회 중 오류가 발생했습니다.");
        }
    }

    @Override
    public JobListContentVo getJobListByUserNo(Long userNo) {
        try {
        	List<JobM> list = jobMRepository.getActiveJobListByUserId(userNo, Sort.by(Sort.Direction.DESC, "updatedAt"));

        	// job List 조회 
        	List<JobListItemVo> myList = list.stream().map(JobListItemVo::of).collect(Collectors.toList());
        	
        	// 최근 job의 미리보기 가공
        	JobPreviewContentVo recentJobPreview = null;
        	if(!CollectionUtils.isEmpty(list)) {
        		recentJobPreview = makeJobPreviewContentVo(list.get(0));
        	}
        	
            return JobListContentVo.builder()
            		.myList(myList)
            		.recentJobPreview(recentJobPreview)
            		.build();
            
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작업 리스트 조회 중 오류가 발생했습니다.");
        }
    }

    @Override
    public JobContentVo getJobContent(String jobId) {
        try {
            JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
        	return getJobContentPostProcessor(jobM);
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0002", "작업 데이터를 불러오는중  오류가 발생했습니다.");
        }
    }
    public JobContentVo getJobContentPostProcessor(JobM jobM) {
    	String jobId = jobM.getJobId();
    	List<JobScriptVo> jobScriptVoList = Lists.newArrayList();
    	List<JobScript> jobScripts = jobM.getJobScripts();
        if (!CollectionUtils.isEmpty(jobScripts)) {
            jobScriptVoList = jobScripts.stream().map(JobScriptVo::of).collect(Collectors.toList());
            setJobScriptsSaveVoMapTagetByRedis(jobId, makeJobScriptSaveVoMap(jobScripts));
        }
        return JobContentVo.of(jobM, jobScriptVoList);
    }
    
    public Map<String, JobScriptVo> getJobScriptMapFromRedis(String jobId) {
    	HashOperations<String, String, JobScriptVo> stringObjectObjectHashOperations = redisTemplate.opsForHash();
    	return stringObjectObjectHashOperations.entries(String.format("ava:job:%s:scripts", jobId));
    }
        
    /**
     * <pre>
     * jobScriptsSaveVoMap 저장(Redis), JobContent 조회시 저장
     * </pre>
     *
     * @param jobId
     * @param jobScriptsVoMap
     * @return
     */
    public boolean setJobScriptsSaveVoMapTagetByRedis(String jobId, Map<String, JobScriptVo> jobScriptsVoMap) {
        try {
            String key = String.format("ava:job:%s:scripts", jobId);
            redisTemplate.opsForHash().putAll(key, jobScriptsVoMap);
            redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0008", "JobScriptSaveVo Map 업데이트 처리 중 오류가 발생했습니다.");
        }
    }


    /**
     * <pre>
     * JobScriptSaveVo 저장(Redis)
     * </pre>
     *
     * @param jobId
     * @param scriptId
     * @param saveVo
     * @return
     */
    @Override
    public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptVo saveVo) {
        try {
            String key = String.format("ava:job:%s:scripts", jobId);
            redisTemplate.opsForHash().put(key, scriptId, saveVo);
            redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
        }
    }
    
    @Transactional
    @Override
    public JobContentVo saveJobContent(Long userNo, String jobId, JobContentSaveVo jobContentSaveVo) {
        log.info("Saving {} jobContent  ==> {}", jobId, jobContentSaveVo);

        try {
        	String targetJobId = jobId;
        	Map<String, JobScriptVo> jobScriptsVoMap = getJobScriptMapFromRedis(jobId);
        	Map<String, TTSApiResponseMessage> ttsResultMap = getTTSResultMapFromRedis(jobId);
        	if(jobContentSaveVo.isSaveAs()) { // 다른 이름으로 저장시 
        		targetJobId = UUID.randomUUID().toString();
        	}
        	
        	Map<String, JobScript> jobScriptsMap = null;
            // JOB 레코드 저장
            JobM jobM = jobMRepository.getJobContentFindByJobId(targetJobId);
            if (jobM != null) {
                jobM.setJobTitle(jobContentSaveVo.getJobTitle());
                jobM.setUpdatedAt(LocalDateTime.now());
                jobScriptsMap = makeJobScriptMap(jobM.getJobScripts());
            } else {
                jobM = new JobM();
                jobM.setJobId(targetJobId);
                jobM.setJobTitle(jobContentSaveVo.getJobTitle());
                jobM.setOwnerBy(userNo);
                jobM.setCreatedAt(LocalDateTime.now());
                jobM.setUpdatedAt(LocalDateTime.now());
                jobScriptsMap = new HashMap<String, JobScript>();
            }
            
            jobM = em.merge(jobM);
            Map<String, VFCharacter> charactersMap = vfCharacterService.getVFCharactersMap(); 

            // 기존 스크립트 삭제
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedScripts())) {
                for (String deletedScriptId : jobContentSaveVo.getDeletedScripts()) {
                    if (jobScriptsMap.containsKey(deletedScriptId)) {
                    	jobM.deleteJobScript(jobScriptsMap.get(deletedScriptId));
                    }
                }
            }
            // 스크립트 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getScripts())) {
                Integer orderSeq = 1;
                for (String saveScriptId : jobContentSaveVo.getScripts()) {
                	
                	boolean existDbJobScript = jobScriptsMap.containsKey(saveScriptId);
                	if(jobScriptsVoMap.containsKey(saveScriptId)) {
                		TTSApiResponseMessage saveTTSResultVo = ttsResultMap.get(saveScriptId);
                		JobScriptVo saveJobScriptVo = jobScriptsVoMap.get(saveScriptId);
                		String scriptId = saveJobScriptVo.getScriptId();
                		String characterId = saveJobScriptVo.getCharacterId();
                		VFCharacter vfCharacter = charactersMap.get(characterId);
                		
                		JobScript jobScript = Optional.ofNullable(jobScriptsMap.get(saveScriptId)).orElse(new JobScript());
                		
                		jobScript.setScriptText(saveJobScriptVo.getScriptText());            // TTS 스크립트 Text
                        if(saveTTSResultVo != null) {
//                        	jobScript.setObjectUrl(saveTTSResultVo.getS3ObjectUrl()); 		 // S3 Object 이름 정보
//                        	jobScript.setAwsS3RegionNm(saveTTSResultVo.getS3Region());		 // S3 Region
//                        	jobScript.setAwsS3BucketNm(saveTTSResultVo.getS3BucketNm());	 // S3 Bucket 이름
//                        	jobScript.setAwsS3ObjectNm(saveTTSResultVo.getS3ObjectNm());	 // S3 Object 이름
                        	jobScript.setTtsExpiredDate(saveTTSResultVo.getExpiredDate());   // TTS Resource 만료일
                        	jobScript.setTtsResource(saveTTSResultVo.getResource());			 // TTS Resource byte[]
                        	jobScript.setTtsPlayTime(saveTTSResultVo.getResourceDuration());
                        }
                        jobScript.setOrderSeq(orderSeq);                                     // 정렬순서
                        jobScript.setTtsState(saveJobScriptVo.getTtsState());                // TTS 상태
                        jobScript.setRowState("S");                                          // ROW 상태
                        
                		if(existDbJobScript) {
                			jobScript.setUpdatedBy(userNo);
                            jobScript.setUpdatedAt(LocalDateTime.now());
                            em.merge(jobScript);
                		}
                		else {
                			jobScript.setJobM(jobM);
                            jobScript.setScriptId(scriptId);
                            jobScript.setVfCharacter(vfCharacter);
                            jobScript.setCreatedBy(userNo);
                            jobScript.setCreatedAt(LocalDateTime.now());
                            jobM.getJobScripts().add(jobScript);
                		}
                         orderSeq++;
                	}
                }
            }
            jobM = em.merge(jobM);
            em.flush();
            
            return getJobContentPostProcessor(jobM);
            
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JOB 저장 처리 중 오류가 발생했습니다. : " + ex.getMessage());
        }
    }

    /**
     * <pre>
     * List<JobScript> to Map<String, JobScript> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private Map<String, JobScript> makeJobScriptMap(List<JobScript> jobScripts) {
        log.info("Mapping JobScript {}", jobScripts);
        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().collect(Collectors.toMap(JobScript::getScriptId, Function.identity()));
        }
        return new HashMap<>();
    }


    /**
     * <pre>
     * List<JobScript> to Map<String, JobScriptSaveVo> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private Map<String, JobScriptVo> makeJobScriptSaveVoMap(List<JobScript> jobScripts) {
        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().map(JobScriptVo::of).collect(Collectors.toMap(JobScriptVo::getScriptId, Function.identity()));
        }
        return new HashMap<>();
    }
    
    @Transactional
    @Override
	public boolean removeJobContent(Long userNo, String jobId) {

        JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
        
        if(jobM == null) {
        	throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", String.format("JOB ID(%s) 데이터가 존재 하지 않습니다.", jobId));
        }
        
        if(jobM.getOwnerBy() != userNo) {
        	throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0010", String.format("JOB ID(%s) 삭제 할 권한이 없습니다.", jobId));
        }
        
        jobM.setIsDeleted("Y");
        jobMRepository.save(jobM);
        
		return true;
	}

	@Override
	public JobScriptSaveResultVo updateJobScript(String jobId, String scriptId, JobScriptVo saveVo) {
		this.setJobScriptsSaveVoTagetByRedis(jobId, scriptId, saveVo);
        String key = "ava:default:filter:words";
        List<String> invalidKeywordList = Optional.ofNullable((List<String>) redisTemplate.opsForValue().get(key))
                .orElseGet(() -> {
                    List<String> words = ttsFilterService.swearWords();
                    redisTemplate.opsForValue().set(key, words);
                    return words;
                });
        if(textFilterActive){
            textFilterService.sendAsyncPredictTextLabel(TextFilterProto.TextFilterRequest.newBuilder().setText(saveVo.getScriptText()).build(), null);
        }else{
            log.info("grpc prop is not active.");
        }
        if(!saveVo.isCheckProfanity()){
            String invalidKeyword = invalidKeywordList.stream()
                    .filter(saveVo.getScriptText()::contains)
                    .distinct()
                    .collect(Collectors.joining(","));

            boolean invalidSlangWord = !invalidKeyword.equals("");
            return JobScriptSaveResultVo.builder()
                    .data(saveVo)
                    .invalidSlangWord(invalidSlangWord)
                    .invalidCause(invalidSlangWord ? "스크립트 내용 중 욕설이 감지 되었습니다." : "")
                    .invalidKeyword(invalidKeyword).build();
        }else{
            String scriptCountKey = "ava:default:filter:count:"+scriptId;
            Map<Object, Object> count = invalidKeywordList.stream()
                    .filter(saveVo.getScriptText()::contains)
                    .map(v -> {
                        long cnt = 0;
                        Matcher matcher = Pattern.compile(v).matcher(saveVo.getScriptText());
                        while (matcher.find()) cnt++;
                        return new Object[]{v, cnt};
                    }).collect(Collectors.toMap(v -> v[0], v -> v[1], (o, n) -> o));
            redisTemplate.opsForValue().set(scriptCountKey, count);
            redisTemplate.expireAt(scriptCountKey, DateUtils.asDate(LocalDateTime.now().plusMinutes(10)));
            return JobScriptSaveResultVo.builder()
                    .invalidSlangWord(count.size() > 0)
                    .build();
        }
	}
	
	/**
	 * <pre>
	 * JobScript 캐릭터 선호도 랭킹 가공 (최대 8건 까지)
	 * <pre>
	 * @param charactersMap
	 * @param jobScripts
	 * @return
	 */
	private List<JobPreviewPreferVFCharacterVo> getPreferVFList(Map<String, VFCharacter> charactersMap, List<JobScript> jobScripts) {
		List<JobPreviewPreferVFCharacterVo> preferVFList = new ArrayList<JobPreviewPreferVFCharacterVo>();
		if (!CollectionUtils.isEmpty(jobScripts)) {
			Map<String, Long> preferVFGroupByCountMap = jobScripts.stream()
					.map(JobScript::getVfCharacter)
					.collect(Collectors.groupingBy(VFCharacter::getCharacterId, Collectors.counting()));
			Set<String> characterIds = charactersMap.keySet();
			preferVFList.addAll(preferVFGroupByCountMap.entrySet().stream()
					.sorted(Map.Entry.<String, Long>comparingByValue().reversed())
					.filter(m->characterIds.contains(m.getKey()))
					.map(p->JobPreviewPreferVFCharacterVo.of(charactersMap.get(p.getKey()), p.getValue()))
					.collect(Collectors.toList()));

			// 8건+ 이상 경우 8건 까지 노출
			final long EXPOSE_LIST_SIZE = 8l;
			if(preferVFList.size() > EXPOSE_LIST_SIZE) {
				preferVFList = preferVFList.stream().limit(EXPOSE_LIST_SIZE).collect(Collectors.toList());
			}
		}
		return preferVFList;
	}

	@Override
	public JobPreviewContentVo getJobContentPreview(String jobId) {
		try {
            JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
            if(jobM == null)
            	throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0008", String.format("JOB ID(%s) 데이터가 존재 하지 않습니다.", jobId));
            
            return makeJobPreviewContentVo(jobM);
        } catch (TTSMakerApiException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "미리 보기 데이터를 불러오는중  오류가 발생했습니다.");
        }
	}
	
	/**
	 * <pre>
	 * JOBM 미리보기 데이터 가공
	 * </pre>
	 * @param jobM
	 * @return
	 */
	private JobPreviewContentVo makeJobPreviewContentVo(JobM jobM) {
		// 보이스폰트 조회(Map)
        Map<String, VFCharacter> charactersMap = vfCharacterService.getVFCharactersMap();
        // 해당 JOB ID의 JobScript 조회
        List<JobScript> jobScripts = jobM.getJobScripts();
        
        List<JobPreviewScriptVo> jobScriptVoList = Lists.newArrayList();
        List<JobPreviewPreferVFCharacterVo> preferVFList = Lists.newArrayList();
        
        // 18건+ 이상 경우 18건 까지 노출
     	final long EXPOSE_JOBSCRIPT_LIST_SIZE = 18l;
     	
     	// 미리보기 데이터 가공
        if (!CollectionUtils.isEmpty(jobScripts)) {
        	jobScriptVoList.addAll(jobScripts.stream().limit(EXPOSE_JOBSCRIPT_LIST_SIZE).map(JobPreviewScriptVo::of).collect(Collectors.toList()));
        	preferVFList.addAll(getPreferVFList(charactersMap, jobScripts));
        }
        
        return JobPreviewContentVo.builder()
        		.id(jobM.getId())
        		.jobId(jobM.getJobId())
        		.jobTitle(jobM.getJobTitle())
        		.updatedAt(jobM.getUpdatedAt())
        		.preferVFList(preferVFList)
        		.jobScripts(jobScriptVoList)
        		.build();
	}
	
   public Map<String, TTSApiResponseMessage> getTTSResultMapFromRedis(String jobId) {
    	HashOperations<String, String, TTSApiResponseMessage> stringObjectObjectHashOperations = redisTemplate.opsForHash();
    	return stringObjectObjectHashOperations.entries(String.format("ava:job:%s:tts-result", jobId));
    }
}

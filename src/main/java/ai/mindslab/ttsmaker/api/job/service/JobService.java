package ai.mindslab.ttsmaker.api.job.service;

import java.util.List;
import java.util.Map;

import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListItemVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptSaveResultVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;
import ai.mindslab.ttsmaker.api.job.vo.response.preview.JobPreviewContentVo;

public interface JobService {
	/**
	 * <pre>
	 * JOB List 조회
	 * </pre>
	 * @return
	 */
	public List<JobListItemVo> getJobList();

	/**
	 * <pre>
	 * 각 User ID마다 JOB List 조회(최근 JOB의 미리보기 포함)
	 * </pre>
	 *
	 * @return
	 */
	public JobListContentVo getJobListByUserNo(Long userNo);
	
	/**
	 * <pre>
	 * JOB ID별 선택한 화자 및 스크립트 로드
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public JobContentVo getJobContent(String jobId);
	
	
	public Map<String, JobScriptVo> getJobScriptMapFromRedis(String jobId);
	
	/**
	 * <pre>
	 * JOB 삭제 처리
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public boolean removeJobContent(Long userNo, String jobId);

	/**
	 * <pre>
	 * 스크립트(JobScriptVo) 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @param saveVo
	 * @return
	 */
	public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptVo saveVo);
	

	/**
	 * <pre>
	 * JOB 저장/다른이름으로 저장
	 * </pre>
	 * @param jobId
	 * @param userNo
	 * @param jobContentSaveVo
	 * @return
	 */
	public JobContentVo saveJobContent(Long userNo, String jobId, JobContentSaveVo jobContentSaveVo);

	/**
	 * <pre>
	 * JOB Script 임시 저장(redis & 욕설 필터 검증)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @param saveVo
	 * @return
	 */
	public JobScriptSaveResultVo updateJobScript(String jobId, String scriptId, JobScriptVo saveVo);

	/**
	 * <pre>
	 * JOB Script 미리 보기
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public JobPreviewContentVo getJobContentPreview(String jobId);

	
}

package ai.mindslab.ttsmaker.api.job.vo.response.preview;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobPreviewScriptVo {
	private String scriptId;			// 스크립트 ID
	private String scriptText;			// 스크립트
	private Integer orderSeq;			// jobScript 순서
	private String characterId;			// 보이스폰트 ID
	private String characterNm; 		// 보이스폰트 이름
	private String characterClassNm;	// 보이스 폰트 class이름
	
	public static JobPreviewScriptVo of(JobScript entity) {
		VFCharacter character = entity.getVfCharacter();
        return JobPreviewScriptVo.builder()
        		.scriptId(entity.getScriptId())
        		.scriptText(entity.getScriptText())
        		.orderSeq(entity.getOrderSeq())
        		.characterId(character.getCharacterId())
        		.characterNm(character.getCharacterNm())
        		.characterClassNm(character.getAvatarClassNm())
        		.build();
    }
}

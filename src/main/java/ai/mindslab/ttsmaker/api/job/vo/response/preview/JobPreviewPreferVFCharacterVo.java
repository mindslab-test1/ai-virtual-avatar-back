package ai.mindslab.ttsmaker.api.job.vo.response.preview;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobPreviewPreferVFCharacterVo {
	private String characterId; 	// 보이스폰트 ID
	private String characterNm; 	// 보이스폰트 이름
	private String avatarClassNm;   // 보이스폰트 아바타 클래스 명
	private Long count;				// 사용된 카운팅
	
	public static JobPreviewPreferVFCharacterVo of(VFCharacter entity, Long count) {
        return JobPreviewPreferVFCharacterVo.builder()
                .characterId(entity.getCharacterId())
                .characterNm(entity.getCharacterNm())
                .avatarClassNm(entity.getAvatarClassNm())
                .count(count)
                .build();
	}
}

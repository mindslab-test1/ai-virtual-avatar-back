package ai.mindslab.ttsmaker.api.job.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.job.service.JobService;
import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptSaveResultVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;
import ai.mindslab.ttsmaker.api.job.vo.response.preview.JobPreviewContentVo;
import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import ai.mindslab.ttsmaker.api.script.service.TTSMakeService;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/job")
public class JobRestController {

	@Autowired
	private JobService jobService;
	
	@Autowired
	private TTSMakeService ttsService;
	
	@ResponseBody
	@GetMapping(value = "/myJobList")
	public JobListContentVo myJobList(@AvaUserParam AvaUser avaUser) throws IOException {
		log.info("[myJobList By User] : {}", AvaUser.userPrint(avaUser));
		return jobService.getJobListByUserNo(avaUser.getUserNo());
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}")
	public JobContentVo getJobContent(@PathVariable String jobId) throws IOException {
		return jobService.getJobContent(jobId);
	}
	
	@ResponseBody
	@PostMapping(value = "/{jobId}")
	public boolean removeJobContent(@PathVariable String jobId, @AvaUserParam AvaUser avaUser) throws IOException {
		return jobService.removeJobContent(avaUser.getUserNo(), jobId);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/preview")
	public JobPreviewContentVo getJobContentPreview(@PathVariable String jobId) throws IOException {
		log.info("[Getting Job Content] : {}", jobId);
		return jobService.getJobContentPreview(jobId);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}")
	public JobContentVo saveJobContent(@AvaUserParam AvaUser avaUser, @PathVariable String jobId, @RequestBody JobContentSaveVo jobContentSaveVo) {
		log.info("[Saving Job Content] : {}", jobId, jobContentSaveVo);
		return jobService.saveJobContent(avaUser.getUserNo(), jobId, jobContentSaveVo);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}/{scriptId}")
	public JobScriptSaveResultVo updateJobScript(@AvaUserParam AvaUser avaUser, @PathVariable String jobId, @PathVariable String scriptId, @RequestBody JobScriptVo saveVo) {
		log.info("[Setting Job Script] : {}, {}", jobId, scriptId);
		return jobService.updateJobScript(jobId, scriptId, saveVo);
	}
	
	@ResponseBody
	@PostMapping(value = "/makeTTS/{jobId}/{scriptId}")
	public TTSMakeResultVo requestTTS(@AvaUserParam AvaUser avaUser, @PathVariable String jobId, @PathVariable String scriptId, @RequestParam String characterId, @RequestParam String text) throws IOException, InterruptedException {
		log.info("[Requesting toMakeTTSv2] : {}, {}, {}, {}", jobId, scriptId, characterId, text );
		return ttsService.requestTTSMakeFile(jobId, scriptId, characterId, text, avaUser);
	}
}

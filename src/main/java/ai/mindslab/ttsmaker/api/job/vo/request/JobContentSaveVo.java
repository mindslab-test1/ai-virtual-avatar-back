package ai.mindslab.ttsmaker.api.job.vo.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentSaveVo {
	String jobTitle;					// 작업 제목
	List<String> scripts;				// 작성한 스크립트 리스트
	List<String> deletedScripts;		// 삭제할 스크립트 리스트
	boolean saveAs;					// 다른 이름으로 저장
}

package ai.mindslab.ttsmaker.api.job.vo.response;

import java.util.List;

import ai.mindslab.ttsmaker.api.job.vo.response.preview.JobPreviewContentVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobListContentVo {
	private List<JobListItemVo> myList;
	private JobPreviewContentVo recentJobPreview;
}

package ai.mindslab.ttsmaker.api.job.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString(exclude = "jobM")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "JOB_SCRIPT")
public class JobScript implements Serializable {
	
	private static final long serialVersionUID = 9182436812539983497L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="JOB_ID", referencedColumnName = "JOB_ID", columnDefinition = "varchar(36)", nullable = false,  foreignKey = @ForeignKey(name = "FK_JOB_SCRIPT__JOB_M"))
    private JobM jobM;
    
    //SCRIPT_ID
    @Column(name = "SCRIPT_ID", columnDefinition = "varchar(36)", nullable = false)
    private String scriptId;
    
    //화자 ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHARACTER_ID", referencedColumnName = "CHARACTER_ID", columnDefinition = "varchar(36)", nullable = false, foreignKey = @ForeignKey(name = "FK_JOB_SCRIPT__CHARACTER"))
    private VFCharacter vfCharacter;
    
    // TTS 스크립트 Text
    @Column(name = "SCRIPT_TEXT", columnDefinition = "TEXT", nullable = false)
    private String scriptText;
    
//    // TTS Resource URL정보(임시보관)
//    @Column(name = "OBJECT_URL", columnDefinition = "varchar(1000)")
//    private String objectUrl;
//    
//    // S3 Region Name
//    @Column(name = "AWS_S3_REGION_NM", columnDefinition = "varchar(250)")
//    private String awsS3RegionNm;
//    
//    // S3 버킷 이름
//    @Column(name = "AWS_S3_BUCKET_NM", columnDefinition = "varchar(500)")
//    private String awsS3BucketNm;
//    
//    // S3 오브젝트 이름
//    @Column(name = "AWS_S3_OBJECT_NM", columnDefinition = "varchar(500)")
//    private String awsS3ObjectNm;
    
    @Lob
    @Column(name = "TTS_RESOURCE", columnDefinition = "MEDIUMBLOB")
    private byte[] ttsResource;
    
    @Column(name = "TTS_PLAY_TIME")
	private Float ttsPlayTime;
    
    @Column(name = "TTS_EXPIRED_DATE")
    private LocalDate ttsExpiredDate;
    
    //정렬순서
    @Column(name = "ORDER_SEQ", columnDefinition = "int(11)")
    private Integer orderSeq;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //TTS 상태
    @Column(name = "TTS_STATE", columnDefinition = "varchar(10) default 'INIT'")
    private String ttsState;
    
    //ROW 상태
    @Column(name = "ROW_STATE", columnDefinition = "varchar(10) default 'S'")
    private String rowState;
    
    //등록일시
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;
    
    //등록자
    @Column(name = "CREATED_BY")
    private Long createdBy;

    //수정일시
    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
    
    //수정자
    @Column(name = "UPDATED_BY")
    private Long updatedBy;
}

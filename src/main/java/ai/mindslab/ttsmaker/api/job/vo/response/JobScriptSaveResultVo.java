package ai.mindslab.ttsmaker.api.job.vo.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobScriptSaveResultVo {
	private JobScriptVo data;
	private boolean invalidSlangWord;
	private String invalidCause;
	private String invalidKeyword;
	private boolean acceptSlangResponsibility; // accept responsibility for the use of slang
}

package ai.mindslab.ttsmaker.api.job.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.job.domain.JobM;

@Repository
public interface JobMRepository extends JpaRepository<JobM, Long> {
	
	@Query("select p from JobM p where p.isDeleted = 'N'")
	List<JobM> getActiveJobList(Sort sort);

	@Query("select p from JobM p where p.isDeleted = 'N' and p.ownerBy = ?1")
	List<JobM> getActiveJobListByUserId(Long userNo, Sort sort);
	
	@Query("select p from JobM p where p.isDeleted = 'N' and p.jobId = ?1")
	JobM getJobContentFindByJobId(String jobId);
}

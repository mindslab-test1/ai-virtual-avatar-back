package ai.mindslab.ttsmaker.api.job.vo.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobListItemVo {
	private Long id;
	private String jobId;
	private String jobTitle;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updatedAt;
	
	public static JobListItemVo of(JobM entity) {
        return JobListItemVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.jobTitle(entity.getJobTitle())
        		.updatedAt(entity.getUpdatedAt())
        		.build();
    }
}

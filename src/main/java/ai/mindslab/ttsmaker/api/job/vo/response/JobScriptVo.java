package ai.mindslab.ttsmaker.api.job.vo.response;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobScriptVo {
	private Long id;
	private String jobId;
	private String scriptId;
	private String characterId;
	private boolean checkProfanity;
	private String scriptText;
	private byte[] ttsResource;
	private Float ttsPlayTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate ttsExpiredDate;
	
	private Integer orderSeq;
	private String ttsState;
	private String rowState;
	
	public static JobScriptVo of(JobScript entity) {
		VFCharacter character = entity.getVfCharacter();
        return JobScriptVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobM().getJobId())
        		.scriptId(entity.getScriptId())
        		.characterId(character.getCharacterId())
        		.scriptText(entity.getScriptText())
        		.ttsResource(entity.getTtsResource())
        		.ttsPlayTime(entity.getTtsPlayTime())
        		.ttsExpiredDate(entity.getTtsExpiredDate())
        		.orderSeq(entity.getOrderSeq())
        		.ttsState(entity.getTtsState())
        		.rowState(entity.getRowState())
        		.build();
    }
}

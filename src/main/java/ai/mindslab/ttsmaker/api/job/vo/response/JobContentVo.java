package ai.mindslab.ttsmaker.api.job.vo.response;

import java.util.List;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentVo {
	private Long id;
	private String jobId;
	private String jobTitle;
	private List<JobScriptVo> jobScripts;
	
	public static JobContentVo of(JobM entity, List<JobScriptVo> scripts) {
        return JobContentVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.jobTitle(entity.getJobTitle())
        		.jobScripts(scripts)
        		.build();
    }
}

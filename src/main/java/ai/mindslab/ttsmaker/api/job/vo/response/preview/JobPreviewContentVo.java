package ai.mindslab.ttsmaker.api.job.vo.response.preview;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobPreviewContentVo {
	private Long id;
	private String jobId;
	private String jobTitle;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updatedAt;
	private List<JobPreviewPreferVFCharacterVo> preferVFList;
	private List<JobPreviewScriptVo> jobScripts; 
}

package ai.mindslab.ttsmaker.common.page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <pre>
 * 페이징 처리 를 위한 {AvaPage} 구현체
 * </pre>
 * @param <T>
 */
@JsonIgnoreProperties(value = {"pageable", "last", "totalPages", "totalElements", "size", "number", "first", "numberOfElements", "sort", "empty"})
public class AvaPageImpl<T> extends PageImpl<T> implements AvaPage<T> {

	private static final long serialVersionUID = 5278000546767470096L;

	@JsonCreator
	public AvaPageImpl(List<T> content, Pageable pageable, long total) {
		super(content, pageable, total);

	}

	@JsonCreator
	public AvaPageImpl(List<T> content) {
		super(content);
	}

	@JsonGetter(value = "data")
	@Override
	public List<T> getContent() {
		return super.getContent();
	}
	
	@JsonGetter(value = "size")
	public int size() {
		if(!CollectionUtils.isEmpty(this.getContent())) {
			return this.getContent().size();
		}
		return 0;
	}

	@JsonGetter(value = "paging")
	public Map<String, Object> getPaging() {
		Map<String, Object> paging = new HashMap<>();
		paging.put("totalPages", super.getTotalPages());
		paging.put("totalElements", super.getTotalElements());
		paging.put("pageSize", super.getSize());
		paging.put("pageNumber", super.getNumber() + 1);
		paging.put("isFirst", super.isFirst());
		paging.put("isLast", super.isLast());
		paging.put("hasNext", super.hasNext());
		paging.put("hasPrevious", super.hasPrevious());
		paging.put("isEmpty", super.isEmpty());
		return paging;
	}
	
	public Map<String, Object> toMap() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("data", this.getContent());
		resultMap.put("paging", this.getPaging());
		return resultMap;
	}
}

package ai.mindslab.ttsmaker.common.page;


import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 페이징 처리 를 위한 interface 클래스
 * </pre>
 *
 * @param <T>
 */
public interface AvaPage<T> {
	/**
	 * <pre>
	 * content List 조회
	 * </pre>
	 * @return
	 */
	List<T> getContent();
	
	/**
	 * <pre>
	 * content size
	 * <pre>
	 * @return
	 */
	int size();
	
	/**
	 * <pre>
	 * 페이징 정보
	 * </pre>
	 * @return
	 */
	Map<String, Object> getPaging();
	
	/**
	 * <pre>
	 * convert to Map
	 * </pre>
	 * @return
	 */
	Map<String, Object> toMap();
}

package ai.mindslab.ttsmaker.common.page;

import org.springframework.data.domain.Sort;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <pre>
 * 페이징 처리를 위한 PageRequest Helper 클래스
 * </pre>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PageRequest {
    private Integer page;
	
    private Integer size = 10;
	
    private String sortColumn = "updatedAt";
	
    private String[] sortColumns = null;

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setSize(Integer size) {
        int DEFAULT_SIZE = 10;
        int MAX_SIZE = 50;
        this.size = size > MAX_SIZE ? DEFAULT_SIZE : size;
    }
  
    // getter
    public org.springframework.data.domain.PageRequest of() {
    	if(page == null) {
    		page = 1;
    	}
    	
        if (sortColumns == null) {
            return org.springframework.data.domain.PageRequest.of(page -1, size, Sort.Direction.DESC, sortColumn);
        } else {
            return org.springframework.data.domain.PageRequest.of(page -1, size, Sort.Direction.DESC, sortColumns);
        }
    }
    
    public boolean hasPage() {
    	return this.page > 0;
    }
}
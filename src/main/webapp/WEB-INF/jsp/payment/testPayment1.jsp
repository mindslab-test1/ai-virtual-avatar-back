<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
	<script src="//code.jquery.com/jquery-1.11.3.js"></script>
	<script language="javascript" type="text/javascript" src="https://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.get( "/product/list", function( data ) {
				$.each(data, function(idx, item){
					$("#productList").append($("<li></li>", {text: item.prdNm, "data-prd-no" : item.prdNo }))
				})
			});

			$(document).on('click', '#productList li', function() {
				var prdNo = $(this).data("prd-no");
				var userId = $("#txtUserId").val();
				var _param = { "method" : "billingPay", "prdNo": prdNo, "userId": userId }
				$.get( "/payment/billingForm", _param, function( data ) {
					var frm$ = $("#billingForm")
					frm$.find("input[name='mid']").val(data.mid);
					frm$.find("input[name='goodname']").val(data.basicName);
					frm$.find("input[name='oid']").val(data.oid);
					frm$.find("input[name='price']").val(data.prdPrice);

					frm$.find("input[name='buyername']").val(data.name);
					frm$.find("input[name='buyertel']").val(data.phoneNumber);
					frm$.find("input[name='buyeremail']").val(data.userId);
					frm$.find("input[name='timestamp']").val(data.timestamp);
					frm$.find("input[name='signature']").val(data.orderSignature);
					frm$.find("input[name='returnUrl']").val(data.siteDomain + "/payment/" + data.method +"?email=" + userId +"?planNm=" +data.basicName);

					frm$.find("input[name='mKey']").val(data.mkey);
					frm$.find("input[name='offerPeriod']").val(data.dateTo + "-"+ data.dateFrom);
					frm$.find("input[name='closeUrl']").val(data.siteDomain + "/payment/close");
					
					
				});
			});

			$("#btnPayment").click(function() {
				INIStdPay.pay('billingForm')
			})		
		})
	</script>
</head>
<body>
	<input type="hidden" id="txtUserId" value="minds@minds.com"/>
	<ul id="productList">
	</ul>
	<button id="btnPayment">결제</button>
	<!-- 무조건 returnUrl을 사용 도메인과 매칭시켜야됩니다. -->
	<form id="billingForm" name="billingForm" method="POST">
	
	    <!-- ***** 필 수 ***** -->
	    <input type="hidden" name="version" value="1.0">
	    <input type="hidden" name="mid" value="" /> <!--mindslab01-->
	    <input type="hidden" name="goodname">
	    <input type="hidden" name="oid"> <!--mindslab01_Date-->
	    <input type="hidden" name="price">
	    <!--TODO: erase value-->
	    <input type="hidden" name="currency" value="WON">
	    <input type="hidden" name="buyername">
	    <input type="hidden" name="buyertel">    <!-- phone 번호 확인 필요 -->
	    <input type="hidden" name="buyeremail">
	    <input type="hidden" name="timestamp">
	    <input type="hidden" name="signature">
	    
	    <input type="hidden" name="returnUrl">
	    <input type="hidden" name="mKey" >
	
	    <!-- ***** 기본옵션 ***** -->
	    <input type="hidden" name="gopaymethod" value="Card">
	    <input type="hidden" name="offerPeriod" >
	    <!--20200930-20201030-->
	    <input type="hidden" name="acceptmethod" value="BILLAUTH(card)">
	    <!-- 	<input   id="billPrint_msg" name="billPrint_msg" value="고객님의 매월 결제일은 24일 입니다." > -->
	
	    <!-- ***** 표시옵션 ***** -->
	    <input type="hidden" name="languageView" value="ko">
	    <input type="hidden" name="charset" value="UTF-8">
	    <input type="hidden" name="payViewType" value="overlay">
	    <input type="hidden" name="closeUrl">
	
	    <!-- ***** 추가옵션 ***** -->
	    <!-- 	<input  name="merchantData" value="" > -->
	    <!--            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />-->
	</form>
</body>
</html>